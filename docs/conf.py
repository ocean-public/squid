"""Sphinx configuration."""
project = "squid"
author = "Krakens Team"
copyright = f"2020, {author}"
source_parsers = {
    ".md": "recommonmark.parser.CommonMarkParser",
}
source_suffix = [".rst", ".md"]
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
]
