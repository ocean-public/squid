Reference
=========
.. contents::
    :local:
    :backlinks: none

squid.framework.core.models.bases
---------------------------
.. automodule:: squid.framework.core.models.bases
   :members:

squid.framework.core.models.responses
---------------------------
.. automodule:: squid.framework.core.models.responses
   :members:

squid.framework.core.commands
---------------------------
.. automodule:: squid.framework.core.commands
   :members:

squid.framework.core.commands.add
---------------------------
.. automodule:: squid.framework.core.commands.add
   :members:

squid.framework.core.commands.dump
---------------------------
.. automodule:: squid.framework.core.commands.dump
   :members:

squid.framework.core.commands.execute
---------------------------
.. automodule:: squid.framework.core.commands.execute
   :members:

squid.framework.core.commands.get
---------------------------
.. automodule:: squid.framework.core.commands.get
   :members:

squid.framework.core.commands.init
---------------------------
.. automodule:: squid.framework.core.commands.init
   :members:

squid.framework.core.commands.invoke
---------------------------
.. automodule:: squid.framework.core.commands.invoke
   :members:

squid.framework.core.commands.link
---------------------------
.. automodule:: squid.framework.core.commands.link
   :members:

squid.framework.core.commands.remove
---------------------------
.. automodule:: squid.framework.core.commands.remove
   :members:

squid.framework.core.commands.update
---------------------------
.. automodule:: squid.framework.core.commands.update
   :members:

squid.framework.services.db
---------------------------
.. automodule:: squid.framework.services.db
   :members:

squid.framework.services.db.db_abc
---------------------------
.. automodule:: squid.framework.services.db.db_abc
   :members:

squid.framework.services.db.db_tinydb
---------------------------
.. automodule:: squid.framework.services.db.db_tinydb
   :members:

squid.framework.services.engine
-------------------------------
.. automodule:: squid.framework.services.engine
   :members:

squid.framework.services.engine.engine_abc
-------------------------------
.. automodule:: squid.framework.services.engine.engine_abc
   :members:

squid.framework.services.engine.engine_luigi
-------------------------------
.. automodule:: squid.framework.services.engine.engine_luigi
   :members:

squid.framework.services.persistence
------------------------------------
.. automodule:: squid.framework.services.persistence
   :members:

squid.framework.services.registry
---------------------------------
.. automodule:: squid.framework.services.registry
   :members:

squid.interfaces.console
-------------------------------
.. automodule:: squid.interfaces.console
   :members:

squid.interfaces.console.add
-------------------------------
.. automodule:: squid.interfaces.console.add
   :members:

squid.interfaces.console.callbacks
-------------------------------
.. automodule:: squid.interfaces.console.callbacks
   :members:

squid.interfaces.console.cli
-------------------------------
.. automodule:: squid.interfaces.console.cli
   :members:

squid.interfaces.console.execute
-------------------------------
.. automodule:: squid.interfaces.console.execute
   :members:

squid.interfaces.console.get
-------------------------------
.. automodule:: squid.interfaces.console.get
   :members:

squid.interfaces.console.helpers
-------------------------------
.. automodule:: squid.interfaces.console.helpers
   :members:

squid.interfaces.console.init
-------------------------------
.. automodule:: squid.interfaces.console.init
   :members:

squid.interfaces.console.link
-------------------------------
.. automodule:: squid.interfaces.console.link
   :members:

squid.interfaces.console.misc
-------------------------------
.. automodule:: squid.interfaces.console.misc
   :members:

squid.interfaces.console.plugins
-------------------------------
.. automodule:: squid.interfaces.console.plugins
   :members:

squid.interfaces.console.remove
-------------------------------
.. automodule:: squid.interfaces.console.remove
   :members:

squid.interfaces.console.update
-------------------------------
.. automodule:: squid.interfaces.console.update
   :members:
