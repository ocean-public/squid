FROM python:latest

RUN mkdir -p /dist
COPY dist/ /dist
COPY requirements.txt /dist/requirements.txt

RUN pip install poetry toml && \
    pip install -r /dist/requirements.txt && \
    mv /dist/squid-*.tar.gz /dist/squid.tar.gz && \
    pip install /dist/squid.tar.gz

WORKDIR /mnt
ENTRYPOINT ["squid"]
