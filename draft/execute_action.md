# EXECUTE Action
With this action you can execute tasks on tentacles from an Squid Project. The execution engine of the task is [Luigi](https://github.com/spotify/luigi) and can be configured with:
* To configured update the file `ls $(squid get tentacle -n head -o "$.path" -r)/.squid/luigi.toml` according to the [documentation](https://luigi.readthedocs.io/en/stable/configuration.html#configurable-options).
* To apply the configuration run `export LUIGI_CONFIG_PATH=$(squid get tentacle -n head -o "$.path" -r)/.squid/luigi.toml && export LUIGI_CONFIG_PARSER=toml`

There are specific env variables to control the execution engine options apart from the configuration, such as:
* **ENGINE_CMDS_EXEC_SEQ** indicates how multiple commands defined on a task will be execute. The valid values are:
  * **isolated** each command will be run individual. This is the default value.
  * **pipe** the commands would be separated with |.
  * **and** the commands would be seperated with &&.
* **ENGINE_CMDS_EXEC_SEQ_SHELL** indicates with shell is going to be used when using **ENGINE_CMDS_EXEC_SEQ=and**. By default is **bash**.
* **ENGINE_CMD_FG** indicates if the execution would be on FOREGROUND **ENGINE_CMD_FG=true** or BACKGROUND **ENGINE_CMD_FG=false**, by default is set to false. When using **ENGINE_CMD_FG=true** the persisting logging is disabled, only the stdout.
* **ENGINE_LOG_ENABLE** indicates if engine logging is enable, by default is set to true.
* **ENGINE_LOG_LEVEL** indicates the level of engine logging which values can be **debug, info, warning, error, critical or not_set**. By default is info.

## Global options
* **--name / -n** name of the tentacle or task to execute depending on the resource specify.
* **--env / -e** specify or overide default values to env vars of the tasks to be executed.
* **--engine-output** when present indicates that the output of the execution will include the summary from the engine.
* **--dry-run / -d** will print you what tasks and tentacles are going to run.
* **--std-in / -i** will open a input stream to the standard input checking for a valid Tentacle | Task schema depending on the resource specify.
* **--verbose / -v** will output all the input parameters.

## EXECUTE TENTACLES Resource
When running this action with the resource **tentacles** the task that will be executed may be:
* Already linked tasks to the tentacle using the option **deep** and not specifying an option **task**.
* Specifying a task with the option **task** and running only on the base tentacle.
* Specifying a task with the option **task** and using the option **deep** for running on the base tentacle and its dependencies.
* Specifying a **std-in** for dynamic created tasks and running only on the base tentacle.
* Specifying a **std-in** for dynamic created tasks and using the option **deep** for running on the base tentacle and its dependencies.

### Options
* **--task / -t** name of the task to be run on the tentacle.
* **--filter-task / -f** filter the tasks to be run across the tentacle. The use is according to the [JsonPath](https://goessner.net/articles/JsonPath/) specs and [JsonPath RW Ext](https://python-jsonpath-rw-ext.readthedocs.io/en/latest/readme.html#quick-start). You can use [this](https://jsonpath.com/) to test syntax.
* **--deep** when present this flag indicates a deep resolving so the returning tentacle has all the dependencies.

_**NOTE:** when using the option **filter-task** use also **deep** to get the tasks already linked and filter them (do not use the option **task**)._

### Examples
* Execute the task named **task_c** on **tentacle_a** on foreground
```bash,use=example,exec
 squid execute tentacles -n tentacle_a -t task_c -e ENGINE_CMD_FG=true
```

* Execute the task named **task_c** on **tentacle_a** on foreground with only the output of the task
```bash,use=example,exec
 squid execute tentacles -n tentacle_a -t task_c -e ENGINE_CMD_FG=true -e ENGINE_LOG_ENABLE=false --no-engine-output
```

* Execute the task linked to the tentacle **tentacle_d** on background with only the output of the tasks on logging but the summary on stdout
```bash,use=example,exec
echo "Clean logs"
rm -f $(squid get tentacle -n tentacle_d -o "$.path" -r)/.tentacle/logs/*.log || true
echo "Execute"
squid execute tentacles -n tentacle_d --deep -e ENGINE_LOG_ENABLE=false
echo "Validating"
ls -la $(squid get tentacle -n tentacle_d -o "$.path" -r)/.tentacle/logs/*.log
for logfile in $(find $(squid get tentacle -n tentacle_d -o "$.path" -r)/.tentacle/logs -name "*.log"); do cat $logfile; done
```

* Trying execute tentacles without linked tasks
```bash,use=example,exec
squid execute tentacles -n tentacle_e --deep -e ENGINE_CMD_FG=true -e ENGINE_LOG_ENABLE=false --no-engine-output
```

* Check execution before run
```bash,use=example,exec
squid execute tentacles -n tentacle_e -t task_c --dry-run --verbose
```

* Execute only the tasks of category **terraform** on the tentacle **head** and its dependencies which have them.
```bash,use=example,exec
squid execute tentacles -n head --deep -f "$.category == 'terraform'" -e ENGINE_CMD_FG=true -e ENGINE_LOG_ENABLE=false --no-engine-output
```

## EXECUTE TASKS Resource
When running this action with the resource **tasks** the task that will be executed may be:
* Already linked tentacles to the task and not specifying an option **tentacle**.
* Specifying a tentacle with the option **tentacle** will run the task on its main and all its dependencies.
* Specifying a **std-in** for dynamic created tentacles.

### Options
* **--tentacle / -t** name of the tentacle on which will run the task.

### Examples
* Execute the task named **task_c** on **tentacle_a** on foreground  with only the output of the task
```bash,use=example,exec
squid execute tasks -n task_c -t tentacle_a -e ENGINE_CMD_FG=true -e ENGINE_LOG_ENABLE=false --no-engine-output
```

* Execute the task named **task_d** on all the tentacles that don't have dependencies to other tentacles
```bash,use=example,exec
squid get tentacles -l "~ ($.dependencies.any($.cls == 'tentacle'))" | squid execute tasks -n task_d --std-in -e ENGINE_CMD_FG=true -e ENGINE_LOG_ENABLE=false --no-engine-output
```

* Execute the task named **task_a** on a dynamic created tentacle and custom env var
```bash,use=example,exec
echo "{\"name\":\"dynamic\",\"type\":\"test\",\"path\":\"$(pwd)\"}" | squid execute tasks -n task_a -i -e TEST="this is squid." -e ENGINE_CMD_FG=true -e ENGINE_LOG_ENABLE=false --no-engine-output
```
