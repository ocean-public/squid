# PLUGINS Action
With this action you can run the entries defined on a plugin register on an Squid Project. The options would depend on how the plugin would behave.

### Examples
* Execute plugin already added to squid
```bash,use=example,exec
echo "Check plugins"
squid get plugins -o "$.[*].name" -r
echo "Plugins on CLI"
squid plugins
echo "Options defined on plugin"
squid plugins inner-exec --help
echo "Execute"
squid plugins inner-exec --tentacle tentacle_a
```
