# LINK Action
With this action you can link Tentacles or Tasks with each other an Squid Project.

## Global options
* **--source / -s** resource to add as a dependency.
* **--source-type / --st** type of resource source could be tentacle or task.
* **--target / -t** resource to add the dependency source, it will depend of the resource selected Tentacles or Tasks.
* **--remove / -r** when present this flag indicates the removal of the link.
* **--dry-run / -d** will print you what is going to be link or unlink.
* **--std-in / -i** will open a input stream to the standard input checking for a valid Tentacle | Task schema depending of the resource specify.
* **--verbose / -v** will output all the input parameters.

_**NOTE:** when using **std-in** it replace the use os **source** and **source-type**._

## LINK TENTACLES Resource

### Examples
* Link **tentacle_a** to **tentacle_b**
```bash,use=example,exec
squid link tentacles --target tentacle_b --source tentacle_a --source-type tentacle --dry-run
```

* Remove the link between **tentacle_a** and **tentacle_b**
```bash,use=example,exec
squid link tentacles --target tentacle_b --source tentacle_a --source-type tentacle -r --dry-run
```

## LINK TASKS Resource

### Examples
* Link **tentacle_a** and **tentacle_b** to **task_a**
```bash,use=example,exec
squid get tentacles --query "($.name == 'tentacle_a') | ($.name == 'tentacle_b')" | squid link tasks --std-in --dry-run --target task_a
```
