# UPDATE Action
With this action you can update Tentacles, Tasks or Plugins from an Squid Project.

## Global options
* **--kvs** the JSON string object with the key values to update. The keys and values acepted will be validating against the type of resource Tentacle, Task or Plugin (some keys are not mutable). There are also special keys to execute operations based on [TinyDB operations](https://tinydb.readthedocs.io/en/stable/usage.html#updating-data).
* **--query / -q** the query statement to search the resource to update Tentacle, Task or Plugins. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).
* **--dry-run / -d** will print you what is going to be updated.
* **--editor / -e** will open the default terminal editor with the current spec of the resource Tentacle | Task | Plugin.
* **--verbose / -v** will output all the input parameters.

_**Note:** when using **editor** option the resources would be resolve with all their dependencies._

## UPDATE TENTACLES Resource
Update tentacles that satisfy the set options.

### Examples
* Update tentacles type that star with the name **tentacle_*
```bash,use=example,exec
echo "Current"
squid get tentacles -q "$.name.matches('tentacle_.*')" -o "$.[*].type"
echo "Update"
squid update tentacles -q "$.name.matches('tentacle_.*')" --kvs '{"type": "new"}'
echo "Verify"
squid get tentacles -q "$.name.matches('tentacle_.*')" -o "$.[*].type"
echo "Rollback"
squid update tentacles -q "$.name.matches('tentacle_.*')" --kvs '{"type": "test"}'
```

* Update tentacles named **tentacle_a** and **tentacle_b** with editor
```bash
squid update tentacles -q "($.name == 'tentacle_a') | ($.name == 'tentacle_b')" -e --dry-run
```

## UPDATE TASKS Resource
Update tasks that satisfy the set options.

### Examples
* Update tasks with category **terraform** by adding suffix to the category
```bash,use=example,exec
echo "Current"
squid get tasks -q "$.category == 'terraform'" -o "$.[*].category"
echo "Update"
squid update tasks -q "$.category == 'terraform'" --kvs '{"category": {"op": "add", "value": "_sfx"}}'
echo "Verify"
squid get tasks -q "$.category.matches('terraform.*')" -o "$.[*].category"
echo "Rollback"
squid update tasks -q "$.category.matches('terraform.*')" --kvs '{"category": {"op": "set", "value": "terraform"}}'
```

## UPDATE PLUGINS Resource
Update plugins that satisfy the set options.

### Examples
* Trying adding suffix to the name of a plugin
```bash,use=example,exec
squid update plugins -q "$.name == 'plugin-exec'" --kvs '{"category": {"op": "add", "value": "_sfx"}}'
```
