# Getting started

## Install
* Download tarball from [RELEASES](https://gitlab.com/entel-ocean/squid/-/releases)
* Run `pip install --upgrade squid-X.X.X.tar.gz` on your virtualenv or main interpreter

## Squid Project Initialization
As mentioned before, squid is intended to work on a git repository so it is important to run theses commands on the root of the git repo.

* With root folder as a tentacle
```bash
squid init
```

* Without root folder as a tentacle
```bash
squid init --not-add-root
```

## Examples
The example folder is intended to be used as a playground for the usage of Squid (all is versioned so don't be shy to messed around)
```bash,prepare=example
cd example
```

## Using Docker
Build python package with poetry.

* Build debian buster image
```bash
docker build -t squid:latest -t squid:v$(poetry version | cut -d " " -f 2) -f buster.Dockerfile .
```

* Build alpine image
```bash
docker build -t squid:alpine-latest -t squid:alpine-v$(poetry version | cut -d " " -f 2) -f alpine.Dockerfile .
```

* Run on example
```bash
docker run -v $(pwd):/mnt -it squid:alpine-latest --help
```
