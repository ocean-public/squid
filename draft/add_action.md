# ADD Action
With this action you can add Tentacles, Tasks or Plugins to an Squid Project.

## Global options
* **--dry-run / -d** will print you what is going to add.
* **--std-in / -i** will open a input stream to the standard input checking for a valid Tentacle | Task | Plugin schema depending of the resource specify.
* **--editor / -e** will open the default terminal editor with a template of a Tentacle | Task | Plugin depending of the resource specify. If it is used with **--std-in / -i**, it would merge the template with the input.
* **--verbose / -v** will output all the input parameters.

## ADD TENTACLES Resource
When you add a tentacle the directory that you select as **path** would have a new folder name **.tentacle** with the following content:
* A config file **config.yaml** that has the options use in the creation.
* A logs folder to persists the output after the execution of a task on the tentacle.

This folder will be used for future features.

### Options
* **--name / -n** Name of the tentacle.
* **--path / -p** Path of the tentacle relative to current directory.
* **--type / -t** Type of the tentacle function | platform | model | ..., according to the squid ci initiative.

_**NOTE:** when using with --std-in or --editor options would merge the keys with these._

### Examples
* Base for example
```bash,prepare=base_tentacle
cd example
mkdir -p tentacles/test
mkdir -p tentacles/test/relative
cd tentacles/test
```

* Add relative directory as a tentacle
```bash,use=base_tentacle,exec
squid add tentacles --name="test" --type="test" --dry-run
```

* Add the current directory as a tentacle
```bash,use=base_tentacle,exec
squid add tentacles --name="test" --type="test" --path="./relative" --dry-run
```

* Trying to add an non existing directory as a tentacle
```bash,use=base_tentacle,exec=1
squid add tentacles --name="test" --type="test" --path="./non_existing" --dry-run
```

* Add the current directory as a tentacle with stdin
```bash,use=base_tentacle,exec
echo '{"name":"test", "type": "test", "path": "."}' | squid add tentacles --std-in --dry-run
```

* Add the current directory as a tentacle with stdin and override keys
```bash,use=base_tentacle,exec
echo '{"name":"test", "type": "test", "path": "."}' | squid add tentacles --name override --std-in --dry-run
```

* Add the current directory as a tentacle with stdin, override keys and using terminal editor
```bash
echo '{"name":"test", "type": "test", "path": "."}' | squid add tentacles --name override --std-in --editor --dry-run
```

* Clean Up
```bash,use=example,exec
rm -rf tentacles/test
```

## ADD TASKS Resource

### Options
* **--name / -n** Name of the task.
* **--category / -c** Category of the task a way to group tasks.
* **--envs** environment variables default values for this task. It has to be a valid object json string example '{"KEY_VAR":"VALUE_VAR"}'.
* **--cmds** list of commands use on this task. It has to be a valid json string with this format [{"args": ["<ARG1>", "<ARGN>"],"cmd":"<COMMAND TO RUN>"}].

_**NOTE:** when using with --std-in or --editor options would merge the keys with these._

### Examples
* Add task
```bash,use=example,exec
squid add tasks --name "echo" --category "shell" --envs {} --cmds '[{"cmd":"echo", "args":["TEST"]}]' --dry-run
```

* Trying adding a task with invalid json format
```bash,use=example,exec=1
squid add tasks --name "echo" --category "shell" --envs '{a}' --cmds '[{"cmd":"echo", "args":["TEST"]}]' --dry-run
```

* Add task with stdin
```bash,use=example,exec
echo '{"name": "echo", "category": "shell", "envs": {}, "cmds": [{"cmd":"echo", "args":["TEST"]}]}' | squid add tasks --std-in --dry-run
```

* Add task with stdin and override keys
```bash,use=example,exec
echo '{"name": "echo", "category": "shell", "envs": {}, "cmds": [{"cmd":"echo", "args":["TEST"]}]}' | squid add tasks -n override --std-in --dry-run
```

* Add task with stdin, override keys and using terminal editor
```bash
echo '{"name": "echo", "category": "shell", "envs": {}, "cmds": [{"cmd":"echo", "args":["TEST"]}]}' | squid add tasks -n override --std-in --editor --dry-run
```

## ADD PLUGINS Resource
When you add a plugin the directory that you select as **path** would have a new folder name **.squid-plugin** with a config file **config.yaml** that has the options use in the creation. This folder will be used for future features. To develop a plugin, please follow this guide (link).

### Options
* **--name / -n** Name of the plugin.
* **--source / -s** Source of the plugin. _Note: In the current version this is a local directory but on future versions is going to be used also as remote git repo._
* **--path / -p** Directory of the plugin. _Note: In the current version this is a existing local directory with the source code but on future versions is going to be used also as where the code will be downloaded._
* **--entrypoint** This is the reference to the module for register the plugin example <PYTHON_FILE>:<METHOD>.
* **--cls** Class of the plugin on this version the only valid value is **cli**
* **--attributes / -a** These are attributes for future use. The have to be a valid list json string [{"ATTRIBUTE": "VALUE"}]

_**NOTE:** when using with --std-in or --editor options would merge the keys with these._

### Examples
* Base for example
```bash,prepare=base_plugin
cd example
mkdir -p plugins/test
touch plugins/test/__init__.py
cat <<EOF  > plugins/test/test.py
def setup(registry):
    registry.entry("test", lambda: "TEST")
EOF
cd plugins/test
```

* Add plugin
```bash,use=base_plugin,exec
squid add plugins --name test --source . --path . --entrypoint "test:setup" --cls cli --attributes "[]" --dry-run
```

* Add plugin with stdin
```bash,use=base_plugin,exec
echo '{"attributes":[],"cls":"cli","entrypoint":"test:setup","name":"test","path":".","source":"."}' | squid add plugins --std-in --dry-run
```

* Add plugin with stdin and override keys
```bash,use=base_plugin,exec
echo '{"attributes":[],"cls":"cli","entrypoint":"test:setup","name":"test","path":".","source":"."}' | squid add plugins --name override --std-in --dry-run
```

* Add plugin with stdin, override keys and using terminal editor
```bash
echo '{"attributes":[],"cls":"cli","entrypoint":"test:setup","name":"test","path":".","source":"."}' | squid add plugins --name override --std-in --editor --dry-run
```

* Clean Up
```bash,use=example,exec
rm -rf plugins/test
```
