# REMOVE Action
With this action you can remove Tentacles, Tasks or Plugins from an Squid Project.

## Global options
* **--query / -q** the query statement to search Tentacles. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).

_**NOTE:** These action will unlink and remove folders managed by squid **.tentacle** **.squid-plugins**._

## REMOVE TENTACLES Resource

### Examples
* Base for example
```bash,prepare=remove_tentacle
cd example
mkdir -p to_remove/tentacle
cd to_remove/tentacle
squid add tentacles --name="to_remove" --type="test"
```

* Remove tentacle
```bash
echo "Current"
ls -la
squid get tentacles -o "$.[*].name" -r
echo "Remove"
squid remove tentacles --query "$.name == 'to_remove'"
echo "Validation"
ls -la
squid get tentacles -o "$.[*].name" -r
```

## REMOVE TASKS Resource

### Examples
* Base for example
```bash,prepare=remove_task
cd example
squid add tasks --name "to_remove_a" --category "shell" --envs {} --cmds '[{"cmd":"echo", "args":["TEST"]}]'
squid add tasks --name "to_remove_b" --category "shell" --envs {} --cmds '[{"cmd":"echo", "args":["TEST"]}]'
```

* Remove task
```bash
echo "Current"
squid get tasks -o "$.[*].name" -r
echo "Remove"
squid remove tasks --query "$.name.matches('to_remove_.*')"
echo "Validation"
squid get tasks -o "$.[*].name" -r
```

## REMOVE PLUGINS Resource

### Examples
* Base for example
```bash,prepare=remove_plugins
cd example
mkdir -p to_remove/plugin
cd to_remove/plugin
touch __init__.py
cat <<EOF  > plugin.py
def main(registry):
    registry.entry("test", lambda: "TEST")
EOF
squid add plugins --name to_remove --source . --path . --entrypoint "plugin:main" --cls cli --attributes "[]"
```

* Remove tentacle
```bash
echo "Current"
ls -la
squid get plugins -o "$.[*].name" -r
echo "Remove"
squid remove plugins --query "$.entrypoint == 'plugin:main'"
echo "Validation"
ls -la
squid get plugins -o "$.[*].name" -r
```

## WRAP-UP
```bash
rm -rf example/to_remove
```
