# GET Action
With this action you can get one or many Tentacle, Task or Plugin from an Squid Project.

## Global options
* **--yaml / -y** when present this flag indicates to format the output as a YAML instead of JSON.
* **--output_stmt / -o** use to filter the output according to the [JsonPath](https://goessner.net/articles/JsonPath/) specs and [JsonPath RW Ext](https://python-jsonpath-rw-ext.readthedocs.io/en/latest/readme.html#quick-start). You can use [this](https://jsonpath.com/) to test syntax.
* **--to_raw / -r** when present this flag indicates to transform output (YAML, JSON) to string.

## GET TENTACLE Resource
Return only one tentacle that satisfy the set options.

### Options
* **--name / -n** Name of the tentacle.
* **--deep** when present this flag indicates a deep resolving so the returning tentacle has all the dependencies.

### Examples
* Get a tentacle named **tentacle_a**
```bash,use=example,exec
squid get tentacle -n tentacle_a
```

* Get a tentacle named **tentacle_a** with YAML format
```bash,use=example,exec
squid get tentacle -n tentacle_a -y
```

* Get a tentacle named **tentacle_b** with all its dependencies
```bash,use=example,exec
squid get tentacle -n tentacle_b --deep
```

* Get a tentacle named **tentacle_b** with all its dependencies and show only the name of the tentacles directly dependant
```bash,use=example,exec
squid get tentacle -n tentacle_b --deep -o "$.tentacles[*].name"
```

* Get a tentacle named **tentacle_a** and show the string of the path
```bash,use=example,exec
squid get tentacle -n tentacle_a -o "$.path" -r
```

## GET TENTACLES Resource
Return a list of tentacles that satisfy the set options.

### Options
* **--query / -q** the query statement to search Tentacles. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).
* **--query_link / -l** the query statement to search the linking dependencies. When use with query the result will be the intersection of both results. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).
* **--deep** when present this flag indicates a deep resolving so the returning tentacles have all the dependencies.

### Examples
* Get all tentacles
```bash,use=example,exec
squid get tentacles
```

* Get tentacles that has **tentacle_b** on its path and return only the names
```bash,use=example,exec
squid get tentacles -q "$.path.matches('.*/tentacle_b/.*')" -o "$.[*].name"
```

* Get tentacles that has tasks dependencies and return all their dependencies.
```bash,use=example,exec
squid get tentacles -l "$.dependencies.any($.cls == 'task')" --deep
```

* Get tentacles that has tasks dependencies and return the name of the tasks as strings.
```bash,use=example,exec
squid get tentacles -l "$.dependencies.any($.cls == 'task')" --deep -o "$.[*].tasks[*].name" -r
```

## GET TASK Resource
Return only one task that satisfy the set options.

### Options
* **--name / -n** Name of the task.
* **--deep** when present this flag indicates a deep resolving so the returning task has all the dependencies.

### Examples
* Get a task named **task_a**
```bash,use=example,exec
squid get task -n task_a
```

* Get a task named **task_a** with YAML format
```bash,use=example,exec
squid get task -n task_a -y
```

* Get a task named **task_a** with all its dependencies
```bash,use=example,exec
squid get task -n task_a --deep
```

* Get a task named **tf_apply** with all its dependencies and show only the commands of the tasks directly dependant
```bash,use=example,exec
squid get task -n tf_apply --deep -o "$.tasks[*].cmds[*].cmd"
```

* Get a task named **tentacle_a** and show the default value of the env var.
```bash,use=example,exec
squid get task -n task_d -o "$.envs.ENGINE_CMDS_EXEC_SEQ" -r
```

## GET TASKS Resource
Return a list of tasks that satisfy the set options.

### Options
* **--query / -q** the query statement to search Tasks. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).
* **--query_link / -l** the query statement to search the linking dependencies. When use with query the result will be the intersection of both results. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).
* **--deep** when present this flag indicates a deep resolving so the returning tasks have all the dependencies.

### Examples
* Get all tasks
```bash,use=example,exec
squid get tasks
```

* Get tasks that has **terraform** as a command
```bash,use=example,exec
squid get tasks -q "$.cmds.any($.cmd == 'terraform')"
```

* Get tasks that has **terraform** as a command and there are tentacles dependant of them.
```bash,use=example,exec
 squid get tasks -q "$.cmds.any($.cmd == 'terraform')" -l "$.dependant.any($.cls == 'tentacle')"
```

* Get tasks that has **terraform** as a command and there are tentacles dependant of them and only return the names.
```bash,use=example,exec
squid get tasks -q "$.cmds.any($.cmd == 'terraform')" -l "$.dependant.any($.cls == 'tentacle')" -o "$.[*].name" -r
```

## GET PLUGIN Resource
Return only one plugin that satisfy the set options.

### Options
* **--name / -n** Name of the plugin.

### Examples
* Get a plugin named **plugin-exec**
```bash,use=example,exec
squid get plugin -n plugin-exec
```

* Get a plugin named **plugin-exec** with YAML format
```bash,use=example,exec
squid get plugin -n plugin-exec -y
```

* Get a plugin named **plugin-exec** and show only the entrypoint string
```bash,use=example,exec
squid get plugin -n plugin-exec -o "$.entrypoint" -r
```

## GET PLUGINS Resource
Return a list of plugins that satisfy the set options.

### Options
* **--query / -q** the query statement to search Tentacles. The syntax is based on [TinyDB queries](https://tinydb.readthedocs.io/en/stable/usage.html#queries).

### Examples
* Get all plugins
```bash,use=example,exec
squid get plugins
```

* Get plugins that has **main:setup** as their entrypoint and show only the names string
```bash,use=example,exec
squid get plugins -q "$.entrypoint == 'main:setup'" -o "$.[*].name" -r
```
