"""Test helpers funtions."""
import pytest

from squid.framework.utils import (
    compose_json,
    compose_yaml,
    descompose_yaml,
    to_obj,
    to_str,
)


def test_compose_json() -> None:
    """Test Util compose json."""
    assert compose_json({"test": True}) == '{\n  "test": true\n}'


def test_compose_yaml() -> None:
    """Test Util compose yaml."""
    assert compose_yaml({"test": True}) == "---\ntest: true\n"


def test_decompose_yaml() -> None:
    """Test Util decompose yaml."""
    gen_yaml = descompose_yaml("---\ntest: true\n")
    assert next(gen_yaml) == {"test": True}

    with pytest.raises(Exception) as exc:
        _ = next(gen_yaml)
    assert "StopIteration" in exc.typename

    gen_yaml = descompose_yaml("---\n- one\n- two\n---\ntest: three\n")
    assert next(gen_yaml) == "one"
    assert next(gen_yaml) == "two"
    assert next(gen_yaml) == {"test": "three"}

    with pytest.raises(Exception) as exc:
        _ = next(gen_yaml)
    assert "StopIteration" in exc.typename


def test_to_str() -> None:
    """Test Util to string."""
    assert to_str(["a", "b"]) == "a\nb"
    assert to_str({"test": "a"}) == '{\n  "test": "a"\n}'
    assert to_str(1) == "1"


def test_to_obj() -> None:
    """Test Util to object."""
    assert to_obj('{"a":true}') == {"a": True}
    assert to_obj('{"a":true}', validate=True)
    assert not to_obj('{"a":t}', validate=True)
    assert to_obj('{"a":t}') == '{"a":t}'
