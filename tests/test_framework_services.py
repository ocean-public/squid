"""Test Squid Services."""
import os
import re
from typing import cast, List, Tuple
from unittest.mock import Mock

from plumbum import local
from pyfakefs.fake_filesystem import FakeFilesystem
import pytest

from squid.framework.core.models.bases import (
    Plugin,
    SquidTable,
    TaskDAG,
    Tentacle,
    TentacleRef,
)
from squid.framework.core.models.responses import (
    DbResponseGet,
    DbResponseQuery,
    PersistenceResponsePath,
    ServiceResponse,
    StatusEnum,
)
from squid.framework.services import SingletonService
from squid.framework.services.db import SquidDb
from squid.framework.services.db.db_abc import DbABC
from squid.framework.services.db.db_tinydb import add, DbTinyDB, Query
from squid.framework.services.engine import SquidEngine
from squid.framework.services.engine.engine_abc import EngineABC
from squid.framework.services.engine.engine_luigi import (
    BashTask,
    BashTaskParameter,
    EngineLuigi,
)
from squid.framework.services.persistence import SquidPersistence
from squid.framework.services.registry import SquidRegistry


def test_singleton_service() -> None:
    """Test SingletonService."""

    class TestService(metaclass=SingletonService):
        """Test."""

        pass

    test_service_a = TestService()
    test_service_b = TestService()
    assert id(test_service_a) == id(test_service_b)


def test_abd() -> None:
    """Test ABC classes."""
    DbABC.__abstractmethods__ = set()  # type: ignore
    assert DbABC.query(DbABC(), "", "") is None  # type: ignore
    assert DbABC.insert(DbABC(), {}, "", False) is None  # type: ignore
    assert DbABC.update(DbABC(), {}, "", "") is None  # type: ignore
    assert DbABC.delete(DbABC(), "", "") is None  # type: ignore
    assert DbABC.get(DbABC(), "", "") is None  # type: ignore

    EngineABC.__abstractmethods__ = set()  # type: ignore
    assert EngineABC.execute_dag(EngineABC(), []) is None  # type: ignore


def test_engine_operations(
    tentacle_simple: List[Tentacle],
    tentacles_factory: Tuple[List[Tentacle], List[Tentacle], List[Tentacle]],
    mock_engine: Mock,
) -> None:
    """Test Squid Engine operations."""
    cwd = os.path.abspath(os.curdir)
    os.chdir(os.path.abspath(os.path.dirname(__file__)))

    squid_engine = SquidEngine(engine=mock_engine)
    (
        tentacle_w_tasks,
        tentacle_w_tasks_w_tentacles,
        tentacle_w_tasks_w_tentacles_v2,
    ) = tentacles_factory

    tasks_dag_v1 = SquidEngine.extract_taskdag_from_tentacles(tentacle_w_tasks)
    stub_dag_v1 = [
        {
            "name": "task_b",
            "envs": {},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_b"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 0,
            "tasks": [],
        }
    ]
    assert tasks_dag_v1 == [TaskDAG(**stub_item) for stub_item in stub_dag_v1]

    tasks_dag_2 = SquidEngine.extract_taskdag_from_tentacles(
        tentacle_w_tasks_w_tentacles
    )
    stub_dag_v2 = [
        {
            "name": "task_a",
            "envs": {"TEST": "env"},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_a"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 0,
            "tasks": [],
        },
        {
            "name": "task_b",
            "envs": {},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_b"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 1,
            "tasks": [],
        },
    ]
    assert tasks_dag_2 == [TaskDAG(**stub_item) for stub_item in stub_dag_v2]

    tasks_dag_3 = SquidEngine.extract_taskdag_from_tentacles(
        tentacle_w_tasks_w_tentacles_v2
    )
    stub_dag_v3 = [
        {
            "name": "task_c",
            "envs": {},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_c"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 0,
            "tasks": [],
        },
        {
            "name": "task_b",
            "envs": {},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_b"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 1,
            "tasks": [],
        },
        {
            "name": "task_d",
            "envs": {},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_d"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 1,
            "tasks": [
                {
                    "name": "task_a",
                    "envs": {"TEST": "env"},
                    "wd": ".",
                    "cmds": [{"cmd": "echo", "args": ["task_a"]}],
                    "logpath": "./.tentacle/logs",
                    "exec_order": 1,
                    "tasks": [],
                }
            ],
        },
        {
            "name": "task_e",
            "envs": {},
            "wd": ".",
            "cmds": [{"cmd": "echo", "args": ["task_e"]}],
            "logpath": "./.tentacle/logs",
            "exec_order": 2,
            "tasks": [],
        },
    ]
    assert tasks_dag_3 == [TaskDAG(**stub_item) for stub_item in stub_dag_v3]

    squid_engine_response = squid_engine.run(tentacles=tentacle_simple)
    assert isinstance(squid_engine_response, ServiceResponse)

    assert isinstance(
        EngineLuigi._build_task(
            [
                {
                    "name": "task",
                    "envs": {},
                    "wd": "./",
                    "cmds": [{"cmd": "echo", "args": ["test"]}],
                    "logpath": "task.log",
                }
            ]
        )[0],
        BashTask,
    )

    bash_task = BashTask(
        cmds=[{"cmd": "echo", "args": ["test"]}],
        logpath="task4.log",
        upstream_task=None,
        wd=".",
        envs={},
    )
    assert bash_task.priority == 0

    bash_task_nested = BashTask(
        cmds=[{"cmd": "echo", "args": ["test"]}],
        logpath="task4.log",
        upstream_task=[bash_task],
        wd=".",
        envs={},
        exec_order=3,
    )
    assert bash_task_nested.priority == 3
    assert bash_task_nested.requires() == (bash_task,)

    bash_task_parameter = BashTaskParameter()
    assert bash_task_parameter.parse() is None
    assert bash_task_parameter.parse(bash_task) == bash_task

    cmds = [{"cmd": "echo", "args": ["test"]}]
    cmds_multiple = [
        {"cmd": "echo", "args": ["test"]},
        {"cmd": "echo", "args": ["test_two"]},
    ]
    local_cmd = local["echo"]["test"]
    local_cmds = [local["echo"]["test"], local["echo"]["test_two"]]
    local_cmds_pipe = [(local["echo"]["test"] | local["echo"]["test_two"])]

    local.env["ENGINE_CMDS_EXEC_SEQ"] = "isolated"
    gen_cmd = BashTask._parse_cmds(cmds)
    assert isinstance(gen_cmd, list)
    assert len(gen_cmd) == 1
    assert isinstance(gen_cmd[0], type(local_cmd))
    assert gen_cmd[0].args == local_cmd.args
    assert gen_cmd[0].cmd.executable == local_cmd.cmd.executable

    local.env["ENGINE_CMDS_EXEC_SEQ"] = "pipe"
    gen_cmd = BashTask._parse_cmds(cmds)
    assert isinstance(gen_cmd, list)
    assert len(gen_cmd) == 1
    assert isinstance(gen_cmd[0], type(local_cmd))
    assert gen_cmd[0].args == local_cmd.args
    assert gen_cmd[0].cmd.executable == local_cmd.cmd.executable

    local.env["ENGINE_CMDS_EXEC_SEQ"] = "isolated"
    gen_cmds = BashTask._parse_cmds(cmds_multiple)
    assert isinstance(gen_cmds[0], type(local_cmds[0]))
    assert gen_cmds[0].args == local_cmds[0].args
    assert gen_cmds[0].cmd.executable == local_cmds[0].cmd.executable
    assert isinstance(gen_cmds[1], type(local_cmds[1]))
    assert gen_cmds[1].args == local_cmds[1].args
    assert gen_cmds[1].cmd.executable == local_cmds[1].cmd.executable

    local.env["ENGINE_CMDS_EXEC_SEQ"] = "pipe"
    gen_cmds = BashTask._parse_cmds(cmds_multiple)
    assert len(gen_cmds) == 1
    assert isinstance(gen_cmds[0], type(local_cmds_pipe[0]))
    assert gen_cmds[0].srccmd.args == local_cmds_pipe[0].srccmd.args
    assert gen_cmds[0].srccmd.cmd.executable == local_cmds_pipe[0].srccmd.cmd.executable
    assert gen_cmds[0].dstcmd.args == local_cmds_pipe[0].dstcmd.args
    assert gen_cmds[0].dstcmd.cmd.executable == local_cmds_pipe[0].dstcmd.cmd.executable

    os.chdir(cwd)


def test_db_operations(mock_db: Mock, fs: FakeFilesystem) -> None:
    """Test SquidDb operations."""
    squid_db = SquidDb()
    squid_db._db = mock_db

    stmt = "($.name == 'head') | ($.type == 'test')"
    stmt_parsed = "(SquidQuery.name == 'head') | (SquidQuery.type == 'test')"
    assert SquidDb._query_parser(stmt, SquidTable.TENTACLES) == stmt_parsed
    with pytest.raises(Exception) as exc:
        SquidDb._query_parser("$.not_a_field == 'test'", SquidTable.TENTACLES)
    assert "Field not_a_field not allowed." in str(exc.value)

    stmt = "($.name == 'tentacle_a') | ($.type == 'test')"

    svc_resp = squid_db.query(stmt=stmt, table=SquidTable.TENTACLES)
    assert svc_resp.status == StatusEnum.success
    tentacles = cast(DbResponseQuery, svc_resp.data).query
    assert tentacles == [
        TentacleRef.parse_obj({"name": "tentacle_a", "type": "test", "path": "./"})
    ]

    svc_resp = squid_db.get(stmt=stmt, table=SquidTable.TENTACLES)
    assert svc_resp.status == StatusEnum.success
    tentacle = cast(DbResponseGet, svc_resp.data).get
    assert isinstance(tentacle, TentacleRef)

    fs.create_dir("/home/test/.squid")
    fs.cwd = "/home/test/a"

    query = Query().name == "head"
    assert DbTinyDB._convert_query("SquidQuery.name == 'head'") == query
    with pytest.raises(SyntaxError) as exc:
        DbTinyDB._convert_query("error - not eval")
    assert bool(re.compile("invalid syntax .*").match(str(exc.value)))

    assert isinstance(
        DbTinyDB._convert_operations({"category": {"op": "add", "value": "!"}}),
        type(add("category", "!")),
    )
    assert DbTinyDB._convert_operations({"category": {"op": "not_a_op"}}) == {}
    assert DbTinyDB._convert_operations({"category": "test"}) == {"category": "test"}

    with pytest.raises(Exception) as exc:
        DbTinyDB._convert_operations(
            {"category": {"op": "add", "value": "!"}, "type": "test"}
        )
    assert "Multiple operations or mixing is not supported." in str(exc.value)

    db_tinydb = DbTinyDB()
    assert db_tinydb.insert(record={"name": "test"}, table="test") == 1
    assert db_tinydb.get(stmt="SquidQuery.name == 'test'", table="test") == {
        "name": "test"
    }
    assert db_tinydb.update(
        kvs={"key": "a"}, stmt="SquidQuery.name == 'test'", table="test"
    ) == [1]
    assert db_tinydb.query(stmt="SquidQuery.name == 'test'", table="test") == [
        {"name": "test", "key": "a"}
    ]
    assert db_tinydb.insert(record={"name": "test"}, table="test", upsert=True) == 1
    with pytest.raises(Exception) as exc:
        db_tinydb.insert(record={"name": "test"}, table="test")
    assert bool(re.compile("Record already exists .*").match(str(exc.value)))
    assert db_tinydb.delete(stmt="SquidQuery.name == 'test'", table="test") == [1]


def test_persistence_operations(fs: FakeFilesystem) -> None:
    """Test SquidPersistence operations."""
    fs.create_dir("/home/test/.squid")
    fs.create_dir("/home/test/a/.tentacle")
    fs.create_dir("/home/test/plugin_test")
    fs.create_dir("/home/test/a/b/c")
    fs.create_file("/home/test/a/b/c/test.txt")

    with pytest.raises(Exception) as exc:
        SquidPersistence._find_by_pattern_from_path(
            current_path="/home", pattern="^.squid$"
        )
    assert "Can't find path from /home with pattern ^.squid$" in str(exc.value)

    squid_persistence = SquidPersistence()

    squid_path = cast(
        PersistenceResponsePath,
        squid_persistence.get_root_path(current_path="/home/test/a/b/c").data,
    ).path
    assert squid_path == "/home/test"

    fs.cwd = "/home/test/a/b/c"
    squid_path = cast(
        PersistenceResponsePath, squid_persistence.get_root_path().data
    ).path
    assert squid_path == "/home/test"

    squid_path = cast(
        PersistenceResponsePath,
        squid_persistence.get_root_path(current_path="/home/test/a/b/c/test.txt").data,
    ).path
    assert squid_path == "/home/test"

    squid_path = cast(
        PersistenceResponsePath,
        squid_persistence.get_squid_path(current_path="/home/test/a/b/c").data,
    ).path
    assert squid_path == "/home/test/.squid"

    fs.cwd = "/home/test/a/b/c"
    squid_path = cast(
        PersistenceResponsePath, squid_persistence.get_squid_path().data
    ).path
    assert squid_path == "/home/test/.squid"

    tentacle_path = cast(
        PersistenceResponsePath,
        squid_persistence.get_tentacle_path(current_path="/home/test/a/b/c").data,
    ).path
    assert tentacle_path == "/home/test/a/.tentacle"

    fs.cwd = "/home/test/a/b/c"
    tentacle_path = cast(
        PersistenceResponsePath, squid_persistence.get_tentacle_path().data
    ).path
    assert tentacle_path == "/home/test/a/.tentacle"

    tentacle_path = cast(
        PersistenceResponsePath,
        squid_persistence.get_tentacle_path(
            current_path="/home/test/a/b/c/test.txt"
        ).data,
    ).path
    assert tentacle_path == "/home/test/a/.tentacle"

    SquidPersistence.dump_to_file(item={"name": "test"}, path="/home/test/a/b/c")
    assert os.path.exists("/home/test/a/b/c/dump.yaml")

    SquidPersistence.dump_to_file(
        item={"name": "test"}, path="/home/test/a/b/c", fmt="JSON"
    )
    assert os.path.exists("/home/test/a/b/c/dump.json")

    with pytest.raises(Exception) as exc:
        SquidPersistence.dump_to_file(item={"name": "test"}, path="/not/dir")
    assert "Path does not exists" in str(exc.value)

    with pytest.raises(Exception) as exc:
        SquidPersistence.dump_to_file(
            item={"name": "test"}, path="/home/test/a/b/c", fmt="TXT"
        )
    assert "Format is not supported try YAML or JSON" in str(exc.value)

    fs.cwd = "/home/test/a"
    tentacle_ref = TentacleRef(name="test_a", type="test", path=".")
    squid_persistence.make_tentacle_path(tentacle_ref)
    assert os.path.exists("/home/test/a/.tentacle")
    assert os.path.exists("/home/test/a/.tentacle/logs")

    tentacle_ref.path = "./a"
    squid_persistence.remove_tentacle_path(tentacle_ref)
    assert not os.path.exists("/home/test/a/.tentacle")

    with pytest.raises(Exception) as exc:
        tentacle_ref = TentacleRef(name="test_a", type="test", path="/not/dir")
        squid_persistence.make_tentacle_path(tentacle_ref)
    assert "Tentacle path /not/dir does not exists." in str(exc.value)

    fs.cwd = "/home/test"
    plugin = Plugin.parse_obj(
        {
            "name": "plugin_test",
            "source": "./plugin_test",
            "cls": "cli",
            "entrypoint": "plugin_test:setup",
            "attributes": [],
            "path": "./plugin_test",
        }
    )
    squid_persistence.make_squid_plugin_path(plugin)
    assert os.path.exists("/home/test/plugin_test/.squid-plugin")

    squid_persistence.remove_squid_plugin_path(plugin)
    assert not os.path.exists("/home/test/plugin_test/.squid-plugin")

    with pytest.raises(Exception) as exc:
        plugin.path = "/not/dir"
        squid_persistence.make_squid_plugin_path(plugin)
    assert "Plugin path /not/dir does not exists." in str(exc.value)


def test_registry_operations() -> None:
    """Test SquidPersistence operations."""
    cwd = os.path.abspath(os.curdir)
    os.chdir(os.path.abspath(os.path.dirname(__file__)))
    squid_registry = SquidRegistry()
    _ = [plugin_source.cleanup() for plugin_source in squid_registry._plugin_sources]

    plugin = Plugin(
        name="test",
        source=os.path.join(os.path.abspath(os.path.dirname(__file__)), "plugin_test"),
        entrypoint="plugin_test:setup",
        path=os.path.join(os.path.abspath(os.path.dirname(__file__)), "plugin_test"),
    )
    _ = [plugin_source.cleanup() for plugin_source in squid_registry._plugin_sources]
    squid_registry._plugin_sources = None  # type: ignore
    squid_registry.discovery([plugin])
    assert "title" in squid_registry.entries
    assert squid_registry.entries["title"]("test") == "Test"

    squid_registry._plugin_sources = []
    with pytest.raises(Exception) as exc:
        squid_registry.context()
    assert "Run discovery stage first." in str(exc.value)

    os.chdir(cwd)
