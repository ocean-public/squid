"""Test Core Commands."""
import json
import os
from typing import Tuple
from unittest.mock import Mock

from pyfakefs.fake_filesystem import FakeFilesystem
import pytest

from squid.framework.core import (
    AddPlugins,
    AddTasks,
    AddTentacles,
    CreateLink,
    DbTinyDB,
    DumpPlugin,
    DumpTentacle,
    Execute,
    GetPlugin,
    GetPlugins,
    GetRegistryEntries,
    GetTask,
    GetTasks,
    GetTentacle,
    GetTentacles,
    Head,
    Init,
    InvokeCtx,
    InvokeFn,
    RemoveLink,
    RemovePlugins,
    RemoveTasks,
    RemoveTentacles,
    Squid,
    SquidDb,
    SquidEngine,
    UpdatePlugins,
    UpdateTasks,
    UpdateTentacles,
)
from squid.framework.core.models.bases import (
    Plugin,
    Task,
    TaskCmd,
    TaskRef,
    Tentacle,
    TentacleRef,
)


def test_init_command(fs: FakeFilesystem) -> None:
    """Test Init command."""
    fs.create_dir("/home/test")
    fs.cwd = "/home/test"
    squid = Squid()
    squid_head = Head()

    with pytest.raises(Exception) as exc:
        squid_head.execute(Init(squid, path="/not/dir"))
    assert "Path: /not/dir is not a directory or it does not exists." in str(exc.value)

    assert squid_head.execute(Init(squid, path=fs.cwd))
    assert os.path.exists("/home/test/.squid")
    assert os.path.exists("/home/test/.squid/logs")
    assert os.path.exists("/home/test/.squid/plugins")
    assert os.path.exists("/home/test/.squid/plugins/built-in")

    assert squid_head.undo()
    assert not os.path.exists("/home/test/.squid")

    fs.create_dir("/home/test/.squid")
    with pytest.raises(Exception) as exc:
        squid_head.execute(Init(squid, path=fs.cwd))
    assert "There is already a Squid project initialize." in str(exc.value)

    fs.rmdir("/home/test/.squid")
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Squid project is not inittialize." in str(exc.value)


def test_add_command(init_squid: Tuple[FakeFilesystem, Head, Squid]) -> None:
    """Test Add command."""
    fs, squid_head, squid = init_squid
    SquidDb()._db = DbTinyDB()

    fs.cwd = "/home/test/a"
    tentacle_ref = TentacleRef(name="test_a", type="test", path=".")
    assert squid_head.execute(AddTentacles(squid, [tentacle_ref]))
    assert os.path.exists("/home/test/a/.tentacle")
    assert os.path.exists("/home/test/a/.tentacle/logs")
    assert os.path.exists("/home/test/a/.tentacle/config.yaml")

    assert squid_head.undo()
    assert not os.path.exists("/home/test/a/.tentacle")

    fs.cwd = "/home/test/b"
    tentacle = Tentacle(
        name="test_b", type="test", path=".", attributes=[], tentacles=[], tasks=[]
    )
    assert squid_head.execute(AddTentacles(squid, [tentacle]))
    assert squid_head.undo()

    assert os.path.exists("/home/test/.squid")
    assert os.path.exists("/home/test/.squid/db.json")

    task_ref = TaskRef(
        name="test", category="echo", envs={}, cmds=[TaskCmd(cmd="echo", args=["test"])]
    )
    assert squid_head.execute(AddTasks(squid, [task_ref]))
    assert squid_head.undo()

    task = Task(
        name="test", category="echo", envs={}, cmds=[TaskCmd(cmd="echo", args=["test"])]
    )
    assert squid_head.execute(AddTasks(squid, [task]))
    assert squid_head.undo()

    fs.cwd = "/home/test"
    plugin = Plugin(name="test", source=".", path=".", entrypoint="test:main")
    assert squid_head.execute(AddPlugins(squid, [plugin]))
    assert squid_head.undo()


def test_get_command(seed_squid: Tuple[FakeFilesystem, Head, Squid]) -> None:
    """Test Get command."""
    _, squid_head, squid = seed_squid
    SquidDb()._db = DbTinyDB()

    assert squid_head.execute(GetTentacle(squid, "not_a_tentacle")) == {}

    tentacle_json = {"name": "test_a", "type": "test", "path": "./tentacle_a"}
    assert squid_head.execute(GetTentacle(squid, "test_a")).dict() == tentacle_json
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    tentacles_json = [
        TentacleRef.parse_obj(
            {"name": "test_a", "type": "test", "path": "./tentacle_a"}
        ),
        TentacleRef.parse_obj(
            {"name": "test_b", "type": "test", "path": "./tentacle_b"}
        ),
    ]
    assert squid_head.execute(GetTentacles(squid, "$.type == 'test'")) == tentacles_json
    with pytest.raises(Exception) as exc:
        squid_head.execute(GetTentacles(squid, "$.type == 'test'", "object"))
    assert "Output choice is not supported object." in str(exc.value)
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    tentacle = Tentacle.parse_obj(
        {
            "name": "test_a",
            "type": "test",
            "path": "./tentacle_a",
            "tentacles": [
                {
                    "name": "test_b",
                    "type": "test",
                    "path": "./tentacle_b",
                    "tentacles": [],
                    "tasks": [],
                }
            ],
            "tasks": [],
        }
    )
    assert squid_head.execute(GetTentacle(squid, "test_a", True)) == tentacle

    tentacles = [
        tentacle,
        Tentacle.parse_obj(
            {
                "name": "test_b",
                "type": "test",
                "path": "./tentacle_b",
                "tentacles": [],
                "tasks": [],
            }
        ),
    ]
    assert (
        squid_head.execute(GetTentacles(squid, "$.type == 'test'", "tree")) == tentacles
    )

    assert squid_head.execute(
        GetTentacles(
            squid,
            "$.name.matches('.*')",
            "list",
            "$.dependencies.any($.name == 'test_b')",
        )
    ) == [
        TentacleRef.parse_obj(
            {"name": "test_a", "type": "test", "path": "./tentacle_a"}
        )
    ]

    assert squid_head.execute(
        GetTentacles(
            squid, "$.name.matches('.*')", "tree", "$.dependant.any($.name == 'test_a')"
        )
    ) == [
        Tentacle.parse_obj(
            {
                "name": "test_b",
                "type": "test",
                "path": "./tentacle_b",
                "tentacles": [],
                "tasks": [],
            }
        )
    ]

    task_json = {
        "name": "test_a",
        "category": "echo",
        "envs": {},
        "cmds": [{"cmd": "echo", "args": ["test"]}],
    }
    assert squid_head.execute(GetTask(squid, "test_a")).dict() == task_json
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    tasks_json = [
        TaskRef.parse_obj(
            {
                "name": "test_a",
                "category": "echo",
                "envs": {},
                "cmds": [{"cmd": "echo", "args": ["test"]}],
            }
        ),
        TaskRef.parse_obj(
            {
                "name": "test_b",
                "category": "echo",
                "envs": {},
                "cmds": [{"cmd": "echo", "args": ["test"]}],
            }
        ),
    ]
    assert (
        squid_head.execute(GetTasks(squid, "$.cmds.any( $.cmd == 'echo' )"))
        == tasks_json
    )
    with pytest.raises(Exception) as exc:
        squid_head.execute(GetTasks(squid, "$.cmds.any( $.cmd == 'echo' )", "object"))
    assert "Output choice is not supported object." in str(exc.value)
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    task = Task.parse_obj(
        {
            "name": "test_b",
            "category": "echo",
            "envs": {},
            "cmds": [{"cmd": "echo", "args": ["test"]}],
            "tentacles": [],
            "tasks": [
                {
                    "name": "test_a",
                    "category": "echo",
                    "envs": {},
                    "cmds": [{"cmd": "echo", "args": ["test"]}],
                    "tentacles": [],
                    "tasks": [],
                }
            ],
        }
    )
    assert squid_head.execute(GetTask(squid, "test_b", True)) == task

    tasks = [
        Task.parse_obj(
            {
                "name": "test_a",
                "category": "echo",
                "envs": {},
                "cmds": [{"cmd": "echo", "args": ["test"]}],
                "tentacles": [],
                "tasks": [],
            }
        ),
        task,
    ]
    assert (
        squid_head.execute(GetTasks(squid, "$.cmds.any( $.cmd == 'echo' )", "tree"))
        == tasks
    )

    plugin_json = {
        "name": "plugin_test",
        "source": "./plugin_test",
        "cls": "cli",
        "entrypoint": "plugin_test:setup",
        "attributes": [],
        "path": "./plugin_test",
    }
    assert squid_head.execute(GetPlugin(squid, "plugin_test")).dict() == plugin_json
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    plugins_json = [
        Plugin.parse_obj(
            {
                "name": "plugin_test",
                "source": "./plugin_test",
                "cls": "cli",
                "entrypoint": "plugin_test:setup",
                "attributes": [],
                "path": "./plugin_test",
            }
        )
    ]
    assert (
        squid_head.execute(
            GetPlugins(
                squid,
                "($.path == './plugin_test') | ($.entrypoint == 'plugin_test:setup')",
            )
        )
        == plugins_json
    )
    with pytest.raises(Exception) as exc:
        squid_head.execute(
            GetPlugins(
                squid,
                "($.path == '.squid/plugins') | ($.entrypoint == 'test:main')",
                "object",
            )
        )
    assert "Output choice is not supported object." in str(exc.value)
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)


def test_update_command(seed_squid: Tuple[FakeFilesystem, Head, Squid]) -> None:
    """Test update command."""
    _, squid_head, squid = seed_squid
    SquidDb()._db = DbTinyDB()

    assert squid_head.execute(
        UpdateTentacles(squid, "$.name == 'test_a'", {"type": "echo"})
    )
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["tentacles"]["1"]["type"] == "echo"

    assert squid_head.undo()
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["tentacles"]["1"]["type"] == "test"

    with pytest.raises(Exception) as exc:
        squid_head.execute(
            UpdateTentacles(squid, "$.name == 'test_a'", {"name": "echo"})
        )
    assert "Path or Name cannot be modified after creation." in str(exc.value)

    assert squid_head.execute(
        UpdateTasks(squid, "$.name == 'test_a'", {"category": "bash"})
    )
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["tasks"]["1"]["category"] == "bash"

    assert squid_head.undo()
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["tasks"]["1"]["category"] == "echo"

    with pytest.raises(Exception) as exc:
        squid_head.execute(UpdateTasks(squid, "$.name == 'test_a'", {"name": "bash"}))
    assert "Name cannot be modified after creation." in str(exc.value)

    assert squid_head.execute(
        UpdatePlugins(squid, "$.name == 'plugin_test'", {"entrypoint": "handler:main"})
    )
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["plugins"]["1"]["entrypoint"] == "handler:main"

    assert squid_head.undo()
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["plugins"]["1"]["entrypoint"] == "plugin_test:setup"

    with pytest.raises(Exception) as exc:
        squid_head.execute(
            UpdatePlugins(squid, "$.name == 'plugin_test'", {"source": "handler:main"})
        )
    assert "Path or Name or Source cannot be modified after creation." in str(exc.value)


def test_remove_command(seed_squid: Tuple[FakeFilesystem, Head, Squid]) -> None:
    """Test remove command."""
    fs, squid_head, squid = seed_squid
    SquidDb()._db = DbTinyDB()

    fs.create_dir("/home/test/tentacle_a/.tentacle")
    assert squid_head.execute(RemoveTentacles(squid, "$.name == 'test_a'"))
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert "1" not in db_json["tentacles"]

    assert squid_head.undo()
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["tentacles"]["3"]["name"] == "test_a"

    assert squid_head.execute(RemoveTasks(squid, "$.category == 'echo'"))
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["tasks"] == {}

    assert squid_head.undo()
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert "3" in db_json["tasks"]
        assert "4" in db_json["tasks"]

    fs.create_dir("/home/test/plugin_test/.squid-plugin")
    assert squid_head.execute(RemovePlugins(squid, "$.name == 'plugin_test'"))
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert len(db_json["plugins"].keys()) == 0

    assert squid_head.undo()
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert len(db_json["plugins"].keys()) == 1


def test_link_command(seed_squid: Tuple[FakeFilesystem, Head, Squid]) -> None:
    """Test remove command."""
    fs, squid_head, squid = seed_squid
    SquidDb()._db = DbTinyDB()

    tentacle_a = TentacleRef.parse_obj(
        {"name": "test_a", "type": "test", "path": "./tentacle_a"}
    )
    tentacle_b = TentacleRef.parse_obj(
        {"name": "test_b", "type": "test", "path": "./tentacle_b"}
    )
    assert squid_head.execute(CreateLink(squid, [tentacle_b], [tentacle_a]))
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["links"]["3"]["name"] == "test_a"

    assert squid_head.execute(CreateLink(squid, [tentacle_a], [tentacle_b]))
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["links"]["3"]["dependant"] == []

    assert squid_head.undo(1)
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["links"]["3"]["dependencies"] == []

    task_a = TaskRef.parse_obj(
        {
            "name": "test_a",
            "category": "echo",
            "envs": {},
            "cmds": [{"cmd": "echo", "args": ["test"]}],
        }
    )
    task_b = TaskRef.parse_obj(
        {
            "name": "test_b",
            "category": "echo",
            "envs": {},
            "cmds": [{"cmd": "echo", "args": ["test"]}],
        }
    )
    assert squid_head.execute(RemoveLink(squid, [task_a], [task_b]))
    with open("/home/test/.squid/db.json") as file_db_json:
        db_json = json.load(file_db_json)
        assert db_json["links"]["1"]["name"] in ["test_a", "test_b"]

    assert squid_head.undo()
    # TODO: -Validate


def test_execute_command(
    seed_squid: Tuple[FakeFilesystem, Head, Squid], mock_engine: Mock
) -> None:
    """Test execute command."""
    _, squid_head, squid = seed_squid
    SquidEngine()._engine = mock_engine

    tentacles = [
        Tentacle.parse_obj(
            {
                "name": "test_a",
                "type": "test",
                "path": "./tentacle_a",
                "tentacles": [],
                "tasks": [
                    {
                        "name": "test_a",
                        "category": "echo",
                        "envs": {},
                        "cmds": [{"cmd": "echo", "args": ["test"]}],
                    }
                ],
            }
        ),
        Tentacle.parse_obj(
            {
                "name": "test_b",
                "type": "test",
                "path": "./tentacle_b",
                "tentacles": [],
                "tasks": [
                    {
                        "name": "test_b",
                        "category": "echo",
                        "envs": {},
                        "cmds": [{"cmd": "echo", "args": ["test"]}],
                    }
                ],
            }
        ),
    ]
    assert squid_head.execute(Execute(squid, tentacles, [])) == {
        "summary": "TEST",
        "exec_tree": ["TEST"],
    }

    tasks = [
        Task.parse_obj(
            {
                "name": "test_replace",
                "category": "echo",
                "envs": {},
                "cmds": [{"cmd": "echo", "args": ["test"]}],
            }
        )
    ]
    assert squid_head.execute(Execute(squid, tentacles, tasks)) == {
        "summary": "TEST",
        "exec_tree": ["TEST"],
    }

    tentacles.append(
        Tentacle.parse_obj(
            {
                "name": "test_c",
                "type": "test",
                "path": "./tentacle_c",
                "tentacles": [
                    {
                        "name": "test_d",
                        "type": "test",
                        "path": "./tentacle_d",
                        "tentacles": [],
                        "tasks": [
                            {
                                "name": "test_d",
                                "category": "echo",
                                "envs": {},
                                "cmds": [{"cmd": "echo", "args": ["test"]}],
                            }
                        ],
                    }
                ],
                "tasks": [
                    {
                        "name": "test_c",
                        "category": "echo",
                        "envs": {},
                        "cmds": [{"cmd": "echo", "args": ["test"]}],
                    }
                ],
            }
        )
    )
    assert squid_head.execute(Execute(squid, tentacles, tasks)) == {
        "summary": "TEST",
        "exec_tree": ["TEST"],
    }

    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)


def test_registry_commands() -> None:
    """Test Registry Commands."""
    cwd = os.path.abspath(os.curdir)
    os.chdir(os.path.abspath(os.path.dirname(__file__)))
    squid = Squid()
    squid_head = Head()

    plugin = Plugin(
        name="test",
        source=os.path.join(os.path.abspath(os.path.dirname(__file__)), "plugin_test"),
        entrypoint="plugin_test:setup",
        path=os.path.join(os.path.abspath(os.path.dirname(__file__)), "plugin_test"),
    )

    assert squid_head.execute(GetRegistryEntries(squid, [plugin])) == ["title"]
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    fn = squid_head.execute(InvokeFn(squid, "title", [plugin]))
    assert fn("test") == "Test"
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    with pytest.raises(Exception) as exc:
        _ = squid_head.execute(InvokeFn(squid, "not_entry", [plugin]))
    assert "Entry not_entry is not registered." in str(exc.value)

    with squid_head.execute(InvokeCtx(squid, "test", [plugin])):
        from squid.plugins import plugin_test  # type: ignore
    assert plugin_test.make_title("test") == "Test"

    with pytest.raises(Exception) as exc:
        with squid_head.execute(InvokeCtx(squid, "not_exists", [plugin])):
            from squid.plugins import not_exists  # type: ignore
        assert not_exists.nothing("test") == "Test"
    assert "Name not_exists is not register." in str(exc.value)

    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert "Operation not supported." in str(exc.value)

    os.chdir(cwd)


def test_dump_command(seed_squid: Tuple[FakeFilesystem, Head, Squid]) -> None:
    """Test dump command."""
    fs, squid_head, squid = seed_squid
    SquidDb()._db = DbTinyDB()

    fs.create_dir("/home/test/tentacle_a/.tentacle")
    fs.create_dir("/home/test/plugin_test/.squid-plugin")
    fs.cwd = "/home/test/a"

    squid_head.execute(DumpTentacle(squid, "test_a"))
    assert os.path.exists("/home/test/tentacle_a/.tentacle/config.yaml")

    squid_head.undo()
    assert not os.path.exists("/home/test/tentacle_a/.tentacle/config.yaml")

    squid_head.execute(DumpTentacle(squid, "test_a"))
    os.remove("/home/test/tentacle_a/.tentacle/config.yaml")
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert (
        "DumpTentacle -> undo: No such file or directory in the fake filesystem"
        in str(exc.value)
    )

    squid_head.execute(DumpPlugin(squid, "plugin_test"))
    assert os.path.exists("/home/test/plugin_test/.squid-plugin/config.yaml")

    squid_head.undo()
    assert not os.path.exists("/home/test/plugin_test/.squid-plugin/config.yaml")

    squid_head.execute(DumpPlugin(squid, "plugin_test"))
    os.remove("/home/test/plugin_test/.squid-plugin/config.yaml")
    with pytest.raises(Exception) as exc:
        squid_head.undo()
    assert (
        "DumpPlugin -> undo: No such file or directory in the fake filesystem"
        in str(exc.value)
    )
