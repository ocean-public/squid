"""Test Squid CLI Interface."""
import json
import os
import re
from typing import Dict
from unittest.mock import Mock

from click.testing import CliRunner
import pytest
from pytest_mock import MockFixture

cwd = os.path.abspath(os.curdir)
os.chdir(os.path.abspath(os.path.dirname(__file__)))
from squid.interfaces.console.callbacks import (  # noqa: E402
    _execute_transformation,
    std_in_callback,
)
from squid.interfaces.console.cli import squid_main as squid  # noqa: E402

os.chdir(cwd)


@pytest.fixture
def runner() -> CliRunner:
    """Fixture for CLI."""
    return CliRunner()


class MockResponse:
    """MockResponse."""

    def __init__(self, json_data: Dict, status_code: int):
        """Constructor."""
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        """Response json."""
        return self.json_data


def mock_response_ok(*args, **kwargs) -> MockResponse:
    """Mock response ok."""
    return MockResponse(
        {
            "success": {"total": 1},
            "contents": {
                "quotes": [
                    {
                        "quote": "Man invented language to satisfy his deep need to complain.",
                        "length": "59",
                        "author": "Lily Tomlin",
                        "tags": [
                            "age",
                            "apothegm",
                            "complain",
                            "funny",
                            "humor",
                            "language",
                            "man",
                        ],
                        "category": "funny",
                        "language": "en",
                        "date": "2020-05-22",
                        "permalink": "https://theysaidso.com/quote/lily-tomlin-man-invented-language-to-satisfy-his-deep-need-to-complain",
                        "id": "QyatVklvb_IsRgZL7qcj7geF",
                        "background": "https://theysaidso.com/img/qod/qod-funny.jpg",
                        "title": "Funny Quote of the day",
                    }
                ]
            },
            "baseurl": "https://theysaidso.com",
            "copyright": {"year": 2022, "url": "https://theysaidso.com"},
        },
        200,
    )


def mock_response_too_many_req(*args, **kwargs) -> MockResponse:
    """Mock response too many requests."""
    return MockResponse(
        {
            "error": {
                "code": 429,
                "message": "Too Many Requests: Rate limit of 10 requests per hour exceeded. Please wait for 59 minutes and 59 seconds.",
            }
        },
        429,
    )


@pytest.fixture
def mock_requests_get_too_many_rq(mocker: MockFixture) -> Mock:
    """Fixture for mocking requests.get."""
    mock = mocker.patch("requests.get", side_effect=mock_response_too_many_req)
    return mock


@pytest.fixture
def mock_requests_get_ok(mocker: MockFixture) -> Mock:
    """Fixture for mocking requests.get."""
    mock = mocker.patch("requests.get", side_effect=mock_response_ok)
    return mock


def test_squid_init(runner: CliRunner) -> None:
    """Test CLI plugins."""
    with runner.isolated_filesystem():
        result = runner.invoke(squid, ["init", "--not-add-root"])
        assert result.exit_code == 0
        assert bool(
            [
                match
                for match in re.finditer(
                    r"PROJECT SUCCESSFULLY INITIALIZED...!!! Enjoy!!!$",
                    result.output.strip(),
                )
            ]
        )


def test_squid_misc_hello(runner: CliRunner) -> None:
    """Test CLI Hello."""
    result = runner.invoke(squid, ["misc", "hello"])
    assert result.exit_code == 0
    assert result.exit_code == 0
    assert bool(re.compile(".*").match(result.output))


def test_squid_misc_saying_ok(runner: CliRunner, mock_requests_get_ok: Mock) -> None:
    """Test CLI saying."""
    result = runner.invoke(squid, ["misc", "saying"])
    assert result.exit_code == 0
    assert (
        result.output.strip()
        == "Man invented language to satisfy his deep need to complain."
    )


def test_squid_misc_saying_not_ok(
    runner: CliRunner, mock_requests_get_too_many_rq: Mock
) -> None:
    """Test CLI saying."""
    result = runner.invoke(squid, ["misc", "saying"])
    assert result.exit_code == 0
    assert (
        result.output.strip()
        == "Well, I guess I ran of sayings... so give me 59 minutes and 59 seconds to think..."
    )


def test_squid_plugins_entries(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI plugins."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()
        result = runner.invoke(squid, ["plugins", "title", "--text", "test"])
        assert result.exit_code == 0
        assert result.output.strip() == "Test"


def test_squid_add(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI add."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()
        result = runner.invoke(
            squid,
            [
                "add",
                "tentacles",
                "--name",
                "test",
                "--type",
                "test",
                "--path",
                ".",
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "name = test\npath = .\ntype = test\nstd_in = None\neditor = [Tentacle(name='test', type='test', path='.', attributes=[], tentacles=[], tasks=[])]\n[Tentacle(name='test', type='test', path='.', attributes=[], tentacles=[], tasks=[])]"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            ["add", "tentacles", "--name", "test", "--type", "test", "--path", "."],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "add",
                "tasks",
                "--name",
                "test",
                "--category",
                "test",
                "--envs",
                "{}",
                "--cmds",
                '[{"cmd":"echo", "args":["test"]}]',
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "name = test\ncategory = test\nenvs = {}\ncmds = [{\"cmd\":\"echo\", \"args\":[\"test\"]}]\nstd_in = None\neditor = [Task(name='test', category='test', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])]\n[Task(name='test', category='test', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])]"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "add",
                "tasks",
                "--name",
                "test",
                "--category",
                "test",
                "--envs",
                "{}",
                "--cmds",
                '[{"cmd":"echo", "args":["test"]}]',
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "add",
                "plugins",
                "--name",
                "test",
                "--source",
                ".",
                "--path",
                ".",
                "--entrypoint",
                "main:setup",
                "--cls",
                "cli",
                "--attributes",
                "[]",
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "name = test\npath = .\nsource = .\nentrypoint = main:setup\ncls = cli\nattributes = []\nstd_in = None\neditor = [Plugin(name='test', source='.', entrypoint='main:setup', cls=<PluginCls.CLI: 'cli'>, attributes=[], path='.')]\n[Plugin(name='test', source='.', entrypoint='main:setup', cls=<PluginCls.CLI: 'cli'>, attributes=[], path='.')]"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "add",
                "plugins",
                "--name",
                "test",
                "--source",
                ".",
                "--path",
                ".",
                "--entrypoint",
                "main:setup",
                "--cls",
                "cli",
                "--attributes",
                "[]",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"


def test_squid_execute(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI execute."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()

        result = runner.invoke(
            squid,
            [
                "execute",
                "tentacles",
                "--name",
                "test_a",
                "--task",
                "test_a",
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "name = test_a\ntask = test_a\nenv = ()\nfilter_task = None\nengine_output = True\ndeep = False\nstd_in = None\nTentacles: [Tentacle(name='test_a', type='test', path='./tentacle_a', attributes=[], tentacles=[], tasks=[Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])])]\nTasks: [Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])]"  # noqa: B950
        )

        result = runner.invoke(
            squid, ["execute", "tentacles", "--name", "test_a", "--task", "test_a"]
        )
        assert result.exit_code == 0
        assert bool(
            [match for match in re.finditer(r"Success!!!$", result.output.strip())]
        )

        result = runner.invoke(
            squid,
            [
                "execute",
                "tasks",
                "--name",
                "test_a",
                "--tentacle",
                "test_a",
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "name = test_a\ntentacle = test_a\nenv = ()\nengine_output = True\nstd_in = None\nTentacles: [Tentacle(name='test_a', type='test', path='./tentacle_a', attributes=[], tentacles=[Tentacle(name='test_b', type='test', path='./tentacle_b', attributes=[], tentacles=[], tasks=[Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])])], tasks=[Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])])]\nTasks: [Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])]"  # noqa: B950
        )

        result = runner.invoke(
            squid, ["execute", "tasks", "--name", "test_a", "--tentacle", "test_a"]
        )
        assert result.exit_code == 0
        assert bool(
            [match for match in re.finditer(r"Success!!!$", result.output.strip())]
        )


def test_squid_get(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI get."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()
        cwd = os.path.abspath(os.curdir)

        tentacle = {
            "name": "test_a",
            "path": os.path.join(cwd, "tentacle_a"),
            "type": "test",
        }

        result = runner.invoke(squid, ["get", "tentacle", "--name", "test_a"])
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == tentacle

        result = runner.invoke(
            squid, ["get", "tentacle", "--name", "test_a", "-o", "$.name"]
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == ["test_a"]

        result = runner.invoke(
            squid, ["get", "tentacle", "--name", "test_a", "-o", "$.name", "-r"]
        )
        assert result.exit_code == 0
        assert result.output.strip() == "test_a"

        result = runner.invoke(
            squid, ["get", "tentacles", "--query", "$.name == 'test_a'"]
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == [tentacle]

        result = runner.invoke(
            squid,
            ["get", "tentacles", "--query", "$.name == 'test_a'", "-o", "$.[*].name"],
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == ["test_a"]

        result = runner.invoke(
            squid,
            [
                "get",
                "tentacles",
                "--query",
                "$.name == 'test_a'",
                "-o",
                "$.[*].name",
                "-r",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "test_a"

        task = {
            "name": "test_a",
            "category": "echo",
            "envs": {},
            "cmds": [{"cmd": "echo", "args": ["test"]}],
        }
        result = runner.invoke(squid, ["get", "task", "--name", "test_a"])
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == task

        result = runner.invoke(
            squid, ["get", "task", "--name", "test_a", "-o", "$.name"]
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == ["test_a"]

        result = runner.invoke(
            squid, ["get", "task", "--name", "test_a", "-o", "$.name", "-r"]
        )
        assert result.exit_code == 0
        assert result.output.strip() == "test_a"

        result = runner.invoke(squid, ["get", "tasks", "--query", "$.name == 'test_a'"])
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == [task]

        result = runner.invoke(
            squid, ["get", "tasks", "--query", "$.name == 'test_a'", "-o", "$.[*].name"]
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == ["test_a"]

        result = runner.invoke(
            squid,
            ["get", "tasks", "--query", "$.name == 'test_a'", "-o", "$.[*].name", "-r"],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "test_a"

        plugin = {
            "name": "plugin_test",
            "source": "./plugin_test",
            "cls": "cli",
            "entrypoint": "plugin_test:setup",
            "attributes": [],
            "path": os.path.join(cwd, "plugin_test"),
        }
        result = runner.invoke(squid, ["get", "plugin", "--name", "plugin_test"])
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == plugin

        result = runner.invoke(
            squid, ["get", "plugin", "--name", "plugin_test", "-o", "$.name"]
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == ["plugin_test"]

        result = runner.invoke(
            squid, ["get", "plugin", "--name", "plugin_test", "-o", "$.name", "-r"]
        )
        assert result.exit_code == 0
        assert result.output.strip() == "plugin_test"

        result = runner.invoke(
            squid, ["get", "plugins", "--query", "$.name == 'plugin_test'"]
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == [plugin]

        result = runner.invoke(
            squid,
            [
                "get",
                "plugins",
                "--query",
                "$.name == 'plugin_test'",
                "-o",
                "$.[*].name",
            ],
        )
        assert result.exit_code == 0
        assert json.loads(result.output.strip()) == ["plugin_test"]

        result = runner.invoke(
            squid,
            [
                "get",
                "plugins",
                "--query",
                "$.name == 'plugin_test'",
                "-o",
                "$.[*].name",
                "-r",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "plugin_test"


def test_squid_link(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI link."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()

        result = runner.invoke(
            squid,
            [
                "link",
                "tentacles",
                "--target",
                "test_a",
                "--source",
                "test_a",
                "--source-type",
                "task",
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "source = test_a\nsource_type = task\ntarget = test_a\nremove = False\nstd_in = None\nDependencies: [Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])]\nTentacle: [Tentacle(name='test_a', type='test', path='./tentacle_a', attributes=[], tentacles=[], tasks=[])]"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "link",
                "tentacles",
                "--target",
                "test_a",
                "--source",
                "test_a",
                "--source-type",
                "task",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "link",
                "tentacles",
                "--target",
                "test_a",
                "--source",
                "test_a",
                "--source-type",
                "task",
                "-r",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "link",
                "tasks",
                "--target",
                "test_a",
                "--source",
                "test_a",
                "--source-type",
                "tentacle",
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "source = test_a\nsource_type = tentacle\ntarget = test_a\nremove = False\nstd_in = None\nDependencies: [Tentacle(name='test_a', type='test', path='./tentacle_a', attributes=[], tentacles=[], tasks=[])]\nTask: [Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])]"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "link",
                "tasks",
                "--target",
                "test_a",
                "--source",
                "test_a",
                "--source-type",
                "tentacle",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "link",
                "tasks",
                "--target",
                "test_a",
                "--source",
                "test_a",
                "--source-type",
                "tentacle",
                "-r",
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"


def test_squid_remove(runner: CliRunner, seed_real_structure) -> None:
    """Testing CLI remove."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()

        cwd = os.path.abspath(os.curdir)
        os.mkdir(os.path.join(cwd, "tentacle_a", ".tentacle"))
        os.mkdir(os.path.join(cwd, "tentacle_b", ".tentacle"))

        result = runner.invoke(
            squid, ["remove", "tentacles", "--query", "$.name.matches('.*')"], input="n"
        )
        assert result.exit_code == 1
        assert bool([match for match in re.finditer(r"test_a", result.output.strip())])
        assert bool(
            [match for match in re.finditer(r"Aborted!$", result.output.strip())]
        )

        result = runner.invoke(
            squid, ["remove", "tentacles", "--query", "$.name.matches('.*')"], input="y"
        )
        assert result.exit_code == 0
        assert bool(
            [match for match in re.finditer(r"Success!!!$", result.output.strip())]
        )

        result = runner.invoke(
            squid, ["remove", "tasks", "--query", "$.name.matches('.*')"], input="n"
        )
        assert result.exit_code == 1
        assert bool([match for match in re.finditer(r"test_a", result.output.strip())])
        assert bool(
            [match for match in re.finditer(r"Aborted!$", result.output.strip())]
        )

        result = runner.invoke(
            squid, ["remove", "tasks", "--query", "$.name.matches('.*')"], input="y"
        )
        assert result.exit_code == 0
        assert bool(
            [match for match in re.finditer(r"Success!!!$", result.output.strip())]
        )

        result = runner.invoke(
            squid, ["remove", "plugins", "--query", "$.name.matches('.*')"], input="n"
        )
        assert result.exit_code == 1
        assert bool(
            [match for match in re.finditer(r"plugin_test", result.output.strip())]
        )
        assert bool(
            [match for match in re.finditer(r"Aborted!$", result.output.strip())]
        )

        result = runner.invoke(
            squid, ["remove", "plugins", "--query", "$.name.matches('.*')"], input="y"
        )
        assert result.exit_code == 0
        assert bool(
            [match for match in re.finditer(r"Success!!!$", result.output.strip())]
        )


def test_squid_update(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI update."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()

        result = runner.invoke(
            squid,
            [
                "update",
                "tentacles",
                "--query",
                "$.name.matches('.*')",
                "--kvs",
                '{"type": "all_roots"}',
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "kvs = {\"type\": \"all_roots\"}\nquery = $.name.matches('.*')\neditor = False\nTentacles original: \n[Tentacle(name='test_a', type='test', path='./tentacle_a', attributes=[], tentacles=[Tentacle(name='test_b', type='test', path='./tentacle_b', attributes=[], tentacles=[], tasks=[])], tasks=[]), Tentacle(name='test_b', type='test', path='./tentacle_b', attributes=[], tentacles=[], tasks=[])]\nTentacles update: \n{\"type\": \"all_roots\"}"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "update",
                "tentacles",
                "--query",
                "$.name.matches('.*')",
                "--kvs",
                '{"type": "all_roots"}',
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "update",
                "tasks",
                "--query",
                "$.name.matches('.*')",
                "--kvs",
                '{"category": "all_roots"}',
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "kvs = {\"category\": \"all_roots\"}\nquery = $.name.matches('.*')\neditor = False\nTasks original: \n[Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[]), Task(name='test_b', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[Task(name='test_a', category='echo', envs={}, cmds=[TaskCmd(cmd='echo', args=['test'])], tentacles=[], tasks=[])])]\nTasks update: \n{\"category\": \"all_roots\"}"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "update",
                "tasks",
                "--query",
                "$.name.matches('.*')",
                "--kvs",
                '{"category": "all_roots"}',
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"

        result = runner.invoke(
            squid,
            [
                "update",
                "plugins",
                "--query",
                "$.name.matches('.*')",
                "--kvs",
                '{"entrypoint": "test:main"}',
                "--dry-run",
                "--verbose",
            ],
        )
        assert result.exit_code == 0
        assert (
            result.output.strip()
            == "kvs = {\"entrypoint\": \"test:main\"}\nquery = $.name.matches('.*')\neditor = False\nPlugins original: \n[Plugin(name='plugin_test', source='./plugin_test', entrypoint='plugin_test:setup', cls=<PluginCls.CLI: 'cli'>, attributes=[], path='./plugin_test')]\nPlugins update: \n{\"entrypoint\": \"test:main\"}"  # noqa: B950
        )

        result = runner.invoke(
            squid,
            [
                "update",
                "plugins",
                "--query",
                "$.name.matches('.*')",
                "--kvs",
                '{"entrypoint": "test:main"}',
            ],
        )
        assert result.exit_code == 0
        assert result.output.strip() == "Success!!!"


def test_squid_version(runner: CliRunner, seed_real_structure) -> None:
    """Test CLI version."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()
        result = runner.invoke(squid, ["version"])
        assert result.exit_code == 0
        assert bool(
            [
                match
                for match in re.finditer(
                    r"Squid CLI Version: v\d{,2}\.\d{,2}\.\d{,2}(rc0)?$",
                    result.output.strip(),
                )
            ]
        )


def test_callbacks(runner: CliRunner, seed_real_structure) -> None:
    """Test callbacks."""
    create = seed_real_structure
    with runner.isolated_filesystem():
        create()
        with pytest.raises(Exception) as exc:
            _ = _execute_transformation("not_command", "not_command", {})
        assert "Abort" in str(exc.typename)

        class Ctx:
            obj: dict = {"editor": "xxxx"}
            resilient_parsing: bool = False

        assert not std_in_callback(Ctx(), True)  # type: ignore
