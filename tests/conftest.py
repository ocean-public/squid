"""Configure tests."""
import json
import os
from pathlib import Path
from typing import Callable, Dict, List, Tuple
from unittest.mock import Mock

from pyfakefs.fake_filesystem import FakeFilesystem
import pytest
from pytest_mock import MockFixture

from squid.framework.core import DbTinyDB, Head, Init, Squid, SquidDb
from squid.framework.core.models.bases import (
    Task,
    TaskCmd,
    Tentacle,
)
from squid.framework.services.db.db_abc import DbABC
from squid.framework.services.engine.engine_abc import EngineABC


@pytest.fixture
def mock_engine(mocker: MockFixture) -> Mock:
    """Mock Engine type."""
    mock = mocker.Mock(spec=EngineABC)
    mock.execute_dag.return_value = {
        "summary": "TEST",
        "exec_tree": ["TEST"],
    }
    return mock


@pytest.fixture
def mock_db(mocker: MockFixture) -> Mock:
    """Mock for DB type."""
    mock = mocker.Mock(spec=DbABC)
    mock.query.return_value = [{"name": "tentacle_a", "type": "test", "path": "./"}]
    mock.get.return_value = {"name": "tentacle_a", "type": "test", "path": "./"}
    mock.insert.return_value = 1
    mock.update.return_value = [1]
    mock.delete.return_value = [1]
    return mock


@pytest.fixture
def tentacle_simple() -> List[Tentacle]:
    """Fixture for simple tasks."""
    tentacle = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[
            Task(
                name="test",
                category="echo",
                envs={},
                cmds=[TaskCmd(cmd="echo", args=["test"])],
            )
        ],
    )
    return [tentacle]


@pytest.fixture
def tentacles_factory() -> Tuple[List[Tentacle], List[Tentacle], List[Tentacle]]:
    """Fixture for tentacle instance."""
    task_a = Task(
        name="task_a",
        category="echo",
        envs={"TEST": "env"},
        cmds=[TaskCmd(cmd="echo", args=["task_a"])],
    )
    task_b = Task(
        name="task_b",
        category="echo",
        envs={},
        cmds=[TaskCmd(cmd="echo", args=["task_b"])],
    )
    task_c = Task(
        name="task_c",
        category="echo",
        envs={},
        cmds=[TaskCmd(cmd="echo", args=["task_c"])],
    )
    task_d = Task(
        name="task_d",
        category="echo",
        envs={},
        cmds=[TaskCmd(cmd="echo", args=["task_d"])],
        tasks=[task_a],
    )
    task_e = Task(
        name="task_e",
        category="echo",
        envs={},
        cmds=[TaskCmd(cmd="echo", args=["task_e"])],
    )

    tentacle_f = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[task_e],
    )
    tentacle_e = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[tentacle_f],
        tasks=[task_d],
    )
    tentacle_d = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[task_b],
    )
    tentacle_c = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[tentacle_d, tentacle_e],
        tasks=[task_c],
    )
    tentacle_b = Tentacle(
        name="ten_b",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[task_b],
    )
    tentacle_a = Tentacle(
        name="ten_a",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[tentacle_b],
        tasks=[task_a],
    )
    tentacle_w_tasks = [tentacle_b]
    tentacle_w_tasks_w_tentacles = [tentacle_a]
    tentacle_w_tasks_w_tentacles_v2 = [tentacle_c]
    return (
        tentacle_w_tasks,
        tentacle_w_tasks_w_tentacles,
        tentacle_w_tasks_w_tentacles_v2,
    )


@pytest.fixture
def init_squid(fs: FakeFilesystem) -> Tuple[FakeFilesystem, Head, Squid]:
    """Initialize Squid."""
    fs.create_dir("/home/test")
    fs.create_dir("/home/test/a")
    fs.create_dir("/home/test/b")
    fs.cwd = "/home/test"
    squid = Squid()
    squid_head = Head()
    squid_head.execute(Init(squid, path=fs.cwd))
    return fs, squid_head, squid


@pytest.fixture
def db_json() -> Dict:
    """DB_JSON mock."""
    return {
        "_default": {},
        "tentacles": {
            "1": {"name": "test_a", "type": "test", "path": "./tentacle_a"},
            "2": {"name": "test_b", "type": "test", "path": "./tentacle_b"},
        },
        "tasks": {
            "1": {
                "name": "test_a",
                "category": "echo",
                "envs": {},
                "cmds": [{"cmd": "echo", "args": ["test"]}],
            },
            "2": {
                "name": "test_b",
                "category": "echo",
                "envs": {},
                "cmds": [{"cmd": "echo", "args": ["test"]}],
            },
        },
        "plugins": {
            "1": {
                "name": "plugin_test",
                "source": "./plugin_test",
                "cls": "cli",
                "entrypoint": "plugin_test:setup",
                "attributes": [],
                "path": "./plugin_test",
            }
        },
        "links": {
            "1": {
                "name": "test_b",
                "cls": "task",
                "dependencies": [
                    {"name": "test_a", "cls": "task", "settings": {"symlink": False}}
                ],
                "dependant": [],
                "attributes": [],
            },
            "2": {
                "name": "test_a",
                "cls": "task",
                "dependencies": [],
                "dependant": [
                    {"name": "test_b", "cls": "task", "settings": {"symlink": False}}
                ],
                "attributes": [],
            },
            "3": {
                "name": "test_a",
                "cls": "tentacle",
                "dependencies": [
                    {
                        "name": "test_b",
                        "cls": "tentacle",
                        "settings": {"symlink": False},
                    }
                ],
                "dependant": [],
                "attributes": [],
            },
            "4": {
                "name": "test_b",
                "cls": "tentacle",
                "dependencies": [],
                "dependant": [
                    {
                        "name": "test_a",
                        "cls": "tentacle",
                        "settings": {"symlink": False},
                    }
                ],
                "attributes": [],
            },
        },
    }


@pytest.fixture
def seed_squid(
    init_squid: Tuple[FakeFilesystem, Head, Squid], db_json: Dict
) -> Tuple[FakeFilesystem, Head, Squid]:
    """Seed Squid."""
    fs, squid_head, squid = init_squid
    fs.create_dir("/home/test/tentacle_a")
    fs.create_dir("/home/test/tentacle_b")
    fs.create_dir("/home/test/plugin_test")

    assert os.path.exists("/home/test/.squid")
    fs.create_file("/home/test/.squid/db.json", contents=json.dumps(db_json))
    assert os.path.exists("/home/test/.squid/db.json")
    return fs, squid_head, squid


@pytest.fixture
def seed_real_structure(db_json: Dict) -> Callable:
    """Init Squid structure."""

    def create():
        squid_path = Path(os.curdir).joinpath(".squid")
        squid_path.mkdir()
        squid_logs = squid_path.joinpath("logs")
        squid_logs.mkdir()
        plugin_test_path = Path(os.curdir).joinpath("plugin_test")
        plugin_test_path.mkdir()
        plugin_test_path.joinpath(".squid-plugin").mkdir()
        with open(
            str(plugin_test_path.joinpath("plugin_test.py").absolute()), "w"
        ) as f:
            f.write(
                """

def make_title(text: str):
    return text.title()


def setup(registry):
    registry.entry("title", make_title)

                """
            )
        with open(str(squid_path.joinpath("db.json").absolute()), "w") as f:
            json.dump(db_json, f)
        tentacle_a_path = Path(os.curdir).joinpath("tentacle_a")
        tentacle_a_path.mkdir()
        tentacle_b_path = Path(os.curdir).joinpath("tentacle_b")
        tentacle_b_path.mkdir()
        squid_db = SquidDb()
        squid_db._db = DbTinyDB()

    return create
