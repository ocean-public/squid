# flake8: noqa
def make_lowercase(text: str):
    return text.lower()


def setup(registry):
    registry.entry("lowercase", make_lowercase)
