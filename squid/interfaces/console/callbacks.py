"""Callbacks."""
from enum import Enum
import os
from typing import Any, Callable, Dict, Tuple

import click
import typer

from squid.framework.core import (
    GetPlugin,
    GetPlugins,
    GetTask,
    GetTasks,
    GetTentacle,
    GetTentacles,
    Squid,
)
from squid.framework.core.models.bases import (
    Plugin,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)
from squid.framework.utils import compose_yaml, descompose_yaml, to_obj, to_str
from squid.interfaces.console import SquidCLI
from squid.interfaces.console.helpers import (
    filter_output,
    fn_check_format_multiple_param,
    get_settings_from_command,
)

TEMPLATES: Dict[str, Callable] = {
    "add": lambda ctx, settings, initial_values, *args, **kwargs: "\n".join(
        [
            compose_yaml(
                {
                    **settings.get("template", {}),
                    **initial_value,
                    **{
                        k: ctx.obj[k]
                        for k in settings.get("required_fields", set())
                        if ctx.obj.get(k, "")
                    },
                }
            )
            for initial_value in initial_values
        ]
    ),
    "update": lambda ctx, *args, **kwargs: "\n".join(
        [
            compose_yaml(item.dict())
            for item in [
                *(
                    SquidCLI().execute(
                        GetTentacles(
                            Squid(), query=ctx.obj.get("query", ""), output="tree"
                        )
                    )
                    if ctx.command.name == "tentacles"
                    else []
                ),
                *(
                    SquidCLI().execute(
                        GetTasks(Squid(), query=ctx.obj.get("query", ""), output="tree")
                    )
                    if ctx.command.name == "tasks"
                    else []
                ),
                *(
                    SquidCLI().execute(
                        GetPlugins(
                            Squid(), query=ctx.obj.get("query", ""), output="tree"
                        )
                    )
                    if ctx.command.name == "plugins"
                    else []
                ),
            ]
        ]
    ),
}

TRANSFORMATIONS: Dict[Tuple[str, str], Callable] = {
    ("add", "tentacles"): lambda x, *args, **kwargs: Tentacle.parse_obj(x),
    ("add", "tasks"): lambda x: Task.parse_obj(x),
    ("add", "plugins"): lambda x: Plugin.parse_obj(x),
    ("update", "tentacles"): lambda x: Tentacle.parse_obj(x) if "kvs" not in x else x,
    ("update", "tasks"): lambda x: Task.parse_obj(x) if "kvs" not in x else x,
    ("update", "plugins"): lambda x: Plugin.parse_obj(x) if "kvs" not in x else x,
    ("link", "tentacles"): lambda x: TentacleRef.parse_obj(x)
    if "type" in x
    else TaskRef.parse_obj(x),
    ("link", "tasks"): lambda x: TaskRef.parse_obj(x)
    if "category" in x
    else TentacleRef.parse_obj(x),
    ("execute", "tentacles"): lambda x: Tentacle.parse_obj(x)
    if "type" in x
    else Task.parse_obj(x),
    ("execute", "tasks"): lambda x: Task.parse_obj(x)
    if "category" in x
    else Tentacle.parse_obj(x),
}


VALIDATORS: Dict[Tuple[str, str, str], Callable[[Any], bool]] = {
    ("add", "tentacles", "name"): lambda x: not bool(
        SquidCLI().execute(GetTentacle(Squid(), x))
    ),
    ("add", "tentacles", "path"): lambda x: os.path.exists(os.path.abspath(x)),
    ("add", "tasks", "name"): lambda x: not bool(
        SquidCLI().execute(GetTask(Squid(), x))
    ),
    ("add", "plugins", "name"): lambda x: not bool(
        SquidCLI().execute(GetPlugin(Squid(), x))
    ),
    ("link", "tentacles", "target"): lambda x: bool(
        SquidCLI().execute(GetTentacle(Squid(), x))
    ),
    ("link", "tentacles", "source"): lambda x: True
    if x is None
    else bool(
        SquidCLI().execute(GetTentacle(Squid(), x))
        or SquidCLI().execute(GetTask(Squid(), x))
    ),
    ("link", "tasks", "target"): lambda x: bool(
        SquidCLI().execute(GetTask(Squid(), x))
    ),
    ("link", "tasks", "source"): lambda x: True
    if x is None
    else bool(
        SquidCLI().execute(GetTentacle(Squid(), x))
        or SquidCLI().execute(GetTask(Squid(), x))
    ),
    ("update", "tentacles", "kvs"): lambda x: True
    if x is None
    else to_obj(x, validate=True),
    ("execute", "tentacles", "name"): lambda x: True
    if x is None
    else bool(SquidCLI().execute(GetTentacle(Squid(), x))),
    ("execute", "tentacles", "task"): lambda x: True
    if x is None
    else bool(SquidCLI().execute(GetTask(Squid(), x))),
    ("execute", "tentacles", "param"): lambda x: True
    if x is None
    else all([fn_check_format_multiple_param(item) for item in x]),
    ("execute", "tasks", "param"): lambda x: True
    if x is None
    else all([fn_check_format_multiple_param(item) for item in x]),
}


def _execute_transformation(main_command: str, sub_command: str, obj: Dict) -> Any:
    """Execute Transformations."""
    value_parsed = TRANSFORMATIONS.get((main_command, sub_command), lambda x: None)(obj)
    if value_parsed is None:
        typer.echo("Subcommand is not supported by transformations.", err=True)
        raise typer.Abort()
    return value_parsed


def cli_callback(ctx: typer.Context) -> None:
    """Main Callback."""
    if not isinstance(ctx.obj, dict):
        ctx.obj = {}
    ctx.obj.update({"main_command": ctx.info_name})


def std_in_callback(ctx: typer.Context, value: bool) -> Any:
    """Callback to use stdin as input."""
    if ctx.resilient_parsing:
        return
    if not isinstance(ctx.obj, dict):
        ctx.obj = {}
    ctx.obj.update({"std_in": value})
    if "editor" in ctx.obj and ctx.obj["editor"]:
        return False

    settings = get_settings_from_command(
        ctx.obj.get("main_command", ""), ctx.command.name
    )

    if value:
        stdin_text = click.get_text_stream("stdin").read()
        objs_transformed = [
            _execute_transformation(
                main_command=ctx.obj.get("main_command", ""),
                sub_command=ctx.command.name,
                obj={
                    **schema,
                    **{
                        k: to_obj(ctx.obj[k])
                        for k in settings.get("required_fields", set())
                        if ctx.obj.get(k, "")
                    },
                },
            )
            for schema in descompose_yaml(stdin_text)
        ]
        ctx.obj.update(
            {
                "std_in_transformed": [
                    obj_transformed.dict()
                    if hasattr(obj_transformed, "dict")
                    else obj_transformed
                    for obj_transformed in objs_transformed
                ]
            }
        )
        return objs_transformed
    return value


def editor_callback(ctx: typer.Context, value: bool) -> Any:
    """Callback to use editor as input."""
    if ctx.resilient_parsing:
        return
    if not isinstance(ctx.obj, dict):
        ctx.obj = {}
    ctx.obj.update({"editor": value})
    initial_values = ctx.obj.get("std_in_transformed", [{}])
    settings = get_settings_from_command(
        ctx.obj.get("main_command", ""), ctx.command.name
    )
    if value:
        template = TEMPLATES.get(
            ctx.obj.get("main_command", ""), lambda *args, **kwargs: ""
        )(ctx, settings, initial_values)
        template_filled = click.edit(template)
        if template_filled is None:
            raise typer.Abort()

        objs_transformed = [
            _execute_transformation(
                main_command=ctx.obj.get("main_command", ""),
                sub_command=ctx.command.name,
                obj=schema,
            )
            for schema in descompose_yaml(template_filled)
        ]
        ctx.obj.update(
            {
                "editor_transformed": [
                    obj_transformed.dict()
                    if hasattr(obj_transformed, "dict")
                    else obj_transformed
                    for obj_transformed in objs_transformed
                ]
            }
        )
        return objs_transformed
    else:
        if not ctx.obj.get("std_in", False):
            objs_transformed = [
                _execute_transformation(
                    main_command=ctx.obj.get("main_command", ""),
                    sub_command=ctx.command.name,
                    obj={
                        k: to_obj(ctx.obj.get(k, ""))
                        for k in settings.get("required_fields", set())
                    },
                )
            ]
            ctx.obj.update(
                {
                    "editor_transformed": [
                        obj_transformed.dict()
                        if hasattr(obj_transformed, "dict")
                        else obj_transformed
                        for obj_transformed in objs_transformed
                    ]
                }
            )
            return (
                False
                if ctx.obj.get("main_command", "") == "update"
                else objs_transformed
            )
        return [
            _execute_transformation(
                main_command=ctx.obj.get("main_command", ""),
                sub_command=ctx.command.name,
                obj=obj,
            )
            for obj in initial_values
        ]


def regular_callback(ctx: typer.Context, param: typer.CallbackParam, value: str) -> Any:
    """Callback to validate inputs."""
    if ctx.resilient_parsing:
        return
    if not isinstance(ctx.obj, dict):
        ctx.obj = {}
    ctx.obj.update({param.name: value})
    validate = VALIDATORS.get(
        (ctx.obj.get("main_command", ""), ctx.command.name, param.name), lambda x: True
    )
    if not validate(value):
        typer.echo(f"Validation for {param.name} did not pass.", err=True)
        raise typer.Abort()
    return value.value if isinstance(value, Enum) else value


def remove_confirmation(
    ctx: typer.Context, param: typer.CallbackParam, value: str
) -> Any:
    """Remove confirmation."""
    if param.name == "query":
        if ctx.command.name == "tentacles":
            items_to_remove = SquidCLI().execute(GetTentacles(Squid(), query=value, output="list"))  # type: ignore

        elif ctx.command.name == "tasks":
            items_to_remove = SquidCLI().execute(GetTasks(Squid(), query=value, output="list"))  # type: ignore

        elif ctx.command.name == "plugins":
            items_to_remove = SquidCLI().execute(GetPlugins(Squid(), query=value, output="list"))  # type: ignore

        else:
            raise typer.Abort()

        output = filter_output("$.[*].name", [item.dict() for item in items_to_remove])
        typer.echo(to_str(output))
    else:
        raise typer.Abort()

    if click.confirm("Do you want to continue?", abort=True):
        return value

    else:
        raise typer.Abort()
