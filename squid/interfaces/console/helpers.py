"""Helpers functions."""
import json
import re
from typing import Any, Callable, cast, Dict, List

import jsonpath_rw_ext as jp
import typer
import yaml

from squid.framework.core.models.bases import (
    Plugin,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)
from squid.framework.utils import compose_json, compose_yaml, to_str
from squid.interfaces.console import CliOutput


SETTINGS = {
    ("add", "tentacles"): {
        "template": {
            "name": "<NAME:str>",
            "type": "<TYPE:str>",
            "path": "<PATH:str>",
            "tentacles": ["<TENTACLE:model>"],
            "tasks": ["<TASK:model>"],
        },
        "required_fields": set(TentacleRef.__fields__.keys()),
    },
    ("add", "tasks"): {
        "template": {
            "name": "<NAME:str>",
            "category": "<CATEGORY:str>",
            "envs": "<ENVS:dict[str,str]>",
            "cmds": [{"cmd": "<CMD:str>", "args": ["<ARG:str>"]}],
            "tentacles": ["<TENTACLE:model>"],
            "tasks": ["<TASK:model>"],
        },
        "required_fields": set(TaskRef.__fields__.keys()),
    },
    ("add", "plugins"): {
        "template": {
            "name": "<NAME:str>",
            "source": "<SOURCE:str>",
            "entrypoint": "<ENTRYPOINT:str>",
            "cls": "<CLS:str[cli|function]>",
            "attributes": ["ATTRIBUTE:dict[str,any]"],
            "path": "PATH:str",
        },
        "required_fields": set(Plugin.__fields__.keys()),
    },
    ("update", "tentacles"): {"template": {}, "required_fields": {"kvs"}},
    ("update", "tasks"): {"template": {}, "required_fields": {"kvs"}},
    ("update", "plugins"): {"template": {}, "required_fields": {"kvs"}},
}


def fn_check_format_multiple_param(param: str) -> bool:
    """Check that params are compliant to KEY=VALUE."""
    return bool([match for match in re.finditer(r"^.[^\s=]*=.[^=]*$", param.strip())])


def get_settings_from_command(main_command: str, sub_command: str) -> Dict:
    """Return settings from a tuple of main and sub commands."""
    return SETTINGS.get(
        (main_command, sub_command), {"template": {}, "required_fields": set()}
    )


def compose_output(output: Any, format: CliOutput) -> str:
    """Compose output."""
    try:
        if format == CliOutput.YAML:
            return compose_yaml(output)
        if format == CliOutput.JSON:
            return compose_json(output)
        return ""
    except json.decoder.JSONDecodeError:
        return output
    except yaml.YAMLError:
        return output


def to_secho(input: Any) -> None:
    """Secho the input obj."""
    if isinstance(input, list):
        for i in input:
            to_secho(input=i)
    elif isinstance(input, dict):
        for k, v in input.items():
            typer.secho(k.upper(), fg=typer.colors.GREEN, bold=True)
            to_secho(input=v)
    elif isinstance(input, str) or isinstance(input, int):
        typer.secho(cast(str, input), fg=typer.colors.GREEN, bold=True)
    else:
        typer.secho(to_str(input), fg=typer.colors.GREEN, bold=True)


def filter_output(filter: str, obj: Any) -> Any:
    """Filter query ouptput."""
    return jp.match(filter, obj)


def merge_cli_env_to_env_tasks(envs: List[str], tasks: List[Task]) -> List[Task]:
    """Merge cli envs to env variables of tasks."""
    envs = envs or []
    envs_obj = {env.split("=")[0]: env.split("=")[1] for env in envs}
    _fn_merge_cli_env_to_env_tasks: Callable = lambda _tasks: [
        Task.parse_obj(
            {
                **task.dict(),
                **{
                    "envs": {**task.envs, **envs_obj},
                    "tasks": [
                        inner_task.dict()
                        for inner_task in _fn_merge_cli_env_to_env_tasks(task.tasks)
                    ],
                },
            }
        )
        for task in _tasks
    ]
    return _fn_merge_cli_env_to_env_tasks(tasks)


def tentacle_filter_by_jpath(
    tentacles: List[Tentacle], jpath: str, fn_validation: Callable
) -> List[Tentacle]:
    """Filter tentacle by json path and fn."""
    tentacles_tmp = []
    for tentacle in tentacles:
        tentacle.tentacles = tentacle_filter_by_jpath(
            tentacle.tentacles, jpath, fn_validation
        )
        obj = jp.match(jpath, tentacle.dict())
        if obj and fn_validation(obj[0]):
            tentacles_tmp.append(tentacle)
        else:
            tentacles_tmp.extend(tentacle.tentacles)
    return tentacles_tmp


def tentacle_tasks_filter_by_jpath(
    tentacles: List[Tentacle], jpath: str
) -> List[Tentacle]:
    """Filter tentacle tasks by json path."""
    jpath = jpath.replace("$", "@")
    return [
        Tentacle.parse_obj(
            {
                **tentacle.dict(),
                "tasks": [
                    task.value
                    for task in jp.parse(f"$.tasks[?({jpath})]").find(tentacle.dict())
                ],
                "tentacles": tentacle_tasks_filter_by_jpath(tentacle.tentacles, jpath),
            }
        )
        for tentacle in tentacles
    ]


def merge_envs_all_tentacles(
    tentacles: List[Tentacle], tasks: List[Task], env: List[str]
) -> List[Tentacle]:
    """Merge envs on all tentacles tasks."""
    return [
        Tentacle.parse_obj(
            {
                **tentacle.dict(),
                **{
                    "tentacles": [
                        tentacle.dict()
                        for tentacle in merge_envs_all_tentacles(
                            tentacle.tentacles, tasks, env
                        )
                    ]
                    if hasattr(tentacle, "tentacles")
                    else [],
                    "tasks": [
                        task.dict()
                        for task in merge_cli_env_to_env_tasks(env, tentacle.tasks)
                    ]
                    if hasattr(tentacle, "tasks") and tentacle.tasks
                    else tasks,
                },
            }
        )
        for tentacle in tentacles
    ]
