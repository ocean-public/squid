"""Remove tentacles|tasks|plugins."""
import typer

from squid.framework.core import Squid
from squid.framework.core.commands.remove import (
    RemovePlugins,
    RemoveTasks,
    RemoveTentacles,
)
from squid.interfaces.console import SquidCLI
from squid.interfaces.console.callbacks import cli_callback, remove_confirmation


cli = typer.Typer(
    callback=cli_callback,
    invoke_without_command=True,
    help="Remove Tentacles | Tasks | Plugins",
)


@cli.command(name="tentacles")
def remove_tentacles(
    query: str = typer.Option(  # noqa: B008
        ..., "--query", "-q", callback=remove_confirmation
    ),
) -> None:
    """Remove tentacles by CLI."""
    try:
        SquidCLI().execute(RemoveTentacles(Squid(), query=query))  # type: ignore
        typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="tasks")
def remove_tasks(
    query: str = typer.Option(  # noqa: B008
        ..., "--query", "-q", callback=remove_confirmation
    ),
) -> None:
    """Remove tasks by CLI."""
    try:
        SquidCLI().execute(RemoveTasks(Squid(), query=query))  # type: ignore
        typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="plugins")
def remove_plugins(
    query: str = typer.Option(  # noqa: B008
        ..., "--query", "-q", callback=remove_confirmation
    ),
) -> None:
    """Remove plugins by CLI."""
    try:
        SquidCLI().execute(RemovePlugins(Squid(), query=query))  # type: ignore
        typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)
