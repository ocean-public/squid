"""Link tentacles|tasks."""
from enum import Enum
from typing import cast, List

import typer

from squid.framework.core import Squid, Task, Tentacle
from squid.framework.core.commands.get import GetTask, GetTentacle
from squid.framework.core.commands.link import CreateLink, RemoveLink
from squid.interfaces.console import SquidCLI
from squid.interfaces.console.callbacks import (
    cli_callback,
    regular_callback,
    std_in_callback,
)

cli = typer.Typer(
    callback=cli_callback, invoke_without_command=True, help="Link Tentacles | Tasks",
)


class SourceType(str, Enum):
    """SourceType."""

    TENTACLE = "tentacle"
    TASK = "task"


@cli.command(
    name="tentacles",
    help="Links Tentales or Task to a Tentacle (when used with --std-in flag will be the input of source as a list of Tentacles or Tasks.)",
)
def link_tentacles(
    source: str = typer.Option(  # noqa: B008
        None,
        "--source",
        "-s",
        callback=regular_callback,
        help="Tentacle name or Task name to add as dependencies.",
    ),
    source_type: SourceType = typer.Option(  # noqa: B008
        SourceType.TENTACLE,
        "--source-type",
        "--st",
        case_sensitive=True,
        callback=regular_callback,
    ),
    target: str = typer.Option(  # noqa: B008
        ...,
        "--target",
        "-t",
        callback=regular_callback,
        help="Tentacle name to be dependant.",
    ),
    remove: bool = typer.Option(  # noqa: B008
        False, "--remove", "-r", help="remove link."
    ),
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input (list of TentacleRef or list TaskRef)",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Link tentacles by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("source", source),
                        ("source_type", source_type),
                        ("target", target),
                        ("remove", remove),
                        ("std_in", std_in),
                    ]
                ]
            )
        )
    try:
        dependencies = (
            std_in
            if std_in
            else [
                SquidCLI().execute(GetTentacle(Squid(), name=source))
                if source_type == SourceType.TENTACLE
                else SquidCLI().execute(GetTask(Squid(), name=source))
            ]
        )
        if isinstance(dependencies, list) and not dependencies[0]:
            typer.secho("No dependencies specified.", err=True)
            raise typer.Abort()
        tentacle = [SquidCLI().execute(GetTentacle(Squid(), name=target))]
        if dry_run:
            dry_run_dependencies = [
                Tentacle.parse_obj(dependency.dict())
                if hasattr(dependency, "type")
                else Task.parse_obj(dependency.dict())
                for dependency in cast(List, dependencies)
            ]
            dry_run_tentacle = [
                Tentacle.parse_obj(tentacle.dict()) for tentacle in tentacle
            ]
            typer.echo(f"Dependencies: {dry_run_dependencies}")
            typer.echo(f"Tentacle: {dry_run_tentacle}")
        else:
            SquidCLI().execute(
                RemoveLink(Squid(), sources=dependencies, targets=tentacle)  # type: ignore
            ) if remove else SquidCLI().execute(
                CreateLink(Squid(), sources=dependencies, targets=tentacle)  # type: ignore
            )
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(
    name="tasks",
    help="Links Tentales or Task to a Task (when used with --std-in flag will be the input of source as a list of Tentacles or Tasks.)",
)
def link_tasks(
    source: str = typer.Option(  # noqa: B008
        None,
        "--source",
        "-s",
        callback=regular_callback,
        help="Tentacle name or Task name to add as dependencies.",
    ),
    source_type: SourceType = typer.Option(  # noqa: B008
        SourceType.TENTACLE,
        "--source-type",
        "--st",
        case_sensitive=True,
        callback=regular_callback,
    ),
    target: str = typer.Option(  # noqa: B008
        ...,
        "--target",
        "-t",
        callback=regular_callback,
        help="Task name to be dependant.",
    ),
    remove: bool = typer.Option(  # noqa: B008
        False, "--remove", "-r", help="remove link."
    ),
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input (list of TentacleRef or list TaskRef)",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Link task by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("source", source),
                        ("source_type", source_type),
                        ("target", target),
                        ("remove", remove),
                        ("std_in", std_in),
                    ]
                ]
            )
        )
    try:
        dependencies = (
            std_in
            if std_in
            else [
                SquidCLI().execute(GetTentacle(Squid(), name=source))
                if source_type == SourceType.TENTACLE
                else SquidCLI().execute(GetTask(Squid(), name=source))
            ]
        )
        if isinstance(dependencies, list) and not dependencies[0]:
            typer.secho("No dependencies specified.", err=True)
            raise typer.Abort()
        task = [SquidCLI().execute(GetTask(Squid(), name=target))]
        if dry_run:
            dry_run_dependencies = [
                Tentacle.parse_obj(dependency.dict())
                if hasattr(dependency, "type")
                else Task.parse_obj(dependency.dict())
                for dependency in cast(List, dependencies)
            ]
            dry_run_task = [Task.parse_obj(task.dict()) for task in task]
            typer.echo(f"Dependencies: {dry_run_dependencies}")
            typer.echo(f"Task: {dry_run_task}")
        else:
            SquidCLI().execute(
                RemoveLink(Squid(), sources=dependencies, targets=task)  # type: ignore
            ) if remove else SquidCLI().execute(
                CreateLink(Squid(), sources=dependencies, targets=task)  # type: ignore
            )
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)
