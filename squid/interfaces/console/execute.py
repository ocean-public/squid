"""Execute tentacles|tasks."""
from typing import List

import typer

from squid.framework.core import Squid, Task, Tentacle
from squid.framework.core.commands.execute import Execute
from squid.framework.core.commands.get import GetTask, GetTentacle, GetTentacles
from squid.interfaces.console import SquidCLI
from squid.interfaces.console.callbacks import (
    cli_callback,
    regular_callback,
    std_in_callback,
)
from squid.interfaces.console.helpers import (
    merge_cli_env_to_env_tasks,
    merge_envs_all_tentacles,
    tentacle_filter_by_jpath,
    tentacle_tasks_filter_by_jpath,
    to_secho,
)


cli = typer.Typer(
    callback=cli_callback,
    invoke_without_command=True,
    help="Execute Tentacles | Tasks",
)


def execute(
    tentacles: list, tasks: list, env: list, dry_run: bool, engine_output: bool
) -> None:
    """Execute tentacles."""
    try:
        tasks = merge_cli_env_to_env_tasks(env, tasks)
        tentacles = merge_envs_all_tentacles(tentacles, tasks, env)

        if not tentacles:
            if typer.confirm(
                "There are no tentacles specified. Do you want to run them all?",
                abort=True,
            ):
                tentacles = SquidCLI().execute(
                    GetTentacles(Squid(), query="$.name.matches('.*')", output="tree")
                )
            else:
                raise typer.Abort()

        if not tasks and not isinstance(tentacles[0], dict) and not tentacles[0].tasks:
            typer.echo("There are no tasks to run.", err=True)
            raise typer.Abort()

        if dry_run:
            dry_run_tentacles = [Tentacle.parse_obj(tentacle) for tentacle in tentacles]
            dry_run_tasks = [Task.parse_obj(task) for task in tasks]
            typer.echo(f"Tentacles: {dry_run_tentacles}")
            typer.echo(f"Tasks: {dry_run_tasks}")

        else:
            resp = SquidCLI().execute(Execute(Squid(), tentacles, tasks))
            if engine_output:
                to_secho(resp)
                typer.secho("\nSuccess!!!", fg=typer.colors.GREEN, bold=True)

    except Exception as e:
        typer.echo(e, err=True)


@cli.command(
    name="tentacles", help="Execute Tasks on a tentacle.",
)
def execute_tentacles(
    name: str = typer.Option(  # noqa: B008
        None, "--name", "-n", callback=regular_callback, help="Tentacle name.",
    ),
    task: str = typer.Option(  # noqa: B008
        None, "--task", "-t", callback=regular_callback, help="Task name.",
    ),
    env: List[str] = typer.Option(  # noqa: B008
        None, "--env", "-e", callback=regular_callback, help="Parameter for task.",
    ),
    filter_task: str = typer.Option(  # noqa: B008
        None,
        "--filter-task",
        "-f",
        callback=regular_callback,
        help="Filter tasks by json path.",
    ),
    engine_output: bool = typer.Option(  # noqa: B008
        True,
        "--engine-output/--no-engine-output",
        help="Flag to get engine summary output.",
    ),
    deep: bool = typer.Option(False),  # noqa: B008,
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Execute tentacles by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("name", name),
                        ("task", task),
                        ("env", env),
                        ("filter_task", filter_task),
                        ("engine_output", engine_output),
                        ("deep", deep),
                        ("std_in", std_in),
                    ]
                ]
            )
        )
    try:
        if name is not None:
            tentacles = [SquidCLI().execute(GetTentacle(Squid(), name=name, deep=deep))]
            tasks = (
                std_in  # type: ignore
                if std_in
                else [SquidCLI().execute(GetTask(Squid(), name=task, deep=True))]
                if task is not None
                else []
            )
        else:
            tasks = (
                [SquidCLI().execute(GetTask(Squid(), name=task, deep=True))]
                if task is not None
                else []
            )
            tentacles = std_in or []
        if filter_task:
            tentacles = tentacle_tasks_filter_by_jpath(tentacles, filter_task)
            tentacles = tentacle_filter_by_jpath(
                tentacles, "$.tasks.`len`", lambda x: x > 0
            )
        execute(tentacles, tasks, env, dry_run, engine_output)  # type: ignore
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(
    name="tasks", help="Execute Tasks on a tentacle.",
)
def execute_tasks(
    name: str = typer.Option(  # noqa: B008
        None, "--name", "-n", callback=regular_callback, help="Task name.",
    ),
    tentacle: str = typer.Option(  # noqa: B008
        None, "--tentacle", "-t", callback=regular_callback, help="Tentacle name.",
    ),
    env: List[str] = typer.Option(  # noqa: B008
        None, "--env", "-e", callback=regular_callback, help="Parameter for task.",
    ),
    engine_output: bool = typer.Option(  # noqa: B008
        True,
        "--engine-output/--no-engine-output",
        help="Flag to get engine summary output.",
    ),
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Execute tasks by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("name", name),
                        ("tentacle", tentacle),
                        ("env", env),
                        ("engine_output", engine_output),
                        ("std_in", std_in),
                    ]
                ]
            )
        )
    try:
        if name is not None:
            tasks = [SquidCLI().execute(GetTask(Squid(), name=name, deep=True))]
            tentacles = (
                std_in
                if std_in
                else [
                    SquidCLI().execute(GetTentacle(Squid(), name=tentacle, deep=True))
                ]
            )
        else:
            tentacles = (
                [SquidCLI().execute(GetTask(Squid(), name=tentacle, deep=True))]
                if tentacle is not None
                else []
            )
            tasks = std_in or []

        execute(tentacles, tasks, env, dry_run, engine_output)  # type: ignore
    except Exception as e:
        typer.echo(e, err=True)
