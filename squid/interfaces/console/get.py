"""Get tentacles|tasks|plugins."""
import typer

from squid.framework.core import Squid
from squid.framework.core.commands.get import (
    GetPlugin,
    GetPlugins,
    GetTask,
    GetTasks,
    GetTentacle,
    GetTentacles,
)
from squid.framework.services.persistence import SquidPersistence
from squid.interfaces.console import CliOutput, SquidCLI
from squid.interfaces.console.callbacks import cli_callback
from squid.interfaces.console.helpers import compose_output, filter_output, to_str


cli = typer.Typer(
    callback=cli_callback,
    invoke_without_command=True,
    help="Get Tentacles | Tasks | Plugins",
)


@cli.command(name="tentacle")
def get_tentacle(
    name: str = typer.Option(..., "--name", "-n"),  # noqa: B008
    deep: bool = typer.Option(False, "--deep"),  # noqa: B008
    yaml: bool = typer.Option(False, "--yaml", "-y"),  # noqa: B008
    output_stmt: str = typer.Option("", "-o"),  # noqa: B008
    to_raw: bool = typer.Option(False, "-r"),  # noqa: B008
) -> None:
    """Get tentacle by CLI."""
    try:
        tentacle_raw = SquidCLI().execute(GetTentacle(Squid(), name=name, deep=deep))
        tentacle = SquidPersistence().set_abspath([tentacle_raw])[0]
        tentacle_ouput = tentacle.dict() if tentacle else tentacle
        if output_stmt:
            tentacle_ouput = filter_output(output_stmt, tentacle_ouput)
        if to_raw:
            typer.echo(to_str(tentacle_ouput))
        else:
            typer.echo(
                compose_output(
                    tentacle_ouput, CliOutput.YAML if yaml else CliOutput.JSON
                )
            )
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="tentacles")
def get_tentacles(
    query: str = typer.Option("$.name.matches('.*')", "--query", "-q"),  # noqa: B008
    query_link: str = typer.Option(None, "--query-link", "-l"),  # noqa: B008
    deep: bool = typer.Option(False, "--deep"),  # noqa: B008
    yaml: bool = typer.Option(False, "--yaml", "-y"),  # noqa: B008
    output_stmt: str = typer.Option("", "-o"),  # noqa: B008
    to_raw: bool = typer.Option(False, "-r"),  # noqa: B008
) -> None:
    """Get a tentacle by CLI."""
    try:
        tentacles_raw = SquidCLI().execute(
            GetTentacles(
                Squid(),
                query=query,
                query_link=query_link,
                output="tree" if deep else "list",
            )
        )
        tentacles = SquidPersistence().set_abspath(tentacles_raw)
        tentacles_output = [tentacle.dict() for tentacle in tentacles]
        if output_stmt:
            tentacles_output = filter_output(output_stmt, tentacles_output)
        if to_raw:
            typer.echo(to_str(tentacles_output))
        else:
            typer.echo(
                compose_output(
                    tentacles_output, CliOutput.YAML if yaml else CliOutput.JSON
                )
            )
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="task")
def get_task(
    name: str = typer.Option(..., "--name", "-n"),  # noqa: B008
    deep: bool = typer.Option(False, "--deep"),  # noqa: B008
    yaml: bool = typer.Option(False, "--yaml", "-y"),  # noqa: B008
    output_stmt: str = typer.Option("", "-o"),  # noqa: B008
    to_raw: bool = typer.Option(False, "-r"),  # noqa: B008
) -> None:
    """Get task by CLI."""
    try:
        task = SquidCLI().execute(GetTask(Squid(), name=name, deep=deep))  # type: ignore
        task_output = task.dict() if task else task
        if output_stmt:
            task_output = filter_output(output_stmt, task_output)
        if to_raw:
            typer.echo(to_str(task_output))
        else:
            typer.echo(
                compose_output(task_output, CliOutput.YAML if yaml else CliOutput.JSON)
            )
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="tasks")
def get_tasks(
    query: str = typer.Option("$.name.matches('.*')", "--query", "-q"),  # noqa: B008
    query_link: str = typer.Option(None, "--query-link", "-l"),  # noqa: B008
    deep: bool = typer.Option(False, "--deep"),  # noqa: B008
    yaml: bool = typer.Option(False, "--yaml", "-y"),  # noqa: B008
    output_stmt: str = typer.Option("", "-o"),  # noqa: B008
    to_raw: bool = typer.Option(False, "-r"),  # noqa: B008
) -> None:
    """Get tasks by CLI."""
    try:
        tasks = SquidCLI().execute(GetTasks(Squid(), query=query, query_link=query_link, output="tree" if deep else "list"))  # type: ignore
        tasks_output = [task.dict() for task in tasks]
        if output_stmt:
            tasks_output = filter_output(output_stmt, tasks_output)
        if to_raw:
            typer.echo(to_str(tasks_output))
        else:
            typer.echo(
                compose_output(tasks_output, CliOutput.YAML if yaml else CliOutput.JSON)
            )
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="plugin")
def get_plugin(
    name: str = typer.Option(..., "--name", "-n"),  # noqa: B008
    yaml: bool = typer.Option(False, "--yaml", "-y"),  # noqa: B008
    output_stmt: str = typer.Option("", "-o"),  # noqa: B008
    to_raw: bool = typer.Option(False, "-r"),  # noqa: B008
) -> None:
    """Get plugin by CLI."""
    try:
        plugin_raw = SquidCLI().execute(GetPlugin(Squid(), name=name))
        plugin = SquidPersistence().set_abspath([plugin_raw])[0]
        plugin_output = plugin.dict() if plugin else plugin
        if output_stmt:
            plugin_output = filter_output(output_stmt, plugin_output)
        if to_raw:
            typer.echo(to_str(plugin_output))
        else:
            typer.echo(
                compose_output(
                    plugin_output, CliOutput.YAML if yaml else CliOutput.JSON
                )
            )
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="plugins")
def get_plugins(
    query: str = typer.Option("$.name.matches('.*')", "--query", "-q"),  # noqa: B008
    yaml: bool = typer.Option(False, "--yaml", "-y"),  # noqa: B008
    output_stmt: str = typer.Option("", "-o"),  # noqa: B008
    to_raw: bool = typer.Option(False, "-r"),  # noqa: B008
) -> None:
    """Get plugins by CLI."""
    try:
        plugins_raw = SquidCLI().execute(GetPlugins(Squid(), query=query))
        plugins = SquidPersistence().set_abspath(plugins_raw)
        plugins_output = [plugin.dict() for plugin in plugins]
        if output_stmt:
            plugins_output = filter_output(output_stmt, plugins_output)
        if to_raw:
            typer.echo(to_str(plugins_output))
        else:
            typer.echo(
                compose_output(
                    plugins_output, CliOutput.YAML if yaml else CliOutput.JSON
                )
            )
    except Exception as e:
        typer.echo(e, err=True)
