"""Init command."""
# flake8: noqa
import os

import typer

from squid.framework.core import Squid
from squid.framework.core.commands.init import Init
from squid.framework.core.commands.add import AddTentacles, Tentacle
from squid.interfaces.console import SquidCLI

cli = typer.Typer()

SQUID_ASCII = """
 /############################################################
| ##______________________________________________________/ ##
| ##                                                      | ##
| ##      /######   /######  /##   /## /###### /#######   | ##
| ##     /##__  ## /##__  ##| ##  | ##|_  ##_/| ##__  ##  | ##
| ##    | ##  \__/| ##  \ ##| ##  | ##  | ##  | ##  \ ##  | ##
| ##    |  ###### | ##  | ##| ##  | ##  | ##  | ##  | ##  | ##
| ##     \____  ##| ##  | ##| ##  | ##  | ##  | ##  | ##  | ##
| ##     /##  \ ##| ##/## ##| ##  | ##  | ##  | ##  | ##  | ##
| ##    |  ######/|  ######/|  ######/ /######| #######/  | ##
| ##     \______/  \____ ### \______/ |______/|_______/   | ##
| ##                    \__/                              | ##
| ##                                                      | ##
| ############################################################
|/___________________________________________________________/

PROJECT SUCCESSFULLY INITIALIZED...!!! Enjoy!!!
"""


@cli.callback(invoke_without_command=True)
def init(
    not_add_root: bool = typer.Option(
        False, "--not-add-root", help="Flag if you wish not to add Root as a tentacle."
    ),  # noqa: B008
) -> None:
    """Init Squid Project."""
    try:
        SquidCLI().execute(Init(Squid(), path=os.curdir))  # type: ignore
        if not not_add_root:
            SquidCLI().execute(
                AddTentacles(
                    receiver=Squid(),
                    tentacles=[Tentacle(name="head", type="root", path=os.curdir)],
                    upsert=True,
                )
            )
        typer.secho(SQUID_ASCII, fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


if __name__ == "__main__":
    cli()
