"""Add tentacles|tasks|plugins."""
import os

import typer

from squid.framework.core import Squid
from squid.framework.core.commands.add import AddPlugins, AddTasks, AddTentacles
from squid.framework.core.models.bases import PluginCls
from squid.interfaces.console import SquidCLI
from squid.interfaces.console.callbacks import (
    cli_callback,
    editor_callback,
    regular_callback,
    std_in_callback,
)


cli = typer.Typer(
    callback=cli_callback,
    invoke_without_command=True,
    help="Add Tentacles | Tasks | Plugins",
)


@cli.command(name="tentacles")
def add_tentacles(
    name: str = typer.Option(  # noqa: B008
        "", "--name", "-n", callback=regular_callback,
    ),
    path: str = typer.Option(  # noqa: B008
        os.curdir, "--path", "-p", callback=regular_callback
    ),
    type: str = typer.Option(  # noqa: B008
        "", "--type", "-t", callback=regular_callback
    ),
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input",
    ),
    editor: bool = typer.Option(  # noqa: B008
        None,
        "--editor",
        "-e",
        callback=editor_callback,
        help="Flag to use the default text editor of the system with a template preloaded.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Add tentacles by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("name", name),
                        ("path", path),
                        ("type", type),
                        ("std_in", std_in),
                        ("editor", editor),
                    ]
                ]
            )
        )
    try:
        tentacles = editor or std_in
        if dry_run:
            typer.echo(tentacles)
        else:
            SquidCLI().execute(AddTentacles(Squid(), tentacles))  # type: ignore
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="tasks")
def add_tasks(
    name: str = typer.Option(  # noqa: B008
        "", "--name", "-n", callback=regular_callback,
    ),
    category: str = typer.Option(  # noqa: B008
        "", "--category", "-c", callback=regular_callback
    ),
    envs: str = typer.Option("", callback=regular_callback),  # noqa: B008
    cmds: str = typer.Option("", callback=regular_callback),  # noqa: B008
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input",
    ),
    editor: bool = typer.Option(  # noqa: B008
        None,
        "--editor",
        "-e",
        callback=editor_callback,
        help="Flag to use the default text editor of the system with a template preloaded.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Add tasks by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("name", name),
                        ("category", category),
                        ("envs", envs),
                        ("cmds", cmds),
                        ("std_in", std_in),
                        ("editor", editor),
                    ]
                ]
            )
        )
    try:
        tasks = editor or std_in
        if dry_run:
            typer.echo(tasks)
        else:
            SquidCLI().execute(AddTasks(Squid(), tasks))  # type: ignore
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="plugins")
def add_plugins(
    name: str = typer.Option(  # noqa: B008
        "", "--name", "-n", callback=regular_callback,
    ),
    source: str = typer.Option(  # noqa: B008
        os.curdir, "--source", "-s", callback=regular_callback
    ),
    path: str = typer.Option(  # noqa: B008
        os.curdir, "--path", "-p", callback=regular_callback
    ),
    entrypoint: str = typer.Option("", callback=regular_callback),  # noqa: B008
    cls: PluginCls = typer.Option(  # noqa: B008
        PluginCls.CLI, case_sensitive=True, callback=regular_callback
    ),
    attributes: str = typer.Option(  # noqa: B008
        "", "--attributes", "-a", callback=regular_callback
    ),
    std_in: bool = typer.Option(  # noqa: B008
        None,
        "--std-in",
        "-i",
        callback=std_in_callback,
        help="Flag to use stdin as the input",
    ),
    editor: bool = typer.Option(  # noqa: B008
        None,
        "--editor",
        "-e",
        callback=editor_callback,
        help="Flag to use the default text editor of the system with a template preloaded.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Add plugins by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("name", name),
                        ("path", path),
                        ("source", source),
                        ("entrypoint", entrypoint),
                        ("cls", cls),
                        ("attributes", attributes),
                        ("std_in", std_in),
                        ("editor", editor),
                    ]
                ]
            )
        )
    try:
        plugins = editor or std_in
        if dry_run:
            typer.echo(plugins)
        else:
            SquidCLI().execute(AddPlugins(Squid(), plugins))  # type: ignore
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)
