"""Service to execute DAG Tasks."""
import os
from typing import Callable, cast, List, Optional

from squid.framework.core.models.bases import (
    Task,
    TaskDAG,
    Tentacle,
)
from squid.framework.core.models.responses import (
    EngineResponse,
    ServiceResponse,
    StatusEnum,
)
from squid.framework.services import SingletonService
from squid.framework.services.engine.engine_abc import EngineABC


class SquidEngine(metaclass=SingletonService):
    """Singleton Entity."""

    _engine: Optional[EngineABC] = None

    def __init__(self, engine: Optional[EngineABC] = None) -> None:
        """Obtain Singleton Instance.

        Args:
            engine: Optional[EngineABC]

        """
        self._engine = engine

    @staticmethod
    def extract_taskdag_from_tentacles(tentacles: List[Tentacle]) -> List[TaskDAG]:
        """Returns TaskDAG from a list of tentacles with tasks.

        Tranform a list of Tentacles models to a list of Taskdag

        Args:
            tentacles: List[Tentacle]

        Returns:
            List[TaskDAG]

        """
        recursive_tasks_dag: Callable[[List[Task], str, int], List[TaskDAG]] = (
            lambda tasks, tentacle_path, exec_order: [
                TaskDAG(
                    name=task.name,
                    cmds=task.cmds,
                    envs=task.envs,
                    wd=tentacle_path,
                    exec_order=exec_order,
                    logpath=os.path.join(tentacle_path, ".tentacle", "logs"),
                    tasks=recursive_tasks_dag(
                        cast(Task, task).tasks, tentacle_path, exec_order
                    )
                    if hasattr(task, "tasks")
                    else [],
                )
                for task in tasks
            ]
            if isinstance(tasks, list)
            else []
        )

        def build_task_dag(
            tentacles_iter: List[Tentacle], idx: int = 0
        ) -> List[TaskDAG]:
            list_task_dag: List[TaskDAG] = []
            for tentacle in tentacles_iter:
                list_task_dag = [
                    *list_task_dag,
                    *recursive_tasks_dag(tentacle.tasks, tentacle.path, idx),
                ]
                if hasattr(tentacle, "tentacles") and bool(tentacle.tentacles):
                    list_task_dag = [
                        *list_task_dag,
                        *build_task_dag(
                            tentacles_iter=tentacle.tentacles, idx=(idx + 1)
                        ),
                    ]
            return list_task_dag

        return build_task_dag(tentacles)

    def run(self, tentacles: List[Tentacle]) -> ServiceResponse:
        """Run tasks.

        Args:
            tentacles: List[Tentacle]

        Returns:
            ServiceResponse

        Rasises:
            Exception: Engine type is not defined on the construct. # noqa: DAR401

        """
        if self._engine is None:
            raise Exception("Engine type is not defined.")

        dag = SquidEngine.extract_taskdag_from_tentacles(tentacles)
        engine_response_raw: dict = self._engine.execute_dag(dag=dag)
        engine_response: EngineResponse = EngineResponse(**engine_response_raw)
        return ServiceResponse(status=StatusEnum.success, data=engine_response)
