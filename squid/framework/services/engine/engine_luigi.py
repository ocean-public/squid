"""Engine implementation of Luigi."""
from __future__ import annotations

import functools
import logging
import multiprocessing
import os
import re
import time
from typing import Any, Callable, cast, Dict, List, Optional, Tuple, Union

import luigi
from luigi.contrib.external_program import ExternalProgramTask
from luigi.execution_summary import LuigiRunResult
import luigi.tools.deps_tree as deps_tree
from plumbum import FG, local

from squid.framework.core.models.bases import TaskDAG
from squid.framework.services.engine.engine_abc import EngineABC
from squid.framework.services.persistence import SquidPersistence

logger = logging.getLogger("luigi-interface")


def parse_env(local_container: Any, env_name: str, datatype: str, default: Any) -> Any:
    """Parse env to data type."""
    env_eval = local_container.env.get(env_name, default)
    PARSERS = {"bool": lambda x: x if isinstance(x, bool) else x.lower() == "true"}
    return PARSERS.get(datatype, lambda x: str(x))(env_eval)


def replace_env_on_args(args: List[str], local_container: Any) -> List[str]:
    """Replace env Vars on args."""
    args_parsed = []
    for arg in args:
        regex = r"\${(.[^\$][^\{]*)\}"
        for match in re.finditer(regex, arg):
            for env_extract in match.groups():
                env_eval = env_extract.split(":-")
                env_value = local_container.env.get(env_eval[0].strip(), None)
                if env_value is None and len(env_eval) == 2:
                    arg = arg.replace(
                        "${" + env_extract + "}", env_eval[1].strip().strip("'")
                    )
                else:
                    arg = arg.replace("${" + env_extract + "}", env_value)
        args_parsed.append(arg)
    return args_parsed


class BashTaskParameter(luigi.ListParameter):
    """Class custom for BashTasks as parameters."""

    def parse(self, obj: BashTask = None) -> Optional[BashTask]:
        """Return a BashTask object.

        Args:
            obj: BashTask

        Returns:
            BashTask

        """
        return obj or None

    def serialize(self, obj: BashTask) -> str:
        """Return a BashTask object serialize.

        Args:
            obj: BashTask

        Returns:
            str

        """
        return str(obj)


class BashTask(ExternalProgramTask):
    """Class implementation for Task to run bash commands."""

    upstream_task = BashTaskParameter()
    cmds = luigi.ListParameter()
    envs = luigi.DictParameter()
    wd = luigi.Parameter()
    logpath = luigi.Parameter()
    exec_order = luigi.IntParameter(default=0)

    @property
    def priority(self) -> luigi.IntParameter:
        """Return Execution order for paralel tasks.

        Returns:
            Execution order.

        """
        return self.exec_order

    def requires(self) -> List[BashTask]:
        """Return others BashTask dependencies.

        Define the dependencies for this task

        Returns:
            BaskTasks.

        """
        return cast(List[BashTask], self.upstream_task)

    @staticmethod
    def _parse_cmds(cmds: luigi.ListParameter) -> List[Any]:
        """Exec cmds."""
        ENGINE_CMDS_EXEC_SEQ = parse_env(
            local, "ENGINE_CMDS_EXEC_SEQ", "str", "isolated"
        )

        cmds = [
            {"cmd": cmd["cmd"], "args": replace_env_on_args(cmd["args"], local)}
            for cmd in cmds
        ]
        if len(cmds) == 1:
            return [local.get(cmds[0]["cmd"]).__getitem__(cmds[0]["args"])]

        elif ENGINE_CMDS_EXEC_SEQ == "pipe":
            return [
                (
                    functools.reduce(
                        lambda x, y: local.get(x["cmd"]).__getitem__(x["args"])
                        | local.get(y["cmd"]).__getitem__(y["args"]),
                        cmds,
                    )
                )
            ]

        elif ENGINE_CMDS_EXEC_SEQ == "and":
            ENGINE_CMDS_EXEC_SEQ_SHELL = parse_env(
                local, "ENGINE_CMDS_EXEC_SEQ_SHELL", "str", "bash"
            )
            cmds_with_and = (
                functools.reduce(
                    lambda x, y: x + y,
                    [
                        ["&&", cmd["cmd"]] + list(cmd["args"])
                        if idx > 0
                        else [cmd["cmd"]] + list(cmd["args"])
                        for idx, cmd in enumerate(cmds)
                    ],
                )
                if len(cmds) > 1
                else [cmds[0]["cmd"]] + cmds[0]["args"]
            )
            return [
                local.get(ENGINE_CMDS_EXEC_SEQ_SHELL).__getitem__(
                    ["-c", " ".join(cmds_with_and)]
                )
            ]

        elif ENGINE_CMDS_EXEC_SEQ == "isolated":
            return [local.get(cmd["cmd"]).__getitem__(cmd["args"]) for cmd in cmds]

        else:
            raise Exception(
                f"Env variable ENGINE_CMDS_EXEC_SEQ with value {ENGINE_CMDS_EXEC_SEQ} is not valid. Please use 'pipe', 'isolated' or 'and' as a valid value."
            )

    @staticmethod
    def _exec_cmds(cmds: luigi.ListParameter) -> Tuple[List[str], List[str]]:
        """Exec cmds."""
        stdouts = []
        stderrs = []
        ENGINE_CMD_FG = parse_env(local, "ENGINE_CMD_FG", "bool", False)
        cmds_to_exec = BashTask._parse_cmds(cmds=cmds)
        for cmd in cmds_to_exec:
            if ENGINE_CMD_FG:
                _ = cmd & FG

            else:
                exit_code, stdout, stderr = cmd.run()
                status = "----SUCCESS----" if exit_code == 0 else "----ERROR----"
                stdouts.append(f"{status}\n{stdout}")
                stderrs.append(f"{status}\n{stderr}")
        return stdouts, stderrs

    @staticmethod
    def _set_logging_engine() -> None:
        """Enable logging on engine."""
        ENGINE_LOG_ENABLE = parse_env(local, "ENGINE_LOG_ENABLE", "bool", True)
        ENGINE_LOG_LEVEL = parse_env(local, "ENGINE_LOG_LEVEL", "str", "info")
        LOGGING_LEVEL = {
            "debug": logging.DEBUG,
            "info": logging.INFO,
            "warning": logging.WARNING,
            "error": logging.ERROR,
            "critical": logging.CRITICAL,
            "not_set": logging.NOTSET,
        }
        if ENGINE_LOG_ENABLE:
            logger.setLevel(LOGGING_LEVEL.get(ENGINE_LOG_LEVEL.lower(), logging.INFO))
        else:
            logger.setLevel(logging.NOTSET)

    @staticmethod
    def _write_log_engine_task(
        stdouts: List[str], stderrs: List[str], output: Callable
    ) -> None:
        """Write log for engine task."""
        ENGINE_CMD_FG = parse_env(local, "ENGINE_CMD_FG", "bool", False)
        if ENGINE_CMD_FG:
            with output().open("w") as outfile:
                outfile.write("---- DONE ----\n")

        else:
            stdouts_parsed = "\n".join(stdouts)
            stderrs_parsed = "\n".join(stderrs)
            with output().open("w") as outfile:
                outfile.write("---- EXECUTION ----\n")
                outfile.write(f"STDOUT\n{stdouts_parsed}")
                outfile.write(f"STDERR\n{stderrs_parsed}")

    def run(self) -> None:
        """Execute commands for task."""
        local.cwd.chdir(self.wd)
        local.env.update({key: value for key, value in self.envs.items()})
        BashTask._set_logging_engine()
        stdouts, stderrs = BashTask._exec_cmds(cmds=self.cmds)
        BashTask._write_log_engine_task(
            stdouts=stdouts, stderrs=stderrs, output=self.output
        )

    def output(self) -> luigi.LocalTarget:
        """Return outputs for task.

        Writes log files for the execution of task

        Returns:
            LocalTarget.

        """
        return luigi.LocalTarget(self.logpath)


class EngineLuigi(EngineABC):
    """Class implementation of EngineABC for Luigi lib."""

    def execute_dag(self, dag: List[TaskDAG]) -> Dict:
        """Execute dag tasks.

        Create instances of BashTasks on DAG and execute it

        Args:
            dag: List[TaskDAG]

        Returns:
            Object for outputs of tasks.

        """
        raw_dag = [task_dag.dict() for task_dag in dag]
        tasks_to_build = EngineLuigi._build_task(raw_dag)

        luigi_run_result: LuigiRunResult = luigi.build(
            tasks_to_build,
            workers=multiprocessing.cpu_count(),
            local_scheduler=True,
            detailed_summary=True,
        )
        return {
            "summary": luigi_run_result.summary_text,
            "exec_tree": [
                deps_tree.print_tree(task_build) for task_build in tasks_to_build
            ],
        }

    @staticmethod
    def _build_task(dag: List[Any]) -> List[BashTask]:
        """Return dag tasks on BashTasks instances.

        Create instances of BashTasks on DAG

        Args:
            dag: List[Any]

        Returns:
            List of BashTasks.

        """
        timestamp = time.time_ns()
        build_task: Callable[[list], Union[List[BashTask], list]] = (
            lambda tasks: [
                BashTask(
                    cmds=task.get("cmds"),
                    envs=task.get("envs"),
                    wd=SquidPersistence().item_abspath(path=task.get("wd")),
                    logpath=SquidPersistence().item_abspath(
                        path=os.path.join(
                            task.get("logpath"), f"{task.get('name')}_{timestamp}.log"
                        )
                    ),
                    exec_order=task.get("exec_order", 0),
                    upstream_task=build_task(task["tasks"])
                    if "tasks" in task and bool(task["tasks"])
                    else [],
                )
                for task in tasks
            ]
            if isinstance(tasks, list)
            else []
        )
        return build_task(dag)
