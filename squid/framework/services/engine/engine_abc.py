"""Definitions of type engines."""
from abc import ABC, abstractmethod
from typing import Dict, List

from squid.framework.core.models.bases import TaskDAG


class EngineABC(ABC):
    """Abstract class to be implemented by engine types."""

    @abstractmethod
    def execute_dag(self, dag: List[TaskDAG]) -> Dict:
        """Abstract method to be implemented for Execute DAG.

        Args:
            dag: List[TaskDAG]

        """
        pass
