"""Service to manage plugin Registry."""
import inspect
from typing import Callable, Dict, List

from pluginbase import PluginBase, PluginSource

from squid.framework.core.models.bases import Plugin
from squid.framework.services import SingletonService


class SquidRegistry(metaclass=SingletonService):
    """Singleton Entity.

    Params:
        _plugin_base: PluginBase
        _plugin_sources: List[PluginSource]
        entries: Dict[str, Callable]

    """

    _plugin_base: PluginBase
    _plugin_sources: List[PluginSource]
    entries: Dict[str, Callable]

    def __init__(self) -> None:
        """Obtain singleton instance."""
        self._plugin_base = PluginBase(package="squid.plugins")
        self.entries = {}
        self._plugin_sources = []

    def entry(self, identifier: str, fn: Callable) -> None:
        """Set entry for plugin.

        Args:
            identifier: str
            fn: Callable

        """
        self.entries[identifier] = fn

    def discovery(self, plugins: List[Plugin]) -> None:
        """Setup entries for plugins.

        Args:
            plugins: List[Plugin]

        """
        if not self._plugin_sources:
            entrypoints = {
                plugin.name: {
                    "file": plugin.entrypoint.split(":")[0],
                    "method": plugin.entrypoint.split(":")[1],
                }
                for plugin in plugins
            }

            self._plugin_sources = [
                self._plugin_base.make_plugin_source(
                    searchpath=[plugin.path], identifier=plugin.name
                )
                for plugin in plugins
            ]

            for plugin_source in self._plugin_sources:
                for plugin_name in plugin_source.list_plugins():
                    eval_entrypoints = {
                        k: {
                            "file": v["file"].format(file_name=plugin_name),
                            "method": v["method"],
                        }
                        for k, v in entrypoints.items()
                    }
                    if (
                        plugin_source.identifier in eval_entrypoints
                        and plugin_name
                        == eval_entrypoints[plugin_source.identifier]["file"]
                    ):
                        plugin = plugin_source.load_plugin(plugin_name)
                        for name, data in inspect.getmembers(plugin):
                            if (
                                name
                                == eval_entrypoints[plugin_source.identifier]["method"]
                            ):
                                data(self)

    def context(self, identifier: str = "squid_plugins") -> PluginSource:
        """Get source context.

        Args:
            identifier: str

        Returns:
            PluginSource

        Raises:
            Exception:  Name <plugin_name> is not register.
                        Run discovery stage first.

        """
        if self._plugin_sources:
            source_ctx = None
            for plugin_source in self._plugin_sources:
                if plugin_source.identifier == identifier:
                    source_ctx = plugin_source
                    break
            if source_ctx is None:
                raise Exception(f"Name {identifier} is not register.")
            return source_ctx
        raise Exception("Run discovery stage first.")
