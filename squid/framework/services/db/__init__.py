"""Service to execute operations on DB."""
import re
from typing import Any, Callable, Dict, KeysView, Optional, Union

from squid.framework.core.models.bases import (
    Link,
    LinkedCls,
    Plugin,
    SquidTable,
    Task,
    TaskCmd,
    TaskRef,
    Tentacle,
    TentacleRef,
)
from squid.framework.core.models.responses import (
    DbResponseDelete,
    DbResponseGet,
    DbResponseInsert,
    DbResponseQuery,
    DbResponseUpdate,
    ServiceResponse,
    StatusEnum,
)
from squid.framework.services import SingletonService
from squid.framework.services.db.db_abc import DbABC


class SquidDb(metaclass=SingletonService):
    """Singleton Entity."""

    _db: Optional[DbABC] = None

    def __init__(self, db: Optional[DbABC] = None) -> None:
        """Obtain Singleton Instance.

        Args:
            db: Optional[DbABC]

        """
        self._db = db

    @staticmethod
    def _query_parser(stmt: str, table: SquidTable, keyword: str = "SquidQuery") -> str:
        """Return statement for query validated and parsed.

        Validate against field declare con Models depending on Tables

        Args:
            stmt: str
            table: SquidTable
            keyword: str = "SquidQuery"

        Raises:
            Exception: Field <FIELD> not allowed. # noqa: DAR401

        Returns:
            str

        """
        fields_allowed_by_table = {
            SquidTable.TENTACLES: [
                *TentacleRef.__fields__.keys(),
                *Tentacle.__fields__.keys(),
            ],
            SquidTable.TASKS: [
                *TaskRef.__fields__.keys(),
                *Task.__fields__.keys(),
                *TaskCmd.__fields__.keys(),
            ],
            SquidTable.PLUGINS: Plugin.__fields__.keys(),
            SquidTable.LINKS: [*Link.__fields__.keys(), *LinkedCls.__fields__.keys()],
        }
        validate_field_key: Callable[
            [str], bool
        ] = lambda field: field in fields_allowed_by_table.get(table, [])
        regex = r"\$\.(\w*)\.?(\w*)"  # REGEX for detecting keys and misc functions
        matches = re.finditer(regex, stmt)
        for match in matches:
            group = [match_group for match_group in match.groups() if match_group]
            if not validate_field_key(group[0]):
                raise Exception(f"Field {group[0]} not allowed.")
            # TODO: Include list of misc functions available
        return stmt.replace("$.", f"{keyword}.")

    def _insert_tentacle(
        self, tentacle: Union[TentacleRef, Tentacle], upsert: bool
    ) -> int:
        """Insert Tentacle.

        Args:
            tentacle: Union[TentacleRef, Tentacle]
            upsert: bool

        Returns:
            int

        Rasises:
            Exception: Operation for class Tentacle not yet supported. # noqa: DAR401
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        doc_id = -1
        if isinstance(tentacle, TentacleRef):
            doc_id = self._db.insert(
                record=tentacle.dict(), table=SquidTable.TENTACLES.value, upsert=upsert
            )
        if isinstance(tentacle, Tentacle):
            # TODO: Create recursive case for Tentacle
            raise Exception("Operation for class Tentacle not yet supported")
        return doc_id

    def _insert_task(self, task: Union[TaskRef, Task], upsert: bool) -> int:
        """Insert Task.

        Args:
            task: Union[TaskRef, Task]
            upsert: bool

        Returns:
            int

        Rasises:
            Exception: Operation for class Task not yet supported. # noqa: DAR401
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        doc_id = -1
        if isinstance(task, TaskRef):
            doc_id = self._db.insert(
                record=task.dict(), table=SquidTable.TASKS.value, upsert=upsert
            )
        if isinstance(task, Task):
            # TODO: Create recursive case for Task
            raise Exception("Operation for class Task not yet supported")
        return doc_id

    def _insert_plugin(self, plugin: Plugin, upsert: bool) -> int:
        """Insert Plugin.

        Args:
            plugin: Plugin
            upsert: bool

        Returns:
            int

        Rasises:
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        return self._db.insert(
            record=plugin.dict(), table=SquidTable.PLUGINS.value, upsert=upsert
        )

    def _insert_link(self, link: Link, upsert: bool) -> int:
        """Insert Link.

        Args:
            link: Link
            upsert: bool

        Returns:
            int

        Rasises:
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        upsert_stmt = SquidDb._query_parser(
            stmt=f"($.name == '{link.name}') & ($.cls == '{link.cls}')",
            table=SquidTable.LINKS,
        )
        return self._db.insert(
            record=link.dict(),
            table=SquidTable.LINKS.value,
            upsert=upsert,
            upsert_stmt=upsert_stmt,
        )

    def insert(self, record: Any, upsert: bool = False) -> ServiceResponse:
        """Insert Record.

        Args:
            record: Any
            upsert: bool = False

        Returns:
            ServiceResponse

        Rasises:
            Exception: record type is not supported. # noqa: DAR401

        """
        _record_type: Dict = {
            Task: self._insert_task,
            TaskRef: self._insert_task,
            Tentacle: self._insert_tentacle,
            TentacleRef: self._insert_tentacle,
            Plugin: self._insert_plugin,
            Link: self._insert_link,
        }
        insert_fn = _record_type.get(type(record), None)
        if insert_fn is None:
            raise Exception(f"record type is not supported {type(record)}")
        result = insert_fn(record, upsert)
        return ServiceResponse(
            status=StatusEnum.success if result > 0 else StatusEnum.failed,
            data=DbResponseInsert(insert=result),
        )

    def query(self, stmt: str, table: SquidTable) -> ServiceResponse:
        """Query Records.

        Args:
            stmt: str
            table: SquidTable

        Returns:
            ServiceResponse

        Rasises:
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        stmt_parsed = SquidDb._query_parser(stmt=stmt, table=table)
        records = self._db.query(stmt=stmt_parsed, table=table.value)
        results = [
            TentacleRef.parse_obj(record)
            if table == SquidTable.TENTACLES
            else TaskRef.parse_obj(record)
            if table == SquidTable.TASKS
            else Plugin.parse_obj(record)
            if table == SquidTable.PLUGINS
            else Link.parse_obj(record)
            if table == SquidTable.LINKS
            else None
            for record in records
        ]
        return ServiceResponse(
            status=StatusEnum.success,
            data=DbResponseQuery.parse_obj({"query": results}),
        )

    def update(self, kvs: Dict, table: SquidTable, stmt: str) -> ServiceResponse:
        """Update Records.

        Args:
            kvs: Dict
            stmt: str
            table: SquidTable

        Returns:
            ServiceResponse

        Rasises:
            Exception: Keys are not supported . # noqa: DAR401
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        is_valid_keys_to_update: Callable[
            [KeysView[str], KeysView[str]], bool
        ] = lambda x, y: not bool(set(x).difference(set(y)))
        verify_operations: Callable[[Dict], Dict[str, bool]] = lambda items: {
            k: bool(v.get("op", "")) for k, v in items.items() if isinstance(v, dict)
        }

        keys_to_update = kvs.keys()
        if table == SquidTable.TENTACLES and not is_valid_keys_to_update(
            keys_to_update, TentacleRef.__fields__.keys()
        ):
            raise Exception(f"Keys are not supported {keys_to_update}")
        if table == SquidTable.TASKS and not is_valid_keys_to_update(
            keys_to_update, TaskRef.__fields__.keys()
        ):
            raise Exception(f"Keys are not supported {keys_to_update}")
        if table == SquidTable.PLUGINS and not is_valid_keys_to_update(
            keys_to_update, Plugin.__fields__.keys()
        ):
            raise Exception(f"Keys are not supported {keys_to_update}")
        if table == SquidTable.LINKS and not is_valid_keys_to_update(
            keys_to_update, Link.__fields__.keys()
        ):
            raise Exception(f"Keys are not supported {keys_to_update}")

        if not all(list(verify_operations(kvs).values())):
            raise Exception("ops key is missing.")

        stmt_parsed = SquidDb._query_parser(stmt=stmt, table=table)
        results = self._db.update(kvs=kvs, table=table.value, stmt=stmt_parsed)
        return ServiceResponse(
            status=StatusEnum.success, data=DbResponseUpdate(update=results),
        )

    def delete(self, stmt: str, table: SquidTable) -> ServiceResponse:
        """Delete Records.

        Args:
            stmt: str
            table: SquidTable

        Returns:
            ServiceResponse

        Rasises:
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        stmt_parsed = SquidDb._query_parser(stmt=stmt, table=table)
        results = self._db.delete(stmt=stmt_parsed, table=table.value)
        return ServiceResponse(
            status=StatusEnum.success, data=DbResponseDelete(delete=results),
        )

    def get(self, stmt: str, table: SquidTable) -> ServiceResponse:
        """Get Record.

        Args:
            stmt: str
            table: SquidTable

        Returns:
            ServiceResponse

        Rasises:
            Exception: Db type is not defined. # noqa: DAR401

        """
        if self._db is None:
            raise Exception("Db type is not defined.")

        stmt_parsed = SquidDb._query_parser(stmt=stmt, table=table)
        record = self._db.get(stmt=stmt_parsed, table=table.value)
        result = {
            SquidTable.TENTACLES: lambda item: TentacleRef.parse_obj(item)
            if item is not None
            else {},
            SquidTable.TASKS: lambda item: TaskRef.parse_obj(item)
            if item is not None
            else {},
            SquidTable.PLUGINS: lambda item: Plugin.parse_obj(item)
            if item is not None
            else {},
            SquidTable.LINKS: lambda item: Link.parse_obj(item)
            if item is not None
            else {},
        }
        return ServiceResponse(
            status=StatusEnum.success,
            data=DbResponseGet.parse_obj(
                {"get": result.get(table, lambda item: {})(record)}
            ),
        )
