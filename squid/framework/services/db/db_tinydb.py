"""Module implementation of TinyDB."""
import os
from typing import Any, Callable, cast, Dict, List, Optional, Union

from tinydb import Query, TinyDB
from tinydb.operations import add, decrement, delete, increment, set, subtract

from squid.framework.services.db.db_abc import DbABC
from squid.framework.services.persistence import (
    PersistenceResponsePath,
    SquidPersistence,
)


class DbTinyDB(DbABC):
    """Class implementation of TinyDB."""

    _main_db: TinyDB

    def __init__(self) -> None:
        """Initialize Db implementation."""
        squid_path = cast(
            PersistenceResponsePath, SquidPersistence().get_squid_path().data
        ).path
        self._main_db = TinyDB(
            os.path.join(squid_path, "db.json"), sort_keys=True, indent=4
        )

    @staticmethod
    def _convert_query(stmt: str) -> Query:
        """Convert Query statement To class.

        Args:
            stmt: str

        Returns:
            Query

        Rasises:
            Exception: raise. # noqa: DAR401

        """
        try:
            # TODO: Evaluate the use of ast.literal_eval more secure using custom
            # https://exceptionshub.com/malformed-string-valueerror-ast-literal_eval-with-string-representation-of-tuple.html
            stmt_converted = eval(stmt, {"SquidQuery": Query()})  # noqa: S307
            return stmt_converted
        except Exception as e:
            print(f"DbTinyDB -> _convert_query: {e}")
            raise

    @staticmethod
    def _convert_operations(kvs: Dict[str, Any]) -> Union[Callable, Dict[str, Any]]:
        """Convert Operations update.

        Args:
            kvs: Dict[str, Any]

        Returns:
             Union[Callable, Dict[str, Any]]

        Rasises:
            Exception: raise. # noqa: DAR401

        """
        try:
            operation: Union[Optional[Callable], Dict] = None
            operations: Dict[str, Callable[[str, Optional[str]], Callable]] = {
                "delete": lambda key, value: delete(key),
                "increment": lambda key, value: increment(key),
                "decrement": lambda key, value: decrement(key),
                "add": lambda key, value: add(key, value),
                "subtract": lambda key, value: subtract(key, value),
                "set": lambda key, value: set(key, value),
            }
            for k, v in kvs.items():
                if isinstance(v, dict):
                    operation = operations.get(v.get("op", ""), lambda x, y: {})(
                        k, v.get("value")
                    )
                    break
            if operation is None:
                return kvs
            if len(kvs.keys()) > 1:
                # TODO: Create custom transform method for use multiple operations
                raise Exception("Multiple operations or mixing is not supported.")
            return operation
        except Exception as e:
            print(f"DbTinyDB -> _convert_operations: {e}")
            raise

    def insert(
        self, record: Dict, table: str, upsert: bool = False, upsert_stmt: str = None
    ) -> int:
        """Insert record to table.

        Args:
            record: Dict
            table: str
            upsert: bool = False
            upsert_stmt: str = None

        Returns:
            int

        Raises:
            Record already exists <record>.  # noqa: DAR401

        """
        if upsert:
            upsert_query = (
                Query().name == record.get("name")
                if upsert_stmt is None
                else DbTinyDB._convert_query(stmt=upsert_stmt)
            )
            doc_id = self._main_db.table(table).upsert(record, upsert_query)
            return isinstance(doc_id[0], int)

        if self._main_db.table(table).contains(Query().name == record.get("name")):
            raise Exception(f"Record already exists {record}")

        doc_id = self._main_db.table(table).insert(record)
        return doc_id

    def update(self, kvs: Dict, table: str, stmt: str) -> List[int]:
        """Update records on table of db.

        Args:
            kvs: Dict
            table: str
            stmt: str

        Returns:
            List[int]

        """
        query_stmt = DbTinyDB._convert_query(stmt=stmt)
        kvs_converted = DbTinyDB._convert_operations(kvs=kvs)
        doc_ids = self._main_db.table(table).update(kvs_converted, query_stmt)
        return doc_ids

    def delete(self, stmt: str, table: str) -> List[int]:
        """Delete records on table of db.

        Args:
            stmt: str
            table: str

        Returns:
            List[int]

        """
        query_stmt = DbTinyDB._convert_query(stmt=stmt)
        doc_ids = self._main_db.table(table).remove(query_stmt)
        return doc_ids

    def get(self, stmt: str, table: str) -> Dict:
        """Get record on table of db.

        Args:
            stmt: str
            table: str

        Returns:
            Dict

        """
        query_stmt = DbTinyDB._convert_query(stmt=stmt)
        record = self._main_db.table(table).get(query_stmt)
        return record

    def query(self, stmt: str, table: str) -> List[Dict]:
        """Query records on table of db.

        Args:
            stmt: str
            table: str

        Returns:
            List[Dict]


        """
        query_stmt = DbTinyDB._convert_query(stmt=stmt)
        records = self._main_db.table(table).search(query_stmt)
        return records
