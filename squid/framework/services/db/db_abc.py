"""Definitions of type dbs."""
from abc import ABC, abstractmethod
from typing import Dict, List


class DbABC(ABC):
    """Abstract class to be implemented by dbs."""

    @abstractmethod
    def query(self, stmt: str, table: str) -> List[Dict]:
        """Abstract method to be implemented for Query records on Table.

        Args:
            stmt: str
            table: str

        """
        pass

    @abstractmethod
    def insert(
        self, record: Dict, table: str, upsert: bool = False, upsert_stmt: str = None
    ) -> int:
        """Abstract method to be implemented for Insert records into Table.

        Args:
            record: Dict
            table: str
            upsert: bool
            upsert_stmt: str = None

        """
        pass

    @abstractmethod
    def update(self, kvs: Dict, table: str, stmt: str) -> List[int]:
        """Abstract method to be implemented for Update records on Table.

        Args:
            kvs: Dict
            table: str
            stmt: str

        """
        pass

    @abstractmethod
    def delete(self, stmt: str, table: str) -> List[int]:
        """Abstract method to be implemented for Delete records on Table.

        Args:
            stmt: str
            table: str

        """
        pass

    @abstractmethod
    def get(self, stmt: str, table: str) -> Dict:
        """Abstract method to be implemented for Get record on Table.

        If multiple documents match the query, probably a random one of them will be returned!

        Args:
            stmt: str
            table: str

        """
        pass
