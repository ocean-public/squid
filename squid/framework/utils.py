"""Helpers functions."""
from copy import deepcopy
from enum import Enum
from functools import reduce
import json
from typing import Any, Dict, List, Tuple, Union

import yaml

from squid.framework.core.models.bases import (
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)


class SquidDumper(yaml.SafeDumper):
    """SquidDumper."""

    def represent_data(self, data: Any) -> Any:
        """Represent data."""
        if isinstance(data, Enum):
            return self.represent_data(data.value)
        return super().represent_data(data)


def descompose_yaml(text: str) -> Any:
    """Descompose yaml string to dicts."""
    for schema in yaml.safe_load_all(text):
        if type(schema) == list:
            for item in schema:
                yield item
        else:
            yield schema


def compose_yaml(obj: Dict, explicit_start: bool = True) -> str:
    """Compose yaml."""
    return yaml.dump(obj, indent=2, explicit_start=explicit_start, Dumper=SquidDumper)


def compose_json(obj: Dict) -> str:
    """Compose json."""
    return json.dumps(obj, indent=2, sort_keys=True, ensure_ascii=False)


def to_obj(input: Any, validate: bool = False) -> Any:
    """Returns obj if is valid else same input."""
    try:
        obj = json.loads(input)
        return True if validate else obj
    except json.decoder.JSONDecodeError:
        return False if validate else input


def to_str(input: Any) -> str:
    """Returns obj as string."""
    if type(input) is list:
        return "\n".join([str(x) for x in input])
    elif type(input) is dict:
        return compose_json(input)
    else:
        return str(input)


def flatten_deep_item_to_ref_links(
    items: List[Union[Task, Tentacle]],
    squid_items: List[Union[TaskRef, TentacleRef]] = None,
    squid_links: List[
        Tuple[List[Union[TaskRef, TentacleRef]], List[Union[TaskRef, TentacleRef]]]
    ] = None,
) -> Tuple[
    List[Union[TaskRef, TentacleRef]],
    List[Tuple[List[Union[TaskRef, TentacleRef]], List[Union[TaskRef, TentacleRef]]]],
]:
    """Flatten deep item tree on ref and links."""
    squid_items = squid_items or []
    squid_links = squid_links or []
    for item in items:
        if isinstance(item, Tentacle):
            tentacle_ref = TentacleRef(name=item.name, type=item.type, path=item.path)
            squid_items.append(tentacle_ref)
            if item.tentacles or item.tasks:
                squid_links.append(
                    (
                        [
                            *[
                                TentacleRef(
                                    name=tentacle.name,
                                    type=tentacle.type,
                                    path=tentacle.path,
                                )
                                for tentacle in item.tentacles
                            ],
                            *[
                                TaskRef(
                                    name=task.name,
                                    category=task.category,
                                    envs=task.envs,
                                    cmds=task.cmds,
                                )
                                for task in item.tasks
                            ],
                        ],
                        [tentacle_ref],
                    )
                )
            _ = flatten_deep_item_to_ref_links(item.tentacles, squid_items, squid_links)  # type: ignore
            _ = flatten_deep_item_to_ref_links(item.tasks, squid_items, squid_links)  # type: ignore

        elif isinstance(item, TentacleRef):
            squid_items.append(item)

        elif isinstance(item, Task):
            task_ref = TaskRef(
                name=item.name, category=item.category, envs=item.envs, cmds=item.cmds,
            )
            squid_items.append(task_ref)
            if item.tentacles or item.tasks:
                squid_links.append(
                    (
                        [
                            *[
                                TentacleRef(
                                    name=tentacle.name,
                                    type=tentacle.type,
                                    path=tentacle.path,
                                )
                                for tentacle in item.tentacles
                            ],
                            *[
                                TaskRef(
                                    name=task.name,
                                    category=task.category,
                                    envs=task.envs,
                                    cmds=task.cmds,
                                )
                                for task in item.tasks
                            ],
                        ],
                        [task_ref],
                    )
                )
            _ = flatten_deep_item_to_ref_links(item.tentacles, squid_items, squid_links)  # type: ignore
            _ = flatten_deep_item_to_ref_links(item.tasks, squid_items, squid_links)  # type: ignore

        elif isinstance(item, TaskRef):
            squid_items.append(item)

    return squid_items, squid_links


def deep_merge(*dicts: Any) -> dict:
    """ Return a merge dict."""  # noqa: D202,D210

    def merge_into(d1: Any, d2: Any) -> Any:
        """Return merge tuples of dict."""
        for key in d2:
            if key in d1:
                if isinstance(d1[key], dict):
                    d1[key] = merge_into(d1[key], d2[key])
                elif isinstance(d1[key], list):
                    if isinstance(d2[key], list):
                        d1[key] = [
                            item for item in d1[key] if item not in d2[key]
                        ] + d2[key]
                    else:
                        d1[key] = [item for item in d1[key] if item != d2[key]]
                        d1[key].append(d2[key])
                else:
                    d1[key] = deepcopy(d2[key])
            else:
                d1[key] = deepcopy(d2[key])
        return d1

    return reduce(merge_into, dicts, {})
