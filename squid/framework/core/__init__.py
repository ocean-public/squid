"""SquidProject."""
import os
from typing import Any, Callable, cast, Dict, List, Optional, Tuple, Union

from squid.framework.core.commands import Invoker, Receiver
from squid.framework.core.commands.add import AddPlugins, AddTasks, AddTentacles
from squid.framework.core.commands.dump import DumpPlugin, DumpTentacle
from squid.framework.core.commands.execute import Execute
from squid.framework.core.commands.get import (
    GetPlugin,
    GetPlugins,
    GetRegistryEntries,
    GetTask,
    GetTasks,
    GetTentacle,
    GetTentacles,
)
from squid.framework.core.commands.init import Init
from squid.framework.core.commands.invoke import InvokeCtx, InvokeFn
from squid.framework.core.commands.link import CreateLink, RemoveLink
from squid.framework.core.commands.remove import (
    RemovePlugins,
    RemoveTasks,
    RemoveTentacles,
)
from squid.framework.core.commands.update import (
    UpdatePlugins,
    UpdateTasks,
    UpdateTentacles,
)
from squid.framework.core.models.bases import (
    Link,
    LinkedCls,
    Plugin,
    SquidClass,
    SquidTable,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)
from squid.framework.core.models.responses import (
    DbResponseGet,
    DbResponseQuery,
    PersistenceResponsePath,
    StatusEnum,
)
from squid.framework.services.db import SquidDb
from squid.framework.services.db.db_tinydb import DbTinyDB
from squid.framework.services.engine import SquidEngine
from squid.framework.services.engine.engine_luigi import EngineLuigi
from squid.framework.services.persistence import SquidPersistence
from squid.framework.services.registry import SquidRegistry


def factory_deep_resolver(
    squid_db: SquidDb,
) -> Callable[[Union[TaskRef, TentacleRef]], Union[Task, Tentacle]]:
    """Factory deep resolver."""

    def _deep_resolver(item_ref: Union[TaskRef, TentacleRef]) -> Union[Task, Tentacle]:
        """Recursice resolver."""
        item_cls: str = "tentacle" if isinstance(item_ref, TentacleRef) else "task"
        link_item = cast(
            DbResponseGet,
            squid_db.get(
                stmt=f"($.name == '{item_ref.name}') & ($.cls == '{item_cls}')",
                table=SquidTable.LINKS,
            ).data,
        ).get
        item: Union[Tentacle, Task] = Tentacle.parse_obj(
            {**item_ref.dict(), **{"tentacles": [], "tasks": []}}
        ) if isinstance(item_ref, TentacleRef) else Task.parse_obj(
            {**item_ref.dict(), **{"tentacles": [], "tasks": []}}
        )
        if link_item:
            item.tentacles = [
                cast(
                    Tentacle,
                    _deep_resolver(
                        item_ref=cast(
                            TentacleRef,
                            cast(
                                DbResponseGet,
                                squid_db.get(
                                    stmt=f"$.name == '{dependency.name}'",
                                    table=SquidTable.TENTACLES,
                                ).data,
                            ).get,
                        )
                    ),
                )
                for dependency in cast(Link, link_item).dependencies
                if dependency.cls == "tentacle"
            ]
            item.tasks = [
                cast(
                    Task,
                    _deep_resolver(
                        item_ref=cast(
                            TaskRef,
                            cast(
                                DbResponseGet,
                                squid_db.get(
                                    stmt=f"$.name == '{dependency.name}'",
                                    table=SquidTable.TASKS,
                                ).data,
                            ).get,
                        )
                    ),
                )
                for dependency in cast(Link, link_item).dependencies
                if dependency.cls == "task"
            ]
        return item

    return _deep_resolver


def populate_system_env(tentacles: List[Tentacle], squid_db: SquidDb) -> List[Tentacle]:
    """Populate env for task with system vars."""
    root_path = cast(
        PersistenceResponsePath, SquidPersistence().get_root_path().data
    ).path
    squid_path = cast(
        PersistenceResponsePath, SquidPersistence().get_squid_path().data
    ).path
    system_envs = {
        **{"SQUID_PATH": squid_path, "ROOT_PATH": root_path},
        **{
            f"TENTACLE_{tentacle.name.upper()}_PATH": os.path.abspath(
                os.path.join(root_path, cast(TentacleRef, tentacle).path)
            )
            for tentacle in cast(
                DbResponseQuery,
                squid_db.query(
                    stmt="$.name.matches('.*')", table=SquidTable.TENTACLES
                ).data,
            ).query
        },
    }
    merge_envs: Callable[
        [List[Task], Dict[str, str]], List[Task]
    ] = lambda items, local_envs: [
        Task.parse_obj(
            {
                **item.dict(),
                **{"envs": {**item.envs, **system_envs, **local_envs}},
                **{
                    "tasks": [
                        task.dict() for task in merge_envs(item.tasks, local_envs)
                    ]
                },
            }
        )
        for item in items
    ]

    walk_tentacles: Callable[[List[Tentacle]], List[Tentacle]] = lambda items: [
        Tentacle.parse_obj(
            {
                **item.dict(),
                **{
                    "tasks": [
                        task.dict()
                        for task in merge_envs(
                            item.tasks,
                            {
                                "TENTACLE_NAME": item.name,
                                "TENTACLE_PATH": os.path.abspath(
                                    os.path.join(root_path, item.path)
                                ),
                                "TENTACLE_TYPE": item.type,
                            },
                        )
                    ],
                    "tentacles": [
                        tentacle.dict() for tentacle in walk_tentacles(item.tentacles)
                    ],
                },
            }
        )
        for item in items
    ]
    return walk_tentacles(tentacles)


class Squid(Receiver):
    """Receiver implementation."""

    def init(self, path: str) -> bool:
        """Init Squid Project."""
        return True

    def terminate(self) -> bool:
        """Terminate Squid Project."""
        return True

    def add(
        self, squid_items: List[Union[Plugin, TaskRef, TentacleRef]], upsert: bool
    ) -> bool:
        """Add Squid items.

        Args:
            squid_items: List[Union[Plugin, TaskRef, TentacleRef]]
            upsert: bool

        Returns:
            bool

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)
        results: List[bool] = [
            squid_db.insert(record=squid_item, upsert=upsert).status
            == StatusEnum.success
            for squid_item in squid_items
        ]
        return all(results)

    def remove(self, squid_items: List[Tuple[str, SquidTable]]) -> bool:
        """Remove Squid items.

        Args:
            squid_items: List[str, SquidTable]]

        Returns:
            bool

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)
        resp = [
            squid_db.delete(stmt=squid_item[0], table=squid_item[1]).status
            == StatusEnum.success
            for squid_item in squid_items
        ]
        return all(resp)

    def get(
        self, name: str, table: SquidTable, deep: bool = False
    ) -> Union[TaskRef, TentacleRef, Plugin, Dict, Task, Tentacle]:
        """Get Squid item.

        Args:
            name: str
            table: SquidTable
            deep: bool = False

        Returns:
            Union[TaskRef, TentacleRef, Plugin, Dict, Task, Tentacle]

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)
        item_ref = cast(
            DbResponseGet, squid_db.get(stmt=f"$.name == '{name}'", table=table).data
        ).get
        if deep and type(item_ref) in [TaskRef, TentacleRef]:
            return factory_deep_resolver(squid_db)(item_ref)  # type: ignore
        return item_ref  # type: ignore

    def query(
        self, stmt: str, table: SquidTable, output: str = "list", query_link: str = ""
    ) -> List[Union[TaskRef, TentacleRef, Plugin, Task, Tentacle]]:
        """Query Squid items.

        Returns:
            List[Union[TaskRef, TentacleRef, Plugin, Task, Tentacle]]

        Args:
            stmt: str
            table: SquidTable
            output: str
            query_link: str

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)
        item_refs = cast(
            DbResponseQuery, squid_db.query(stmt=stmt, table=table).data
        ).query

        if table not in [SquidTable.TASKS, SquidTable.TENTACLES]:
            return item_refs  # type: ignore

        if query_link:
            item_cls = "tentacle" if table == SquidTable.TENTACLES else "task"
            item_filter = [
                item_link.name
                for item_link in cast(
                    DbResponseQuery,
                    squid_db.query(stmt=query_link, table=SquidTable.LINKS).data,
                ).query
                if cast(Link, item_link).cls == item_cls
            ]
        else:
            item_filter = [item_ref.name for item_ref in item_refs]

        if output == "list":
            return [item_ref for item_ref in item_refs if item_ref.name in item_filter]  # type: ignore
        elif output == "tree":
            return [
                factory_deep_resolver(squid_db)(item_ref)  # type: ignore
                for item_ref in item_refs
                if item_ref.name in item_filter
            ]
        return []

    def update(self, stmt: str, table: SquidTable, kvs: Dict) -> bool:
        """Update Squid items.

        Args:
            stmt: str
            table: SquidTable
            kvs: Dict

        Returns:
            bool

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)
        return (
            squid_db.update(stmt=stmt, table=table, kvs=kvs).status
            == StatusEnum.success
        )

    def link(
        self,
        sources: List[Union[TaskRef, TentacleRef]],
        targets: List[Union[TaskRef, TentacleRef]],
    ) -> bool:
        """Link Squid items.

        Args:
            sources: List[Union[TaskRef, TentacleRef]]
            targets: List[Union[TaskRef, TentacleRef]]

        Returns:
            bool

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)

        fn_get_cls: Callable[
            [Union[TaskRef, TentacleRef]], str
        ] = lambda item: "tentacle" if isinstance(item, TentacleRef) else "task"
        fn_get_link: Callable[[Union[TaskRef, TentacleRef]], Link] = lambda item: cast(
            Link,
            cast(
                DbResponseGet,
                squid_db.get(
                    stmt=f"($.name == '{item.name}') & ($.cls == '{fn_get_cls(item)}')",
                    table=SquidTable.LINKS,
                ).data,
            ).get,
        ) or Link(
            name=item.name,
            cls=SquidClass.TENTACLE
            if isinstance(item, TentacleRef)
            else SquidClass.TASK,
            dependencies=[],
            dependant=[],
            attributes=[],
        )

        fn_check_exists: Callable[
            [List[LinkedCls], str], bool
        ] = lambda items, name: bool([item for item in items if item.name == name])

        results: List[bool] = []
        for target in sources:
            target_link = fn_get_link(target)
            for source in targets:
                if source.name == target.name and type(source) == type(target):
                    continue

                source_link = fn_get_link(source)
                if not fn_check_exists(
                    source_link.dependencies, target.name
                ) and not fn_check_exists(source_link.dependant, target.name):
                    source_link.dependencies.append(
                        LinkedCls(
                            name=target.name,
                            cls=SquidClass.TENTACLE
                            if isinstance(target, TentacleRef)
                            else SquidClass.TASK,
                        )
                    )
                    results.append(
                        squid_db.insert(record=source_link, upsert=True).status
                        == StatusEnum.success
                    )

                if not fn_check_exists(
                    target_link.dependant, source.name
                ) and not fn_check_exists(target_link.dependencies, source.name):
                    target_link.dependant.append(
                        LinkedCls(
                            name=source.name,
                            cls=SquidClass.TENTACLE
                            if isinstance(source, TentacleRef)
                            else SquidClass.TASK,
                        )
                    )
            results.append(
                squid_db.insert(record=target_link, upsert=True).status
                == StatusEnum.success
            )

        return all(results)

    def unlink(
        self,
        sources: List[Union[TaskRef, TentacleRef]],
        targets: List[Union[TaskRef, TentacleRef]],
    ) -> bool:
        """Link Squid items.

        Args:
            sources: List[Union[TaskRef, TentacleRef]]
            targets: List[Union[TaskRef, TentacleRef]]

        Returns:
            bool

        """
        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)

        fn_get_cls: Callable[
            [Union[TaskRef, TentacleRef]], str
        ] = lambda item: "tentacle" if isinstance(item, TentacleRef) else "task"

        fn_get_link: Callable[[Union[TaskRef, TentacleRef]], Link] = lambda item: cast(
            Link,
            cast(
                DbResponseGet,
                squid_db.get(
                    stmt=f"($.name == '{item.name}') & ($.cls == '{fn_get_cls(item)}')",
                    table=SquidTable.LINKS,
                ).data,
            ).get,
        )

        fn_get_idx: Callable[[List[LinkedCls], str], List[int]] = lambda items, name: [
            idx for idx, item in enumerate(items) if item.name == name
        ] or [-1]

        results: List[bool] = []
        for target in sources:
            target_link = fn_get_link(target)
            for source in targets:
                if source.name == target.name and type(source) == type(target):
                    continue

                source_link = fn_get_link(source)
                source_idx = fn_get_idx(source_link.dependencies, target.name)[0]
                if source_idx >= 0:
                    source_link.dependencies.pop(source_idx)
                    results.append(
                        squid_db.insert(record=source_link, upsert=True).status
                        == StatusEnum.success
                    )

                target_idx = fn_get_idx(target_link.dependant, source.name)[0]
                if target_idx >= 0:
                    target_link.dependant.pop(target_idx)
            results.append(
                squid_db.insert(record=target_link, upsert=True).status
                == StatusEnum.success
            )

        return all(results)

    def execute(self, tentacles: List[Tentacle]) -> Dict:
        """Get Execute item.

        Args:
            tentacles: List[Tentacle]

        Returns:
            Dict

        """
        engine_luigi = EngineLuigi()
        squid_engine = SquidEngine(engine=engine_luigi)

        db_tinydb = DbTinyDB()
        squid_db = SquidDb(db=db_tinydb)

        tentacles_parsed = populate_system_env(tentacles, squid_db)
        return squid_engine.run(tentacles_parsed).data.dict()

    def registry_entries(self, plugins: List[Plugin]) -> List[str]:
        """Get Registry Entries.

        Args:
            plugins: List[Plugin]

        Returns:
            List[str]

        """
        squid_registry = SquidRegistry()
        # _ = [
        #     plugin_source.cleanup() for plugin_source in squid_registry._plugin_sources
        # ]  # Patch for multiple invokes
        squid_registry.discovery(SquidPersistence().set_abspath(items=plugins))
        return list(squid_registry.entries.keys())

    def invoke_fn(self, entry: str, plugins: List[Plugin]) -> Optional[Callable]:
        """Invoke function by entry.

        Args:
            entry: str
            plugins: List[Plugin]

        Returns:
            Optional[Callable]

        """
        squid_registry = SquidRegistry()
        # _ = [
        #     plugin_source.cleanup() for plugin_source in squid_registry._plugin_sources
        # ]  # Patch for multiple invokes
        squid_registry.discovery(plugins)
        return squid_registry.entries.get(entry)

    def invoke_ctx(self, plugin_name: str, plugins: List[Plugin]) -> Optional[Any]:
        """Invoke context of entry.

        Args:
            plugin_name: str
            plugins: List[Plugin]

        Returns:
            Optional[Callable]

        """
        squid_registry = SquidRegistry()
        # _ = [
        #     plugin_source.cleanup() for plugin_source in squid_registry._plugin_sources
        # ]  # Patch for multiple invokes
        squid_registry.discovery(plugins)
        return squid_registry.context(plugin_name)


class Head(Invoker):
    """Invoker implementation."""

    def __init__(self) -> None:
        """Constructor."""
        super().__init__(
            [
                Execute,
                DumpPlugin,
                DumpTentacle,
                CreateLink,
                Init,
                InvokeCtx,
                InvokeFn,
                AddPlugins,
                AddTasks,
                AddTentacles,
                GetPlugin,
                GetPlugins,
                GetRegistryEntries,
                GetTask,
                GetTasks,
                GetTentacle,
                GetTentacles,
                RemoveLink,
                RemovePlugins,
                RemoveTasks,
                RemoveTentacles,
                UpdatePlugins,
                UpdateTasks,
                UpdateTentacles,
            ]
        )
