"""Command Link Tentacle|Tasks|Plugins."""
from typing import Any, List, Union

from squid.framework.core.commands import Command
from squid.framework.core.models.bases import (
    TaskRef,
    TentacleRef,
)


class CreateLink(Command):
    """Command CreateLink."""

    __slots__ = ("sources", "targets", "settings")

    def __init__(
        self,
        receiver: Any,
        sources: List[Union[TaskRef, TentacleRef]],
        targets: List[Union[TaskRef, TentacleRef]],
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            sources: List[Union[TaskRef, TentacleRef]]
            targets: List[Union[TaskRef, TentacleRef]]

        """
        super().__init__(receiver)
        self.sources = sources
        self.targets = targets

    def execute(self) -> bool:
        """Execute Link.

        Returns:
            bool

        """
        return self._receiver.action("link", self.sources, self.targets)

    def unexecute(self) -> bool:
        """Undo Execute Link.

        Returns:
            bool

        """
        return self._receiver.action("unlink", self.sources, self.targets)


class RemoveLink(Command):
    """Command RemoveLink."""

    __slots__ = ("sources", "targets", "_originals")

    def __init__(
        self,
        receiver: Any,
        sources: List[Union[TaskRef, TentacleRef]],
        targets: List[Union[TaskRef, TentacleRef]],
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            sources: List[Union[TaskRef, TentacleRef]]
            targets: List[Union[TaskRef, TentacleRef]]

        """
        super().__init__(receiver)
        self.sources = sources
        self.targets = targets

    def execute(self) -> bool:
        """Execute RemoveLink.

        Returns:
            bool

        """
        return self._receiver.action("unlink", self.sources, self.targets)

    def unexecute(self) -> bool:
        """Undo Execute RemoveLink.

        Returns:
            bool

        """
        return self._receiver.action("link", self.sources, self.targets)
