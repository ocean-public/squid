"""Command Execute Tasks on Tentacles."""
from typing import Any, Callable, Dict, List

from squid.framework.core.commands import Command
from squid.framework.core.models.bases import (
    Task,
    Tentacle,
)


class Execute(Command):
    """Command Execute."""

    __slots__ = ("tentacles", "tasks")

    def __init__(
        self, receiver: Any, tentacles: List[Tentacle], tasks: List[Task] = None
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            tentacles: List[Tentacle]
            tasks: List[Task] = None

        """
        super().__init__(receiver)
        self.tentacles = tentacles
        self.tasks = tasks or []

    def execute(self) -> Dict:
        """Execute Execute.

        Returns:
            Dict

        """
        tasks = [task.dict() for task in self.tasks]
        fn_replace_tasks: Callable[
            [List[Tentacle]], List[Tentacle]
        ] = lambda tentacles: [
            Tentacle.parse_obj(
                {
                    **tentacle.dict(),
                    **{
                        "tasks": tasks,
                        "tentacles": [
                            tentacle_tmp.dict()
                            for tentacle_tmp in fn_replace_tasks(tentacle.tentacles)
                        ],
                    },
                }
            )
            for tentacle in tentacles
        ]
        return self._receiver.action(
            "execute", fn_replace_tasks(self.tentacles) if tasks else self.tentacles
        )

    def unexecute(self) -> bool:
        """Undo Execute Get Plugin.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")
