"""Command Invoke registry entries or context."""
from typing import Any, Callable, List

from squid.framework.core.commands import Command
from squid.framework.core.commands.get import GetPlugins
from squid.framework.core.models.bases import Plugin


class InvokeFn(Command):
    """Command Invoke Function of entry."""

    __slots__ = ("entry", "plugins")

    def __init__(self, receiver: Any, entry: str, plugins: List[Plugin] = None) -> None:
        """Constructor.

        Args:
            receiver: Any
            entry: str
            plugins: List[Plugin]

        """
        super().__init__(receiver)
        self.entry = entry
        self.plugins = (
            plugins or GetPlugins(receiver, "$.name.matches('.*')", "tree").execute()
        )

    def execute(self) -> Callable:
        """Execute Invoke Function of entry.

        Returns:
            Callable

        Raises:
            Exception: Entry <entry> is not registered.

        """
        fn = self._receiver.action("invoke_fn", self.entry, self.plugins)
        if fn is None:
            raise Exception(f"Entry {self.entry} is not registered.")
        return fn

    def unexecute(self) -> bool:
        """Undo Execute Invoke Function of entry.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class InvokeCtx(Command):
    """Command Invoke context of plugin."""

    __slots__ = ("plugin_name", "plugins")

    def __init__(
        self,
        receiver: Any,
        plugin_name: str = "squid_plugins",
        plugins: List[Plugin] = None,
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            plugin_name: str
            plugins: List[Plugin]

        """
        super().__init__(receiver)
        self.plugin_name = plugin_name
        self.plugins = (
            plugins
            or GetPlugins(receiver, f"$.name == '{plugin_name}'", "tree").execute()
        )

    def execute(self) -> Any:
        """Execute Invoke context of plugin.

        Returns:
            Callable

        """
        return self._receiver.action("invoke_ctx", self.plugin_name, self.plugins)

    def unexecute(self) -> bool:
        """Undo Execute Invoke context of plugin.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")
