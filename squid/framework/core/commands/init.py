"""Command Init Squid."""
import os
from pathlib import Path
import shutil
from typing import Any, cast

from squid.framework.core.commands import Command
from squid.framework.services.persistence import (
    PersistenceResponsePath,
    SquidPersistence,
)


class Init(Command):
    """Command Init."""

    __slots__ = ("path",)

    def __init__(self, receiver: Any, path: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            path: str

        Raises:
            NotADirectoryError: Path: <path> is not a directory or it does not exists.

        """
        if not os.path.exists(path):
            raise NotADirectoryError(
                f"Path: {path} is not a directory or it does not exists."
            )
        super().__init__(receiver)
        self.path = path

    def execute(self) -> bool:
        """Execute Init.

        Returns:
            bool

        """
        try:
            _ = cast(
                PersistenceResponsePath, SquidPersistence().get_squid_path().data
            ).path
        except Exception as _:  # noqa: F841
            squid_path = Path(self.path).joinpath(".squid")
            squid_path.mkdir()
            squid_path.joinpath("luigi.toml").touch()
            squid_logs = squid_path.joinpath("logs")
            squid_logs.mkdir()
            squid_logs.joinpath(".gitkeep").touch()
            squid_plugins = squid_path.joinpath("plugins")
            squid_plugins.mkdir()
            squid_plugins.joinpath(".gitkeep").touch()
            squid_plugins_built_in = squid_plugins.joinpath("built-in")
            squid_plugins_built_in.mkdir()
            squid_plugins_built_in.joinpath(".gitkeep").touch()
            return self._receiver.action("init", self.path)
        else:
            raise Exception("There is already a Squid project initialize.")

    def unexecute(self) -> bool:
        """Undo Execute Init.

        Returns:
            bool

        Raises:
            Exception: Squid project is not inittialize.

        """
        try:
            shutil.rmtree(
                cast(
                    PersistenceResponsePath, SquidPersistence().get_squid_path().data
                ).path
            )
            return self._receiver.action("terminate")
        except Exception as e:  # noqa: F841
            raise Exception("Squid project is not inittialize.")
