"""Command Remove Tentacle|Tasks|Plugins."""
from typing import Any, cast

from squid.framework.core.commands import Command
from squid.framework.core.commands.add import AddPlugins, AddTasks, AddTentacles
from squid.framework.core.commands.get import GetPlugins, GetTasks, GetTentacles
from squid.framework.core.models.bases import (
    SquidTable,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)
from squid.framework.services.persistence import SquidPersistence


class RemoveTentacles(Command):
    """Command Remove Tentacles."""

    __slots__ = ("query", "_originals")

    def __init__(self, receiver: Any, query: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str

        """
        super().__init__(receiver)
        self.query = query
        self._originals = GetTentacles(
            receiver=receiver, query=query, output="tree"
        ).execute()

    def execute(self) -> bool:
        """Execute Remove Tentacles.

        Returns:
            bool

        """
        for tentacle_to_remove in self._originals:
            SquidPersistence().remove_tentacle_path(tentacle_to_remove)
            self._receiver.action(
                "unlink",
                [
                    TentacleRef(
                        name=tentacle_to_ref.name,
                        type=tentacle_to_ref.type,
                        path=tentacle_to_ref.path,
                    )
                    for tentacle_to_ref in cast(Tentacle, tentacle_to_remove).tentacles
                ],
                [
                    TentacleRef(
                        name=tentacle_to_remove.name,
                        type=tentacle_to_remove.type,
                        path=tentacle_to_remove.path,
                    )
                ],
            )
            self._receiver.action(
                "unlink",
                [
                    TaskRef(
                        name=task_to_ref.name,
                        category=task_to_ref.category,
                        envs=task_to_ref.envs,
                        cmds=task_to_ref.cmds,
                    )
                    for task_to_ref in cast(Tentacle, tentacle_to_remove).tasks
                ],
                [
                    TentacleRef(
                        name=tentacle_to_remove.name,
                        type=tentacle_to_remove.type,
                        path=tentacle_to_remove.path,
                    )
                ],
            )

            tentacle_to_remove_links = self._receiver.action(
                "query", f"$.name == '{tentacle_to_remove.name}'", SquidTable.LINKS
            )
            if tentacle_to_remove_links:
                self._receiver.action(
                    "unlink",
                    [
                        TentacleRef(
                            name=tentacle_to_remove.name,
                            type=tentacle_to_remove.type,
                            path=tentacle_to_remove.path,
                        )
                    ],
                    [
                        self._receiver.action(
                            "get", dependant.name, SquidTable.TENTACLES
                        )
                        if dependant.cls == "tentacle"
                        else self._receiver.action(
                            "get", dependant.name, SquidTable.TASKS
                        )
                        for dependant in tentacle_to_remove_links[0].dependant
                    ],
                )
        return self._receiver.action("remove", [(self.query, SquidTable.TENTACLES)])

    def unexecute(self) -> bool:
        """Undo Execute Remove Tentacles.

        Returns:
            bool

        """
        return AddTentacles(
            receiver=self._receiver, tentacles=self._originals, upsert=True
        ).execute()


class RemoveTasks(Command):
    """Command Remove Tasks."""

    __slots__ = ("query", "_originals")

    def __init__(self, receiver: Any, query: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str

        """
        super().__init__(receiver)
        self.query = query
        self._originals = GetTasks(
            receiver=receiver, query=query, output="tree"
        ).execute()

    def execute(self) -> bool:
        """Execute Remove Tasks.

        Returns:
            bool

        """
        for task_to_remove in self._originals:
            self._receiver.action(
                "unlink",
                [
                    TaskRef(
                        name=task_to_ref.name,
                        category=task_to_ref.category,
                        envs=task_to_ref.envs,
                        cmds=task_to_ref.cmds,
                    )
                    for task_to_ref in cast(Task, task_to_remove).tasks
                ],
                [
                    TaskRef(
                        name=task_to_remove.name,
                        category=task_to_remove.category,
                        envs=task_to_remove.envs,
                        cmds=task_to_remove.cmds,
                    )
                ],
            )
            self._receiver.action(
                "unlink",
                [
                    TentacleRef(
                        name=tentacle_to_ref.name,
                        type=tentacle_to_ref.type,
                        path=tentacle_to_ref.path,
                    )
                    for tentacle_to_ref in cast(Task, task_to_remove).tentacles
                ],
                [
                    TaskRef(
                        name=task_to_remove.name,
                        category=task_to_remove.category,
                        envs=task_to_remove.envs,
                        cmds=task_to_remove.cmds,
                    )
                ],
            )
        return self._receiver.action("remove", [(self.query, SquidTable.TASKS)])

    def unexecute(self) -> bool:
        """Undo Execute Add Tentacles.

        Returns:
            bool

        """
        return AddTasks(
            receiver=self._receiver, tasks=self._originals, upsert=True
        ).execute()


class RemovePlugins(Command):
    """Command Remove Plugins."""

    __slots__ = ("query", "_originals")

    def __init__(self, receiver: Any, query: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str

        """
        super().__init__(receiver)
        self.query = query
        self._originals = GetPlugins(receiver=receiver, query=query).execute()

    def execute(self) -> bool:
        """Execute Remove Plugins.

        Returns:
            bool

        """
        for plugins_to_remove in self._originals:
            SquidPersistence().remove_squid_plugin_path(plugins_to_remove)
        return self._receiver.action("remove", [(self.query, SquidTable.PLUGINS)])

    def unexecute(self) -> bool:
        """Undo Execute Add Plugins.

        Returns:
            bool

        """
        return AddPlugins(
            receiver=self._receiver, plugins=self._originals, upsert=True
        ).execute()
