"""Command Get Tentacle|Tasks|Plugins."""
from typing import Any, Dict, List, Union

from squid.framework.core.commands import Command
from squid.framework.core.models.bases import (
    Plugin,
    SquidTable,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)


class GetTentacle(Command):
    """Command Get Tentacle."""

    __slots__ = ("name", "deep")

    def __init__(self, receiver: Any, name: str, deep: bool = False) -> None:
        """Constructor.

        Args:
            receiver: Any
            name: str
            deep: bool = False

        """
        super().__init__(receiver)
        self.name = name
        self.deep = deep

    def execute(self) -> Union[Tentacle, TentacleRef, Dict]:
        """Execute Get Tentacle.

        Returns:
            Union[Tentacle, TentacleRef, Dict]

        """
        tentacle = self._receiver.action(
            "get", self.name, SquidTable.TENTACLES, self.deep
        )
        return tentacle

    def unexecute(self) -> None:
        """Undo Execute Get Tentacle.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class GetTentacles(Command):
    """Command Gets Tentacles."""

    __slots__ = ("query", "output", "query_link")

    def __init__(
        self, receiver: Any, query: str, output: str = "list", query_link: str = None
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str
            output: str
            query_link: str

        Raises:
            Exception: Output choice is not supported <output>. # noqa: DAR401

        """
        super().__init__(receiver)
        self.query = query
        if output not in ["list", "tree"]:
            raise Exception(f"Output choice is not supported {output}.")
        self.output = output
        self.query_link = query_link or ""

    def execute(self) -> List[Union[Tentacle, TentacleRef]]:
        """Execute Gets Tentacles.

        Returns:
            List[Union[Tentacle, TentacleRef]]

        """
        tentacles = self._receiver.action(
            "query", self.query, SquidTable.TENTACLES, self.output, self.query_link
        )
        return tentacles

    def unexecute(self) -> None:
        """Undo Execute Gets Tentacles.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class GetTask(Command):
    """Command Get Task."""

    __slots__ = ("name", "deep")

    def __init__(self, receiver: Any, name: str, deep: bool = False) -> None:
        """Constructor.

        Args:
            receiver: Any
            name: str
            deep: bool = False

        """
        super().__init__(receiver)
        self.name = name
        self.deep = deep

    def execute(self) -> Union[Task, TaskRef, Dict]:
        """Execute Get Task.

        Returns:
            Union[Task, TaskRef, Dict]

        """
        return self._receiver.action("get", self.name, SquidTable.TASKS, self.deep)

    def unexecute(self) -> None:
        """Undo Execute Get Task.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class GetTasks(Command):
    """Command Gets Tasks."""

    __slots__ = ("query", "output", "query_link")

    def __init__(
        self, receiver: Any, query: str, output: str = "list", query_link: str = None
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str
            output: str
            query_link: str

        Raises:
            Exception: Output choice is not supported <output>. # noqa: DAR401

        """
        super().__init__(receiver)
        self.query = query
        if output not in ["list", "tree"]:
            raise Exception(f"Output choice is not supported {output}.")
        self.output = output
        self.query_link = query_link or ""

    def execute(self) -> List[Union[Task, TaskRef]]:
        """Execute Gets Tasks.

        Returns:
            List[Union[Task, TaskRef]]

        """
        tasks = self._receiver.action(
            "query", self.query, SquidTable.TASKS, self.output, self.query_link
        )
        return tasks

    def unexecute(self) -> None:
        """Undo Execute Gets Tasks.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class GetPlugin(Command):
    """Command Get Plugin."""

    __slots__ = ("name",)

    def __init__(self, receiver: Any, name: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            name: str

        """
        super().__init__(receiver)
        self.name = name

    def execute(self) -> Union[Plugin, Dict]:
        """Execute Get Plugin.

        Returns:
            Union[Plugin, Dict]

        """
        plugin = self._receiver.action("get", self.name, SquidTable.PLUGINS, False)
        return plugin

    def unexecute(self) -> None:
        """Undo Execute Get Plugin.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class GetPlugins(Command):
    """Command Gets Plugins."""

    __slots__ = ("query", "output", "format")

    def __init__(self, receiver: Any, query: str, output: str = "list") -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str
            output: str = "list"

        Raises:
            Exception: Output choice is not supported <output>. # noqa: DAR401

        """
        super().__init__(receiver)
        self.query = query
        if output not in ["list", "tree"]:
            raise Exception(f"Output choice is not supported {output}.")
        self.output = output

    def execute(self) -> List[Plugin]:
        """Execute Gets Plugins.

        Returns:
            List[Plugin]

        """
        plugins = self._receiver.action(
            "query", self.query, SquidTable.PLUGINS, self.output
        )
        return plugins

    def unexecute(self) -> None:
        """Undo Execute Gets Plugins.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")


class GetRegistryEntries(Command):
    """Command Gets Entries from registry."""

    __slots__ = "_plugins"

    def __init__(self, receiver: Any, plugins: List[Plugin] = None) -> None:
        """Constructor.

        Args:
            receiver: Any
            plugins: List[Plugin]

        """
        super().__init__(receiver)
        self.plugins = (
            plugins or GetPlugins(receiver, "$.name.matches('.*')", "tree").execute()
        )

    def execute(self) -> List[str]:
        """Execute Gets Entries.

        Returns:
            List[str]

        """
        return self._receiver.action("registry_entries", self.plugins)

    def unexecute(self) -> None:
        """Undo Execute Gets Entries.

        Raises:
            Exception: Operation not supported.

        """
        raise Exception("Operation not supported.")
