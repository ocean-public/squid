"""Model for tentacle object."""
from __future__ import annotations

from enum import Enum
from typing import Any, Dict, List

from pydantic import BaseModel, constr


class TentacleRef(BaseModel):
    """TentacleRef Model."""

    name: constr(min_length=2)  # type: ignore
    type: constr(min_length=2)  # type: ignore
    path: constr(min_length=1)  # type: ignore


class Tentacle(TentacleRef):
    """Tentacle Model."""

    attributes: List[Dict[str, Any]] = []
    tentacles: List[Tentacle] = []
    tasks: List[Task] = []


class TaskCmd(BaseModel):
    """TaskCmd Model."""

    cmd: str  # type: ignore
    args: List[str]


class TaskRef(BaseModel):
    """TaskRef Model."""

    name: constr(min_length=2)  # type: ignore
    category: constr(min_length=2)  # type: ignore
    envs: Dict
    cmds: List[TaskCmd]


class Task(TaskRef):
    """Task Model."""

    tentacles: List[Tentacle] = []
    tasks: List[Task] = []


class TaskDAG(BaseModel):
    """TaskDAG Model."""

    name: str
    cmds: List[TaskCmd]
    logpath: str
    wd: str
    exec_order: int = 0
    envs: Dict = {}
    tasks: List[TaskDAG] = []


class PluginCls(str, Enum):
    """PluginCls Model."""

    CLI = "cli"
    FUNCTION = "function"


class Plugin(BaseModel):
    """Plugin Model."""

    name: constr(min_length=2)  # type: ignore
    source: constr(min_length=1)  # type: ignore
    entrypoint: constr(min_length=2)  # type: ignore
    cls: PluginCls = PluginCls.CLI
    attributes: List[Dict[constr(min_length=2), Any]] = []  # type: ignore
    path: constr(min_length=1) = ".squid/plugins"  # type: ignore


class SquidClass(str, Enum):
    """SquidClass Model."""

    TENTACLE = "tentacle"
    TASK = "task"


class LinkedCls(BaseModel):
    """LinkedCls Model."""

    name: str
    cls: SquidClass


class Link(BaseModel):
    """Link Model."""

    name: str
    cls: SquidClass
    dependencies: List[LinkedCls]
    dependant: List[LinkedCls]
    attributes: List[Dict[str, Any]] = []


class SquidTable(str, Enum):
    """SquidTable Model."""

    TENTACLES = "tentacles"
    LINKS = "links"
    TASKS = "tasks"
    PLUGINS = "plugins"


class SquidItem(str, Enum):
    """SquidItem Model."""

    TENTACLE = "tentacle"
    TASK = "task"
    PLUGIN = "plugin"


Tentacle.update_forward_refs()
Task.update_forward_refs()
TaskDAG.update_forward_refs()
