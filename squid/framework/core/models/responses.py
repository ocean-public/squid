"""Models for responses object."""
from enum import Enum
from typing import Any, Dict, List, Union

from pydantic import BaseModel

from squid.framework.core.models.bases import (
    Link,
    Plugin,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)


class StatusEnum(str, Enum):
    """StatusEnum Model."""

    success = "success"
    failed = "failed"
    error = "error"


class EngineResponse(BaseModel):
    """EngineResponse Model."""

    summary: str
    exec_tree: List[str]


class DbResponseInsert(BaseModel):
    """DbResponseInsert Model."""

    insert: int


class DbResponseUpdate(BaseModel):
    """DbResponseUpdate Model."""

    update: List[int]


class DbResponseDelete(BaseModel):
    """DbResponseDelete Model."""

    delete: List[int]


class DbResponseGet(BaseModel):
    """DbResponseGet Model."""

    get: Union[TaskRef, TentacleRef, Plugin, Link, Dict, Task, Tentacle]


class DbResponseQuery(BaseModel):
    """DbResponseQuery Model."""

    query: List[Union[TaskRef, TentacleRef, Plugin, Link, Task, Tentacle]]


class PersistenceResponsePath(BaseModel):
    """PersistenceResponse Model."""

    path: str


class RegistryResponse(BaseModel):
    """RegistryResponse Model."""

    entries: Any


class ServiceResponse(BaseModel):
    """EngineResponse Model."""

    status: StatusEnum
    data: Union[
        EngineResponse,
        DbResponseGet,
        DbResponseQuery,
        DbResponseUpdate,
        DbResponseInsert,
        DbResponseDelete,
        PersistenceResponsePath,
        RegistryResponse,
    ]
