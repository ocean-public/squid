import json
from squid.framework.core import (
    AddPlugins,
    AddTasks,
    AddTentacles,
    GetTasks,
    GetTentacles,
    GetPlugins,
    Head,
    Init,
    Squid,
    CreateLink,
)
from squid.framework.core.models.bases import (
    TentacleRef,
    Tentacle,
    Task,
    TaskCmd,
    Plugin,
)


def run():
    squid = Squid()
    squid_head = Head()
    try:
        squid_head.execute(Init(squid, path="."))
        print("Squid is initialize.")
    except:
        print("Squid is already initialize.")

    tentacle_ref_a = TentacleRef(name="tentacle_a", type="test", path="./tentacle_a")
    tentacle_ref_b = TentacleRef(name="tentacle_b", type="test", path="./tentacle_b")
    tentacle_c = Tentacle.parse_obj(
        {
            "attributes": [],
            "name": "tentacle_c",
            "path": "./tentacle_c",
            "tasks": [
                {
                    "category": "echo",
                    "cmds": [{"args": ["test.py"], "cmd": "python"}],
                    "envs": {"TEST": "hola"},
                    "name": "task_b",
                    "tasks": [],
                    "tentacles": [],
                }
            ],
            "tentacles": [
                {
                    "attributes": [],
                    "name": "tentacle_a",
                    "path": "./tentacle_a",
                    "tasks": [],
                    "tentacles": [],
                    "type": "test",
                }
            ],
            "type": "test",
        }
    )

    # flag_add_tentacles = squid_head.execute(AddTentacles(squid, [tentacle_ref_a, tentacle_ref_b, tentacle_ref_c], True))
    flag_add_tentacles = squid_head.execute(
        AddTentacles(squid, [tentacle_c, tentacle_ref_b], True)
    )
    print(f"flag_add_tentacle: {flag_add_tentacles}")
    print(
        json.dumps(
            [
                item.dict()
                for item in squid_head.execute(
                    GetTentacles(squid, "$.name.matches('.*')")
                )
            ],
            indent=4,
            sort_keys=True,
        )
    )

    task_a = Task(
        name="task_a",
        category="echo",
        envs={},
        cmds=[
            TaskCmd(
                cmd="bash",
                args=["-c", "ln -s $TENTACLE_PATH/test.py $TENTACLE_TENTACLE_C_PATH"],
            )
        ],
    )
    # task_b = Task(
    #     name="task_b",
    #     category="echo",
    #     envs={"TEST": "hola"},
    #     cmds=[TaskCmd(cmd="python", args=["test.py"])],
    # )
    task_c = Task(
        name="task_c",
        category="echo",
        envs={},
        cmds=[TaskCmd(cmd="bash", args=["-c", "echo $TENTACLE_PATH"])],
    )
    flag_add_task = squid_head.execute(AddTasks(squid, [task_c], True))
    print(f"flag_add_task: {flag_add_task}")
    print(
        json.dumps(
            [
                item.dict()
                for item in squid_head.execute(GetTasks(squid, "$.name.matches('.*')"))
            ],
            indent=4,
            sort_keys=True,
        )
    )

    flag_link = squid_head.execute(CreateLink(squid, [task_a], [tentacle_ref_a]))
    print(flag_link)

    flag_link = squid_head.execute(CreateLink(squid, [task_c], [tentacle_ref_b]))
    print(flag_link)

    # flag_link = squid_head.execute(CreateLink(squid, [task_b, tentacle_ref_a], [tentacle_ref_c]))
    # print(flag_link)
    print(
        json.dumps(
            [
                item.dict()
                for item in squid_head.execute(
                    GetTentacles(squid, "$.name.matches('.*')", "tree")
                )
            ],
            indent=4,
            sort_keys=True,
        )
    )

    plugin = Plugin(
        name="to_list", source="./plugins", entrypoint="plugin:main", path="./plugins"
    )
    flag_add_plugin = squid_head.execute(AddPlugins(squid, [plugin], True))
    print(f"flag_add_plugin: {flag_add_plugin}")
    print(
        json.dumps(
            [
                item.dict()
                for item in squid_head.execute(
                    GetPlugins(squid, "$.name.matches('.*')")
                )
            ],
            indent=4,
            sort_keys=True,
        )
    )


if __name__ == "__main__":
    run()
