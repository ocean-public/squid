import json
import re
from typing import Any
import subprocess
from configparser import ConfigParser, SectionProxy
from os.path import expanduser
import os
import sys
import typer


def print_results(profile_name: str, credentials: dict):
    typer.secho(
        "THIS ENV VARIABLES ARE TO ACTIVATE PROFILE MFA",
        fg=typer.colors.WHITE,
        bold=True,
    )
    stmt = f'export AWS_ACCESS_KEY_ID="{credentials["AccessKeyId"]}"'
    stmt = f'{stmt} && export AWS_SECRET_ACCESS_KEY="{credentials["SecretAccessKey"]}"'
    stmt = f'{stmt} && export AWS_SESSION_TOKEN="{credentials["SessionToken"]}"'
    if "AWS_PROFILE" in os.environ:
        stmt = f"{stmt} && unset AWS_PROFILE"
    typer.secho(stmt, fg=typer.colors.GREEN, bold=True)
    typer.secho(f"OR USE THE PROFILE {profile_name}", fg=typer.colors.WHITE, bold=True)


def get_profile_from_user(config: ConfigParser) -> str:
    """Get profile from user."""
    sections = {
        str(idx): section
        for idx, section in enumerate(config.sections())
        if not bool([match for match in re.finditer(r".*-mfa$", section.strip())])
    }
    sections_choices = "\n".join(
        [f"{idx}-{section}" for idx, section in sections.items()]
    )
    section = typer.prompt(
        f"Choose profile from CONFIG file:\n{sections_choices}\n>> ", prompt_suffix=""
    )
    choice = sections.get(section, None)
    if choice is None:
        valid_choices = ", ".join([f"{idx}" for idx, _ in sections.items()])
        typer.echo(
            f"Invalid choice please tye with one of the following: {valid_choices}"
        )
        raise typer.Abort()
    return choice


def get_profile_config(profile: str = None) -> SectionProxy:
    """Get profile config."""
    config = ConfigParser()
    config.read(expanduser("~/.aws/config"))
    choice = get_profile_from_user(config=config) if profile is None else profile
    if not config.has_section(choice):
        typer.echo(f"Profile {choice} not found on $HOME/.aws/config", err=True)
        raise typer.Abort()
    return config[choice]


def get_clean_env() -> dict:
    """Get clean env."""
    clean_env = os.environ.copy()
    if "AWS_SESSION_TOKEN" in clean_env:
        typer.echo("Deleting session token")
        del clean_env["AWS_SESSION_TOKEN"]
    return clean_env


def parse_aws_response(data: Any) -> dict:
    """Parse AWS Response."""
    try:
        data_json = json.loads(data.decode("utf-8"))
    except json.JSONDecodeError as e:
        typer.echo(f"Error getting credentials {e}", err=True)
        raise typer.Abort()
    parse_data = data_json.get("Credentials", None)
    if parse_data is None:
        typer.echo(f"Error getting credentials.", err=True)
        raise typer.Abort()
    return parse_data


def get_credentials_from_aws(
    mfa_arn: str, mfa_token: str, profile_name: str, env: dict
) -> dict:
    """Get Credentials from AWS."""
    proc = subprocess.Popen(
        [
            "aws",
            "--profile",
            profile_name,
            "sts",
            "get-session-token",
            "--serial-number",
            mfa_arn,
            "--token-code",
            mfa_token,
            "--output",
            "json",
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        env=env,
    )
    data, _ = proc.communicate()
    if proc.returncode != 0:
        typer.echo("get-session-token failed: ", data.decode("utf-8"), err=True)
        raise typer.Abort()
    return parse_aws_response(data=data)


def get_credentials(config_profile: SectionProxy, token: str) -> dict:
    """Get Credentials."""
    mfa_arn = config_profile.get("mfa_arn")
    if not mfa_arn:
        typer.echo(
            f"Profile {config_profile.name} does not have a MFA ARN specify on $HOME/.aws/config.\nPlease add value 'mfa_arn=arn:aws:iam::############:mfa/<USER>' to $HOME/.aws/config",
            err=True,
        )
        sys.exit(-1)
    mfa_token = (
        typer.prompt("MFA TOKEN:\n>> ", prompt_suffix="") if token is None else token
    )
    clean_env = get_clean_env()
    return get_credentials_from_aws(
        mfa_arn=mfa_arn,
        mfa_token=mfa_token,
        profile_name=config_profile.name,
        env=clean_env,
    )


def persists_credentials(profile_name: str, credentials: dict) -> None:
    """Persists credentials in "~/.aws/credentials file."""
    creds = ConfigParser()
    creds.read(expanduser("~/.aws/credentials"))
    creds[f"{profile_name}"] = {}
    creds[f"{profile_name}"]["AWS_ACCESS_KEY_ID"] = credentials.get("AccessKeyId", "")
    creds[f"{profile_name}"]["AWS_SECRET_ACCESS_KEY"] = credentials.get(
        "SecretAccessKey", ""
    )
    creds[f"{profile_name}"]["AWS_SESSION_TOKEN"] = credentials.get("SessionToken", "")
    with open(expanduser("~/.aws/credentials"), "w") as configfile:
        creds.write(configfile)


def aws_mfa(profile: str = None, token: str = None) -> str:
    """MFA Auth for AWS profiles."""
    config_profile = get_profile_config(profile=profile)
    credentials = get_credentials(config_profile=config_profile, token=token)
    profile_name_mfa = f"{config_profile.name}-mfa"
    persists_credentials(profile_name=profile_name_mfa, credentials=credentials)
    print_results(profile_name=profile_name_mfa, credentials=credentials)
    return "Success!!!"


def setup(registry):
    registry.entry("aws-mfa", aws_mfa)
